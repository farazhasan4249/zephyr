<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {
  function __construct() {
    parent::__construct();

    $this->load->model('general', 'g');
  }  

  public function general(){   
    if($_POST){

      $this->form_validation->set_rules('settings_title', 'Site Title', 'required|min_length[3]|max_length[30]');
      $this->form_validation->set_rules('settings_email', 'Email', 'required|valid_email');
      $this->form_validation->set_rules('settings_footer_text', 'Footer Text', 'required');
      $this->form_validation->set_rules('settings_phone_num', 'Phone Number', 'required|exact_length[14]');
      $this->form_validation->set_rules('settings_address', 'Address', 'min_length[5]|max_length[300]');

      if (!$this->form_validation->run() == FALSE){
       
        $content = array(
          'settings_title' => $this->input->post('settings_title',TRUE),
          'settings_email' => $this->input->post('settings_email',TRUE),
          'settings_phone_num' => $this->input->post('settings_phone_num',TRUE),
          'settings_footer_text' => $this->input->post('settings_footer_text',TRUE),
          'settings_address' => $this->input->post('settings_address',TRUE),
          'settings_favicon' => $this->input->post('settings_favicon',TRUE),
          'settings_logo' => $this->input->post('settings_logo',TRUE),
          'settings_status' => 'enable',
          'settings_updated_by' => '1'
        );    
        if($_FILES['settings_logo']['size'] > 0){
          $settings_logo = single_image_upload($_FILES['settings_logo'],'./uploads/settings');
          if(is_array($settings_logo)){            
            $this->session->set_flashdata('error', $settings_logo);
          }else{
            $content['settings_logo'] = $settings_logo;
          }
        }  
        if($_FILES['settings_favicon']['size'] > 0){
          $settings_favicon = single_image_upload($_FILES['settings_favicon'],'./uploads/settings');
          if(is_array($settings_favicon)){            
            $this->session->set_flashdata('error', $settings_favicon);
          }else{
            $content['settings_favicon'] = $settings_favicon;
          }
        }  
        $data['where'] = array('settings_id' => 1);   
        $data['table'] = 'settings';  
        
        $this->g->update($data,$content);        
        $this->session->set_flashdata('success', 'Updated Successfully.');
        redirect('admin/settings/general');
      }
      else{
        $this->session->set_flashdata('error', 'Update Unsuccessfully.');
        $data['where'] = array('settings_id' => 1);   
        $data['table'] = 'settings';  
        $data['output_type'] = 'row'; 
        $content['title'] = 'General Settings';   
        $content['record']  = $this->g->get($data);
        $content['main_content'] = 'settings/general';      
        $this->load->view('admin/inc/view',$content);   
      }
    }
    else{        
      $data['where'] = array('settings_id' => 1);   
      $data['table'] = 'settings';  
      $data['output_type'] = 'row'; 
      $content['title'] = 'General Settings';   
      $content['record']  = $this->g->get($data);
      $content['main_content'] = 'settings/general';      
      $this->load->view('admin/inc/view',$content);   
    }
  }
}