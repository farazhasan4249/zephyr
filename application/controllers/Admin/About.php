<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {
    function __construct() {
    parent::__construct();  

    $this->load->model('general', 'g'); 
    }  

    public function edit()
    {   
      if($_POST){
       
            $this->form_validation->set_rules('about_heading', 'About heading', 'required');
            $this->form_validation->set_rules('about_content', 'About Content', 'required');
            $this->form_validation->set_rules('about_second_heading', 'About Second Heading', 'required');
            $this->form_validation->set_rules('about_second_content', 'About Second Content', 'required');
            

            if (!$this->form_validation->run() == FALSE){

                $content = array(
                'about_heading' => $this->input->post('about_heading',TRUE),
                'about_content' => $this->input->post('about_content',TRUE),
                'about_second_heading' => $this->input->post('about_second_heading',TRUE),
                'about_second_content' => $this->input->post('about_second_content',TRUE),

                'about_status' => 'enable',
                'about_updated_by' => '1',
                );    
                

                $data['where'] = array('about_id' => 1);     
                $data['table'] = 'about';    
                $this->general->update($data,$content);      
     
                $this->session->set_flashdata('success', 'Updated Successfully.');
                redirect('admin/About/edit');
            }else{        
                $data['where'] = array('about_id' => 1);     
                $data['table'] = 'about';    
                $data['output_type'] = 'row';   
                $content['title'] = 'about'; 
                $content['record']  = $this->general->get($data);
                $content['main_content'] = 'About/edit';         
                $this->load->view('admin/inc/view',$content);   
            } 
        }else{        
            $data['where'] = array('about_id' => 1);     
            $data['table'] = 'about';    
            $data['output_type'] = 'row';   
            $content['title'] = 'about'; 
            $content['record']  = $this->general->get($data);

            $content['main_content'] = 'about/edit';         
            $this->load->view('admin/inc/view',$content);   
        } 
    }
}
?>