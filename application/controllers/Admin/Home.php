<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    function __construct() {
    parent::__construct();	

    $this->load->model('general', 'g');	
    }  

    public function edit()
    {   
      if($_POST){
     // print_r($_POST); exit; 
            $this->form_validation->set_rules('home_heading', 'Home heading', 'required');
            $this->form_validation->set_rules('home_content', 'Home Content', 'required');
            $this->form_validation->set_rules('home_second_heading', 'Home Second Heading', 'required');
            $this->form_validation->set_rules('home_second_content', 'Home Second Content', 'required');
            //$this->form_validation->set_rules('service_page_heading', 'Service Page Heading', 'required');

            if (!$this->form_validation->run() == FALSE){

                $content = array(
                'home_heading' => $this->input->post('home_heading',TRUE),
                'home_content' => $this->input->post('home_content',TRUE),
                'home_second_heading' => $this->input->post('home_second_heading',TRUE),
                'home_second_content' => $this->input->post('home_second_content',TRUE),
                'home_banner' => $this->input->post('home_banner'.TRUE),
                'home_status' => 'enable',
                'home_updated_by' => '1',
                );

                if($_FILES['home_banner']['size'] > 0){
                    $video = single_image_upload($_FILES['home_banner'],'./uploads/images');
                    if(is_array($video)){            
                        $this->session->set_flashdata('error', $video);
                    }else{
                        $content['home_banner'] = $video;
                    }
                }    
                

                $data['where'] = array('home_id' => 1);     
                $data['table'] = 'home';    
                $this->general->update($data,$content);      
     
                $this->session->set_flashdata('success', 'Updated Successfully.');
                redirect('admin/home/edit');
            }else{        
                $data['where'] = array('home_id' => 1);     
                $data['table'] = 'home';    
                $data['output_type'] = 'row';   
                $content['title'] = 'home'; 
                $content['record']  = $this->general->get($data);
                $content['main_content'] = 'home/edit';         
                $this->load->view('admin/inc/view',$content);   
            } 
        }else{        
            $data['where'] = array('home_id' => 1);     
            $data['table'] = 'home';    
            $data['output_type'] = 'row';   
            $content['title'] = 'home'; 
            $content['record']  = $this->general->get($data);

            $content['main_content'] = 'home/edit';         
            $this->load->view('admin/inc/view',$content);   
        } 
    }
}
?>