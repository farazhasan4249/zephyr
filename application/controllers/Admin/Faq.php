<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {
    function __construct() {
    parent::__construct();  

    $this->load->model('general', 'g'); 
    }  

    public function edit()
    {   
      if($_POST){
       
            $this->form_validation->set_rules('faqs_heading', 'faq heading', 'required');
            $this->form_validation->set_rules('faqs_content', 'faq Content', 'required');
            $this->form_validation->set_rules('faqs_question', 'faqs question', 'required');
            $this->form_validation->set_rules('faqs_answer', 'faqs answer', 'required');
            

            if (!$this->form_validation->run() == FALSE){

                $content = array(
                'faqs_heading' => $this->input->post('faqs_heading',TRUE),
                'faqs_content' => $this->input->post('faqs_content',TRUE),
                'faqs_question' => $this->input->post('faqs_question',TRUE),
                'faqs_answer' => $this->input->post('faqs_answer',TRUE),
                'faqs_banner' => $this->input->post('faqs_banner',TRUE),

                'faqs_status' => 'enable',
                'faqs_updated_by' => '1',
                );    
                

                $data['where'] = array('faqs_id' => 1);     
                $data['table'] = 'faqs';    
                $this->general->update($data,$content);      
     
                $this->session->set_flashdata('success', 'Updated Successfully.');
                redirect('admin/faqs/edit');
            }else{        
                $data['where'] = array('faqs_id' => 1);     
                $data['table'] = 'faqs';    
                $data['output_type'] = 'row';   
                $content['title'] = 'faqs'; 
                $content['record']  = $this->general->get($data);
                $content['main_content'] = 'faqs/edit';         
                $this->load->view('admin/inc/view',$content);   
            } 
        }else{        
            $data['where'] = array('faqs_id' => 1);     
            $data['table'] = 'faqs';    
            $data['output_type'] = 'row';   
            $content['title'] = 'faqs'; 
            $content['record']  = $this->general->get($data);

            $content['main_content'] = 'faqs/edit';         
            $this->load->view('admin/inc/view',$content);   
        } 
    }
}
?>