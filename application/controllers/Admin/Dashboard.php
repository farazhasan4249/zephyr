<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller{
	
	public function index()
	{	
		 // $data['table'] = 'feature_section';
		 // $data['output_type'] = 'result';
		 // $content['features'] = $this->general->get($data);

		 // $data1['table'] = 'technology_section';
		 // $data1['output_type'] = 'result';
		 // $content['technology'] = $this->general->get($data1);

		  $data2['table'] = 'about';
		  $data2['output_type'] = 'result';
		  $content['about'] = $this->general->get($data2);

		  $data3['table'] = 'home';
		  $data3['output_type'] = 'result';
		  $content['home'] = $this->general->get($data3);
		
		 // $data3['table'] = 'contact_page';
		 // $data3['output_type'] = 'result';
		 // $content['contact'] = $this->general->get($data3);
		
		$content['main_content'] = 'dashboard/dashboard';			
		$this->load->view('admin/inc/view',$content);
	}
	
}
