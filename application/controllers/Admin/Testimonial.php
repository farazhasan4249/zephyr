<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonial extends CI_Controller{
    function __construct() {
        parent::__construct();  
    }
  
    public function index()
    {   
        $data['table'] = 'testimonial';
        $data['output_type'] = 'result';
        $content['records'] = $this->general->get($data);
        $content['title'] = 'Testimonial';
        $content['main_content'] = 'Testimonial/list';          
        $this->load->view('admin/inc/view',$content);
    }
    public function add()
    {
        if($_POST)
        {
            $this->form_validation->set_rules('testimonial_name', 'Name', 'trim|required|min_length[3]|max_length[300]');
            
            $this->form_validation->set_rules('testimonial_text', 'Text', 'trim|required');
            
            
            if(!$this->form_validation->run() == FALSE)
            {

                $content = array(
                    'testimonial_name' => $this->input->post('testimonial_name', TRUE),
                    'testimonial_text' => $this->input->post('testimonial_text', TRUE),
                    'testimonial_heading' => $this->input->post('testimonial_heading',TRUE),
                    'testimonial_image' => $this->input->post('testimonial_image',TRUE),
                    'testimonial_status' => 'enable',
                    'testimonial_created_by' => '1'
                );
                if($_FILES['testimonial_image']['size'] > 0){
                    $image = single_image_upload($_FILES['testimonial_image'],'./uploads/settings');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['testimonial_image'] = $image;
                    }
                } 
                
                $data['table'] = 'testimonial';
                $insert_id = $this->general->insert($data, $content);
                $this->session->set_flashdata('success', 'Added Successfully.');
                redirect('admin/testimonial');
            }
            else
            {   
                $content['main_content'] = 'testimonial/add';           
                $this->load->view('admin/inc/view',$content); 
            }
        }
        else
        {   
            $content['title'] = 'Testimonial';
            $content['main_content'] = 'testimonial/add';           
            $this->load->view('admin/inc/view',$content);  
        }  
    }
    
    public function edit($id)
    {
        if($_POST)
        {

            $this->form_validation->set_rules('testimonial_name', 'Name', 'trim|required|min_length[3]|max_length[300]');
            
            $this->form_validation->set_rules('testimonial_text', 'Text', 'trim|required');
           
            if(!$this->form_validation->run() == FALSE)
            {
                $content = array(
                    
                    'testimonial_name' => $this->input->post('testimonial_name', TRUE),
                    'testimonial_text' => $this->input->post('testimonial_text', TRUE),
                    'testimonial_heading' => $this->input->post('testimonial_heading',TRUE),
                    'testimonial_image' => $this->input->post('testimonial_image',TRUE),
                    'testimonial_status' => 'enable',
                    'testimonial_created_by' => '1'
                );
                if($_FILES['testimonial_image']['size'] > 0){
                    $image = single_image_upload($_FILES['testimonial_image'],'./uploads/settings');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['testimonial_image'] = $image;
                    }
                } 
                    $data['where'] = array('testimonial_id'=>$id);
                    $data['table'] = 'testimonial';
                    $insert_id = $this->general->update($data, $content);

                    $this->session->set_flashdata('success', 'Updated Successfully.');
                    redirect('admin/testimonial');
            }
            else
            {

                $data['where'] = array('testimonial_id'=>$id);
                $data['table'] = 'testimonial';
                $data['output_type'] = 'row';
                $content['record'] = $this->general->get($data);
                
                $content['title'] = 'Testimonial';
                $content['main_content'] = 'testimonial/edit';          
                $this->load->view('admin/inc/view',$content);
            }
        }
        else
        {
            $data['where'] = array('testimonial_id'=>$id);
            $data['table'] = 'testimonial';
            $data['output_type'] = 'row';
            $content['record'] = $this->general->get($data);
            $content['title'] = 'Testimonial';
            $content['main_content'] = 'testimonial/edit';          
            $this->load->view('admin/inc/view',$content);
        }  
    }
    public function view($id)
    {   
        $data['where'] = array('testimonial_id'=>$id);
        $data['table'] = 'testimonial';
        $data['output_type'] = 'row';
        $content['record'] = $this->general->get($data);
    
        $content['title'] = 'Testimonial';
        $content['main_content'] = 'testimonial/view';          
        $this->load->view('admin/inc/view',$content);
      
    }
    public function delete($id)
    {
         $content = array(
            'testimonial_status' => 'disable',
            'testimonial_updated_by' => '1',
        );
        $data['where'] = array('testimonial_id' => $id);
        $data['table'] = 'testimonial';
        $this->general->update($data, $content);
        $this->session->set_flashdata('success', 'Delete Successfully.');
        redirect('admin/testimonial');
        
    }
}

