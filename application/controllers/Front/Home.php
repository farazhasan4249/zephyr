<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Home extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
         $data = array();
         $data['table'] = "home";
        $data['output_type'] = "row";
        $content['home'] = $this->general->get($data);
		 

		 if($this->input->post()){
           $this->form_validation->set_rules('home_location', 'Home location', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('home_area', 'Home Area', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('home_budget', 'Home Budget', 'required');
			
			if (!$this->form_validation->run() == FALSE){
				
				$content = array(
					'home_location' => $this->input->post('home_location',TRUE),
					'home_area' => $this->input->post('home_area',TRUE),
					'home_budget' => $this->input->post('home_budget',TRUE),
					'home_status' => 'enable',
					'home_created_by' => '1',

				); 

				$this->general->insert($data,$content);      

				$this->session->set_flashdata('success', 'Updated Successfully.');
				redirect('forsale');
			}else{        
				redirect('');

			} 

		 }
		  $data = array();
		  $data['table'] = "testimonial";
        $data['output_type'] = "result";
        $content['testimonial'] = $this->general->get($data);

	$content['main_content'] = 'home';
		 $this->load->view('front/inc/view',$content);
        
	}

}
?>