<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Front_Controller
{

	public function index()
	{
		$content['main_content'] = 'login';
		$this->load->view('front/inc/view', $content);
	}

	public function loginquery()
	{
        // echo"a";exit;
		if (!$this->session->userdata('customer_email')) {

			$email = $this->input->post('customer_email', TRUE);
			$password = $this->input->post('customer_password', TRUE);
			$result = $this->general->get_list('customer', array('customer_email' => $email));
            //print_r($email);exit;
			if ($result) {
				foreach ($result as $res) {
					$pass = $this->encryption->decrypt($res->customer_password);
					//print_r($pass); exit;
				}
				if ($pass == $password) {
					$session_data = array
					(
						'customer_id' => $res->customer_id,
						'customer_email' => $res->customer_email,
						'customer_name' => $res->customer_name,
						'customer_newsletter' => $res->customer_newsletter,
						
					);
					$this->session->set_userdata($session_data);
					$this->session->set_flashdata('msg', '1');
					$this->session->set_flashdata('alert_data', 'Login Successfull.');
					redirect('');
				} else {

					$this->session->set_flashdata('msg', '2');
					$this->session->set_flashdata('alert_data', 'Invalid Email Or Password.');
					redirect('login');
				}
			} else {

				$this->session->set_flashdata('msg', '2');
				$this->session->set_flashdata('alert_data', 'Invalid Email Or Password.');
				redirect('login');
			}
		} else {
			redirect();
		}
	}

	public function forgot_password()
	{
		if (!empty($_POST)) {
			$email = $this->input->post('customer_email', TRUE);
			$result = $this->general->get_list('customer', array('customer_email' => $email));
			if ($result) {
				$today = date("Ymd");
				$rand = strtoupper(substr(uniqid(sha1(time())), 0, 120));
				$unique = $today . $rand;
				$forgot_password_token = $unique;
				$content['customer_forget_pass_token'] = $forgot_password_token;
				$data['where'] = array('customer_email' => $email);
				$data['table'] = 'customer';
				$this->general->update($data, $content);
				$section['subject'] = 'Password Reset Link';
				$section['body'] = '<strong>Reset Link :</strong> <a href="' . base_url('login/reset_password/') . $forgot_password_token . '">Click Here And You Will Be Redirected To The Website.</a>';
				$body = $this->load->view('front/email/template', $section, TRUE);
				send_email($email, $this->site_title . 'Password Reset Link', $body);
				$this->session->set_flashdata('success', 'Email Has Been Send With A Rest Link.');
				redirect('login/forgot_password');
			} else {
				$this->session->set_flashdata('error', 'Invalid Email.');
				redirect('login/forgot_password');
			}
		} else {
			$content['main_content'] = 'forgot_password';
			$this->load->view('front/inc/view', $content); 
		}
	}

	public function reset_password()
	{
		if (!empty($_POST)) {
			$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[3]|max_length[25]');
			$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[new_password]');
			if (!$this->form_validation->run() == FALSE) {
				$content['customer_password'] = $this->encryption->encrypt($this->input->post('confirm_password'));
				$data['where'] = array('customer_forget_pass_token' => $this->uri->segment(2));
				$data['table'] = 'customer';   
				$this->general->update($data, $content);
				$this->session->set_flashdata('success', 'Password Rest Successfull.');
				redirect('login');
			} else {
				$this->load->view('forgot-password');
			}
		} else {
			$content['main_content'] = 'new_password';
			$this->load->view('front/inc/view', $content);
		}
	}

}
?>
