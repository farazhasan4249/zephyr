<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class About extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index(){

        $data['table'] = "about";
        $data['output_type'] = "row";
        $content['about'] = $this->general->get($data);
		$content['main_content'] = "about";
		$this->load->view('front/inc/view',$content);
	}
}