<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Seller extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index(){
		
		$content['main_content'] = "seller";
		$this->load->view('front/inc/view',$content);

	}

	public function Seller(){
		// echo "<pre>";
		// print_r($this->input->post());die();

		if($this->input->post()){
			$this->form_validation->set_rules('seller_firstname', 'Seller Firstname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('seller_lastname', 'Seller Lastname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('seller_address', 'Seller Address', 'required');
			$this->form_validation->set_rules('seller_phone', 'Seller Phone', 'required|min_length[9]|max_length[15]');
			$this->form_validation->set_rules('seller_email', 'Seller Email', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('seller_message', 'Seller Message', 'required|min_length[2]|max_length[500]');

			if (!$this->form_validation->run() == FALSE){
				
				$content = array(
					'seller_firstname' => $this->input->post('seller_firstname',TRUE),
					'seller_lastname' => $this->input->post('seller_lastname',TRUE),
					'seller_address' => $this->input->post('seller_address',TRUE),
					'seller_phone' => $this->input->post('seller_phone',TRUE),
					'seller_email' => $this->input->post('seller_email',TRUE),
					'seller_message' => $this->input->post('seller_message',TRUE),
					'seller_status' => 'enable',
					'seller_created_by' => '1',

				); 

				$email = $this->input->post('seller_email');
				$data['table'] = 'seller';
				$content['main_content'] = "seller";
				$headers = 'From: '.$email;
				$to_email = 'fh@nadocrm.com';
				$subject = 'Seller Info';
				$message = '
				First Name : ' . $this->input->post("seller_firstname", TRUE) . '<br>
				Last Name : ' . $this->input->post("seller_lastname", TRUE) . '<br>
				Email : ' . $this->input->post("seller_email", TRUE) . '<br>
				Phone : ' . $this->input->post("seller_phone", TRUE) . '<br>
				Address : ' . $this->input->post("seller_address", TRUE) . '<br>
				Message : ' . $this->input->post("seller_message", TRUE) . '<br>
				';
				
				send_email($to_email,$subject,$message,$headers); 

				
				$this->general->insert($data,$content);      

				$this->session->set_flashdata('success', 'Updated Successfully.');
				redirect('');
			}else{        
				redirect('');

			} 
		}
	}
}
?>