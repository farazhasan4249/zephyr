<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Faq extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index(){
        
        $data['table'] = "faqs";
        $data['output_type'] = "row";
        $content['faqs'] = $this->general->get($data);
		$content['main_content'] = "faqs";
		$this->load->view('front/inc/view',$content);
	}
}