<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Contact extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index(){
		
		$content['main_content'] = "contact";
		$this->load->view('front/inc/view',$content);

	}


	
	public function Contact(){

		if($this->input->post()){
			
             // echo "<pre>";
             // print_r($this->input->post());die();

			$this->form_validation->set_rules('contact_page_fname', 'First Name', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('contact_page_lname', 'Last Name', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('contact_page_email', 'Email', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('contact_page_phonenum', 'Phonenum', 'required|min_length[9]|max_length[15]');
			$this->form_validation->set_rules('contact_page_message', 'Message', 'required|min_length[3]|max_length[300]');

			if (!$this->form_validation->run() == FALSE){
				
				$content = array(
					'contact_page_fname' => $this->input->post('contact_page_fname',TRUE),
					'contact_page_lname' => $this->input->post('contact_page_lname',TRUE),
					'contact_page_email' => $this->input->post('contact_page_email',TRUE),
					'contact_page_city' => $this->input->post('contact_page_city',TRUE),
					'contact_page_type' => $this->input->post('contact_page_type',TRUE),
					'contact_page_phonenum' => $this->input->post('contact_page_phonenum',TRUE),
					'contact_page_message' => $this->input->post('contact_page_message',TRUE),
					'contact_page_status' => 'enable',
					'contact_page_created_by' => '1',

				); 


				$email = $this->input->post('contact_page_email');
				$data['table'] = 'contact_page';
				$content['main_content'] = "contact";
				$headers = 'From: '.$email;
				$to_email = 'fh@nadocrm.com';
				$subject = 'Contact Info';
				$message = '
				First Name : ' . $this->input->post("contact_page_fname", TRUE) . '<br>
				Last Name : ' . $this->input->post("contact_page_lname", TRUE) . '<br>
				Email : ' . $this->input->post("contact_page_email", TRUE) . '<br>
				Phone : ' . $this->input->post("contact_page_phonenum", TRUE) . '<br>
				City : ' . $this->input->post("contact_page_city", TRUE) . '<br>
				Type : ' .$this->input->post("contact_page_type", TRUE) . '<br>
				Message : ' . $this->input->post("contact_page_message", TRUE) . '<br>
				';
				
				send_email($to_email,$subject,$message,$headers); 

				
				$this->general->insert($data,$content);      

				$this->session->set_flashdata('success', 'Updated Successfully.');
				redirect('');
			}else{        
				redirect('');

			} 
		}
	}
}


?>

