<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Property extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index(){
		
		$content['main_content'] = "property";
		$this->load->view('front/inc/view',$content);

	}

	public function Property(){
		     //echo "<pre>";
             //print_r($this->input->post());die();

		if($this->input->post()){
			$this->form_validation->set_rules('property_firstname', 'Property Firstname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('property_lastname', 'Property Lastname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('property_ownerfirst', 'Property Owner Firstname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('property_ownerlast', 'Property Owner Lastname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('property_address', 'Property Address', 'required');
			$this->form_validation->set_rules('property_phone', 'Property Phone', 'required|min_length[9]|max_length[15]');
			$this->form_validation->set_rules('property_email', 'Property Email', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('property_message', 'Property Message', 'required|min_length[2]|max_length[500]');

			if (!$this->form_validation->run() == FALSE){
				
				$content = array(
					'property_firstname' => $this->input->post('property_firstname',TRUE),
					'property_lastname' => $this->input->post('property_lastname',TRUE),
					'property_ownerfirst' => $this->input->post('property_ownerfirst',TRUE),
					'property_ownerlast' => $this->input->post('property_ownerlast',TRUE),
					'property_address' => $this->input->post('property_address',TRUE),
					'property_phone' => $this->input->post('property_phone',TRUE),
					'property_email' => $this->input->post('property_email',TRUE),
					'property_message' => $this->input->post('property_message',TRUE),
					'property_status' => 'enable',
					'property_created_by' => '1',

				); 

				$email = $this->input->post('property_email');
				$data['table'] = 'property';
				$content['main_content'] = "property";
				$headers = 'From: '.$email;
				$to_email = 'fh@nadocrm.com';
				$subject = 'Property Info';
				$message = '
				First Name : ' . $this->input->post("property_firstname", TRUE) . '<br>
				Last Name : ' . $this->input->post("property_lastname", TRUE) . '<br>
				Email : ' . $this->input->post("property_email", TRUE) . '<br>
				Phone : ' . $this->input->post("property_phone", TRUE) . '<br>
				Address : ' . $this->input->post("property_address", TRUE) . '<br>
				Message : ' . $this->input->post("property_message", TRUE) . '<br>
				';
				
				send_email($to_email,$subject,$message,$headers); 

				
				$this->general->insert($data,$content);      

				$this->session->set_flashdata('success', 'Updated Successfully.');
				redirect('');
			}else{        
				redirect('');

			} 
		}
	}
}
?>