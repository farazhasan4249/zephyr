<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Lend extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index(){
		
		$content['main_content'] = "lend";
		$this->load->view('front/inc/view',$content);

	}

	public function Lend(){
		     echo "<pre>";
             print_r($this->input->post());die();

		if($this->input->post()){
			$this->form_validation->set_rules('lend_firstname', 'Lend Firstname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('lend_lastname', 'Lend Lastname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('lend_address', 'Lend Address', 'required');
			$this->form_validation->set_rules('lend_phone', 'Lend Phone', 'required|min_length[9]|max_length[15]');
			$this->form_validation->set_rules('lend_email', 'Lend Email', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('lend_message', 'Lend Message', 'required|min_length[2]|max_length[500]');

			if (!$this->form_validation->run() == FALSE){
				
				$content = array(
					'lend_firstname' => $this->input->post('lend_firstname',TRUE),
					'lend_lastname' => $this->input->post('lend_lastname',TRUE),
					'lend_address' => $this->input->post('lend_address',TRUE),
					'lend_phone' => $this->input->post('lend_phone',TRUE),
					'lend_email' => $this->input->post('lend_email',TRUE),
					'lend_message' => $this->input->post('lend_message',TRUE),
					'lend_status' => 'enable',
					'lend_created_by' => '1',

				); 

				$email = $this->input->post('lend_email');
				$data['table'] = 'lend';
				$content['main_content'] = "lend";
				$headers = 'From: '.$email;
				$to_email = 'fh@nadocrm.com';
				$subject = 'Lend Info';
				$message = '
				First Name : ' . $this->input->post("lend_firstname", TRUE) . '<br>
				Last Name : ' . $this->input->post("lend_lastname", TRUE) . '<br>
				Email : ' . $this->input->post("lend_email", TRUE) . '<br>
				Phone : ' . $this->input->post("lend_phone", TRUE) . '<br>
				Address : ' . $this->input->post("lend_address", TRUE) . '<br>
				Message : ' . $this->input->post("lend_message", TRUE) . '<br>
				';
				
				send_email($to_email,$subject,$message,$headers); 

				
				$this->general->insert($data,$content);      

				$this->session->set_flashdata('success', 'Updated Successfully.');
				redirect('');
			}else{        
				redirect('');

			} 
		}
	}
}
?>