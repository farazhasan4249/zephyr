<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Rental extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index(){
         
    $pages = $this->input->get('per_page');
    $per_page = 4;    
    $this->load->library('pagination');
    $total_row = $this->db->where('rental_status','enable')->get('rental')->num_rows();


    $config['base_url'] = base_url().'rental';
    $config['total_rows'] = $this->db->where('rental_status','enable')->get('rental')->num_rows();
    $config['per_page'] = 1;
    $config['uri_segment'] = 3;
    $config['num_links'] = 3;
    // $config['use_page_numbers'] = TRUE;
    $config['page_query_string'] = TRUE;
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    $config['next_link'] = '&gt;';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['prev_link'] = '&lt;';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $offset = $this->uri->segment(3);
    $this->pagination->initialize($config);
    $page = $this->input->get('per_page');
    $start = ($page - 1) * $config['per_page'];

    echo pagenation(base_url().'rental',$per_page,$total_row);

    $data['table'] = "rental";
    $data['limit'] = $per_page;
    $data['offset'] = $pages;
    $data['output_type'] = "result";
    $content['records'] = $this->general->get($data);
    $content['main_content'] = 'rental';      
     $this->load->view('front/inc/view',$content);
	}

    public function search(){

    $pages = $this->input->get('per_page');
    $per_page = 2;    
    $this->load->library('pagination');
    $total_row = $this->db->where('rental_status','enable')->get('rental')->num_rows();


    $config['base_url'] = base_url().'rental';
    $config['total_rows'] = $this->db->where('rental_status','enable')->get('rental')->num_rows();
    $config['per_page'] = 2;
    $config['uri_segment'] = 3;
    $config['num_links'] = 3;
    // $config['use_page_numbers'] = TRUE;
    $config['page_query_string'] = TRUE;
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    $config['next_link'] = '&gt;';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['prev_link'] = '&lt;';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $offset = $this->uri->segment(3);
    $this->pagination->initialize($config);
    $page = $this->input->get('per_page');
    $start = ($page - 1) * $config['per_page'];

    echo pagenation(base_url().'rental',$per_page,$total_row);

    $data['table'] = "rental";
    $data['limit'] = $per_page;
    $data['offset'] = $pages;
    $data['output_type'] = "result";
    $content['records'] = $this->general->get($data);

     $keyword = $this->input->get('rental_keyword',TRUE);
   $location = $this->input->get('rental_location',TRUE);
   $property = $this->input->get('rental_property_type',TRUE);
   $status = $this->input->get('rental_property_status',TRUE);
   $square = $this->input->get('rental_squarefeet',TRUE);
   $bedroom = $this->input->get('rental_bedroom',TRUE);
   $bathroom = $this->input->get('rental_bathroom',TRUE);
   $area = $this->input->get('rental_maximum_area',TRUE);
 

   if(!empty($keyword)){
    $data['like'] = array('rental_keyword' => $keyword);
  }
  if(!empty($location)){
    $data['like_array'][0]['or_like'] = array('rental_location' => $location);
  }
  if(!empty($property)){
    $data['like_array'][1]['or_like'] = array('rental_property_type' => $property);
  }
  if(!empty($status)){
    $data['like_array'][2]['or_like'] = array('rental_property_status' => $status);
  }
  if(!empty($square)){
    $data['like_array'][3]['or_like'] = array('rental_squarefeet' => $square);
  }
  if(!empty($bedroom)){
    $data['like_array'][4]['or_like'] = array('rental_bedroom' => $bedroom);
  }
  if(!empty($bathroom)){
    $data['like_array'][5]['or_like'] = array('rental_bathroom' => $bathroom);
  }
  if(!empty($area)){
    $data['like_array'][6]['or_like'] = array('rental_maximum_area' => $area);
  }

  $data['table'] = "rental";
  $data['output_type'] = "result";
  $content['records'] = $this->general->get($data);

   $content['main_content'] = 'rental';     
   $this->load->view('front/inc/view',$content);
    }

	public function add()
    {
        if($_POST)
        {
            $this->form_validation->set_rules('rental_home_heading', 'Rental Home Heading', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('rental_location', 'rental Location', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('rental_squarefeet', 'rental SquareFt.', 'required');
			$this->form_validation->set_rules('rental_bedroom', 'rental Bedroom', 'required');
			$this->form_validation->set_rules('rental_bathroom', 'rental Bathroom', 'required');
			
			$this->form_validation->set_rules('rental_price', 'rental Price', 'required');
            $this->form_validation->set_rules('rental_keyword', 'rental Keyword', 'required');
            $this->form_validation->set_rules('rental_property_type', 'rental property type', 'required');
            $this->form_validation->set_rules('rental_property_status', 'rental Property status', 'required');
            $this->form_validation->set_rules('rental_maximum_area', 'rental maximum area', 'required');

			if (!$this->form_validation->run() == FALSE){
				
				$content = array(
					'rental_home_heading' => $this->input->post('rental_home_heading',TRUE),
                    'rental_home_subheading' => $this->input->post('rental_home_subheading',TRUE),
					'rental_location' => $this->input->post('rental_location',TRUE),
					'rental_image' => $this->input->post('rental_image',TRUE),
					'rental_squarefeet' => $this->input->post('rental_squarefeet',TRUE),
					'rental_bedroom' => $this->input->post('rental_bedroom',TRUE),
					'rental_bathroom' => $this->input->post('rental_bathroom',TRUE),
					'rental_price' => $this->input->post('rental_price',TRUE),
                    'rental_text' => $this->input->post('rental_text',TRUE),
                    'rental_keyword' => $this->input->post('rental_keyword',TRUE),
                    'rental_property_type' => $this->input->post('rental_property_type',TRUE),
                    'rental_property_status' => $this->input->post('rental_property_status',TRUE),
                    'rental_maximum_area' => $this->input->post('rental_maximum_area',TRUE),
					'rental_status' => 'enable',
					'rental_created_by' => '1',

				); 
				// print_r($content);exit;
                if($_FILES['rental_image']['size'] > 0){
                    $image = single_image_upload($_FILES['rental_image'],'./uploads/settings');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['rental_image'] = $image;
                    }
                } 
                
                $data['table'] = 'rental';
               // $content['record'] = $this->general->get($data);
                $insert_id = $this->general->insert($data, $content);
                $this->session->set_flashdata('success', 'Added Successfully.');
                redirect('rental');
            }
            else
            {   
                $content['main_content'] = 'rental/add';           
                $this->load->view('front/inc/view',$content); 
            }
        }
        else
        {   
            $content['title'] = 'rental';
            $content['main_content'] = 'rental/add';           
            $this->load->view('front/inc/view',$content);  
        }  
    }
}