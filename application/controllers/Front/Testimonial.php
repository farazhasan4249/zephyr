<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Testimonial extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index(){
        
        $data['table'] = 'testimonial';
		$content['main_content'] = "home";
		$content['testimonial'] = $this->general->get($data, $content);
		$this->load->view('front/inc/view',$content);
	}
	
}