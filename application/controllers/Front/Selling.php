<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Selling extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index(){
        
        $data['table'] = 'forsale';
		$content['main_content'] = "selling";
		$content['forsale'] = $this->general->get($data, $content);
		$this->load->view('front/inc/view',$content);
	}
	
}