<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Foreclosure extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index(){
    $pages = $this->input->get('per_page');
    $per_page = 1;    
    $this->load->library('pagination');
    $total_row = $this->db->where('foreclosure_status','enable')->get('foreclosure')->num_rows();


    $config['base_url'] = base_url().'foreclosure';
    $config['total_rows'] = $this->db->where('foreclosure_status','enable')->get('foreclosure')->num_rows();
    $config['per_page'] = 1;
    $config['uri_segment'] = 3;
    $config['num_links'] = 3;
    // $config['use_page_numbers'] = TRUE;
    $config['page_query_string'] = TRUE;
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    $config['next_link'] = '&gt;';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['prev_link'] = '&lt;';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $offset = $this->uri->segment(3);
    $this->pagination->initialize($config);
    $page = $this->input->get('per_page');
    $start = ($page - 1) * $config['per_page'];

    echo pagenation(base_url().'foreclosure',$per_page,$total_row);

    $data['table'] = "foreclosure";
    $data['limit'] = $per_page;
    $data['offset'] = $pages;
    $data['output_type'] = "result";
    $content['records'] = $this->general->get($data);
    $content['main_content'] = 'foreclosure/list';      
     $this->load->view('front/inc/view',$content);
	}

	public function search()
 {
    $pages = $this->input->get('per_page');
    $per_page = 1;    
    $this->load->library('pagination');
    $total_row = $this->db->where('foreclosure_status','enable')->get('foreclosure')->num_rows();


    $config['base_url'] = base_url().'foreclosure';
    $config['total_rows'] = $this->db->where('foreclosure_status','enable')->get('foreclosure')->num_rows();
    $config['per_page'] = 1;
    $config['uri_segment'] = 3;
    $config['num_links'] = 3;
    // $config['use_page_numbers'] = TRUE;
    $config['page_query_string'] = TRUE;
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    $config['next_link'] = '&gt;';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['prev_link'] = '&lt;';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $offset = $this->uri->segment(3);
    $this->pagination->initialize($config);
    $page = $this->input->get('per_page');
    $start = ($page - 1) * $config['per_page'];

    echo pagenation(base_url().'foreclosure',$per_page,$total_row);

    $data['table'] = "foreclosure";
    $data['limit'] = $per_page;
    $data['offset'] = $pages;
    $data['output_type'] = "result";
    $content['records'] = $this->general->get($data);

   $keyword = $this->input->get('foreclosure_keyword',TRUE);
   $location = $this->input->get('foreclosure_location',TRUE);
   $property = $this->input->get('foreclosure_property_type',TRUE);
   $status = $this->input->get('foreclosure_property_status',TRUE);
   $square = $this->input->get('foreclosure_squarefeet',TRUE);
   $bedroom = $this->input->get('foreclosure_bedroom',TRUE);
   $bathroom = $this->input->get('foreclosure_bathroom',TRUE);
   $area = $this->input->get('foreclosure_maximum_area',TRUE);
 

   if(!empty($keyword)){
    $data['like'] = array('foreclosure_keyword' => $keyword);
  }
  if(!empty($location)){
    $data['like_array'][0]['or_like'] = array('foreclosure_location' => $location);
  }
  if(!empty($property)){
    $data['like_array'][1]['or_like'] = array('foreclosure_property_type' => $property);
  }
  if(!empty($status)){
    $data['like_array'][2]['or_like'] = array('foreclosure_property_status' => $status);
  }
  if(!empty($square)){
    $data['like_array'][3]['or_like'] = array('foreclosure_squarefeet' => $square);
  }
  if(!empty($bedroom)){
    $data['like_array'][4]['or_like'] = array('foreclosure_bedroom' => $bedroom);
  }
  if(!empty($bathroom)){
    $data['like_array'][5]['or_like'] = array('foreclosure_bathroom' => $bathroom);
  }
  if(!empty($area)){
    $data['like_array'][6]['or_like'] = array('foreclosure_maximum_area' => $area);
  }

  $data['table'] = "foreclosure";
  $data['output_type'] = "result";
  $content['records'] = $this->general->get($data);

   $content['main_content'] = 'foreclosure/list';     
   $this->load->view('front/inc/view',$content);
}

	public function add()
    {
        if($_POST)
        {
            $this->form_validation->set_rules('foreclosure_home_heading', 'Home Heading', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('foreclosure_location', 'Foreclosure Location', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('foreclosure_squarefeet', 'Foreclosure SquareFt.', 'required');
			$this->form_validation->set_rules('foreclosure_bedroom', 'Foreclosure Bedroom', 'required');
			$this->form_validation->set_rules('foreclosure_bathroom', 'Foreclosure Bathroom', 'required');
			
			$this->form_validation->set_rules('foreclosure_price', 'Foreclosure Price', 'required');
            $this->form_validation->set_rules('foreclosure_keyword', 'Foreclosure Keyword', 'required');
            $this->form_validation->set_rules('foreclosure_property_type', 'Foreclosure Property Type', 'required');
            $this->form_validation->set_rules('foreclosure_property_status', 'Foreclosure Property Status', 'required');
            $this->form_validation->set_rules('foreclosure_maximum_area', 'Foreclosure Maximum Area', 'required');

			if (!$this->form_validation->run() == FALSE){
				
				$content = array(
					'foreclosure_home_heading' => $this->input->post('foreclosure_home_heading',TRUE),
					'foreclosure_location' => $this->input->post('foreclosure_location',TRUE),
					'foreclosure_image' => $this->input->post('foreclosure_image',TRUE),
					'foreclosure_squarefeet' => $this->input->post('foreclosure_squarefeet',TRUE),
					'foreclosure_bedroom' => $this->input->post('foreclosure_bedroom',TRUE),
					'foreclosure_bathroom' => $this->input->post('foreclosure_bathroom',TRUE),
					'foreclosure_price' => $this->input->post('foreclosure_price',TRUE),
                    'foreclosure_keyword' => $this->input->post('foreclosure_keyword',TRUE),
                    'Foreclosure_property_type' => $this->input->post('foreclosure_property_type',TRUE),
                    'Foreclosure_property_status' => $this->input->post('foreclosure_property_status',TRUE),
                    'foreclosure_maximum_area' => $this->input->post('foreclosure_maximum_area',TRUE),
					'foreclosure_status' => 'enable',
					'foreclosure_created_by' => '1',

				); 
				// print_r($content);exit;
                if($_FILES['foreclosure_image']['size'] > 0){
                    $image = single_image_upload($_FILES['foreclosure_image'],'./uploads/settings');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['foreclosure_image'] = $image;
                    }
                } 
                
                $data['table'] = 'foreclosure';
               // $content['record'] = $this->general->get($data);
                $insert_id = $this->general->insert($data, $content);
                $this->session->set_flashdata('success', 'Added Successfully.');
                redirect('foreclosure');
            }
            else
            {   
                $content['main_content'] = 'foreclosure/add';           
                $this->load->view('front/inc/view',$content); 
            }
        }
        else
        {   
            $content['title'] = 'foreclosure';
            $content['main_content'] = 'foreclosure/add';           
            $this->load->view('front/inc/view',$content);  
        }  
    }
}