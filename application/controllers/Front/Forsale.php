<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Forsale extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();


	}

	public function index(){
    $pages = $this->input->get('per_page');
    $per_page = 4;
    $this->load->library('pagination');
    $total_row = $this->db->where('forsale_status','enable')->get('forsale')->num_rows();

    $config['base_url'] = base_url().'forsale';
    $config['total_rows'] = $this->db->where('forsale_status','enable')->get('forsale')->num_rows();
    $config['per_page'] = 1;
    $config['uri_segment'] = 3;
    $config['num_links'] = 3;
    $config['page_query_string'] = TRUE;
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    $config['next_link'] = '&gt;';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['prev_link'] = '&lt;';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $offset = $this->uri->segment(3);
    $this->pagination->initialize($config);
    $page = $this->input->get('per_page');
    $start = ($page - 1) * $config['per_page'];

    echo pagenation(base_url().'forsale',$per_page,$total_row);
    $data['table'] = "forsale";
    $data['limit'] = $per_page;
    $data['offset'] = $pages;
    $data['output_type'] = "result";
    $content['records'] = $this->general->get($data);


    $content['main_content'] = 'forsale/list';       
     $this->load->view('front/inc/view',$content);
  }


  public function like()
  {
    if ($this->input->post()) {
      $content = array(
        'like' => $this->input->post('like',TRUE),
        'forsale_id' => $this->session->userdata('forsale_id'),
      );
      $data['table'] = 'forsale';
      $data['where'] = array('forsale_id' => $this->input->post('forsale_id',TRUE));
      $this->general->update($data,$content);
      echo json_encode("success");
    } else {
      echo json_encode("Invalid Error");
    }
  }


	public function search()
 {
    $pages = $this->input->get('per_page');
    $per_page = 4;
    $this->load->library('pagination');
    $total_row = $this->db->where('forsale_status','enable')->get('forsale')->num_rows();

    $config['base_url'] = base_url().'forsale';
    $config['total_rows'] = $this->db->where('forsale_status','enable')->get('forsale')->num_rows();
    $config['per_page'] = 1;
    $config['uri_segment'] = 3;
    $config['num_links'] = 3;
    $config['page_query_string'] = TRUE;
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    $config['next_link'] = '&gt;';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['prev_link'] = '&lt;';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $offset = $this->uri->segment(3);
    $this->pagination->initialize($config);
    $page = $this->input->get('per_page');
    $start = ($page - 1) * $config['per_page'];
    echo pagenation(base_url().'forsale',$per_page,$total_row);
    $data['table'] = "forsale";
    $data['limit'] = $per_page;
    $data['offset'] = $pages;
    $data['output_type'] = "result";
    $content['records'] = $this->general->get($data);

   $keyword = $this->input->get('forsale_keyword',TRUE);
   $location = $this->input->get('forsale_location',TRUE);
   $property = $this->input->get('forsale_property_type',TRUE);
   $status = $this->input->get('forsale_property_status',TRUE);
   $square = $this->input->get('forsale_squarefeet',TRUE);
   $bedroom = $this->input->get('forsale_bedroom',TRUE);
   $bathroom = $this->input->get('forsale_bathroom',TRUE);
   $area = $this->input->get('forsale_maximum_area',TRUE);
 

   if(!empty($keyword)){
    $data['like'] = array('forsale_keyword' => $keyword);
  }
  if(!empty($location)){
    $data['like_array'][0]['or_like'] = array('forsale_location' => $location);
  }
  if(!empty($property)){
    $data['like_array'][1]['or_like'] = array('forsale_property_type' => $property);
  }
  if(!empty($status)){
    $data['like_array'][2]['or_like'] = array('forsale_property_status' => $status);
  }
  if(!empty($square)){
    $data['like_array'][3]['or_like'] = array('forsale_squarefeet' => $square);
  }
  if(!empty($bedroom)){
    $data['like_array'][4]['or_like'] = array('forsale_bedroom' => $bedroom);
  }
  if(!empty($bathroom)){
    $data['like_array'][5]['or_like'] = array('forsale_bathroom' => $bathroom);
  }
  if(!empty($area)){
    $data['like_array'][6]['or_like'] = array('forsale_maximum_area' => $area);
  }

  $data['table'] = "forsale";
  $data['output_type'] = "result";
  $content['records'] = $this->general->get($data);

   $content['main_content'] = 'forsale/list';     
   $this->load->view('front/inc/view',$content);
}


	public function add()
    {
        if($_POST)
        {
            $this->form_validation->set_rules('forsale_home_heading', 'Home Heading', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('forsale_home_subheading', 'Home Subheading', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('forsale_squarefeet', 'Forsale SquareFt.', 'required');
			$this->form_validation->set_rules('forsale_bedroom', 'Forsale Bedroom', 'required');
			$this->form_validation->set_rules('forsale_bathroom', 'Forsale Bathroom', 'required');
			$this->form_validation->set_rules('forsale_text', 'Forsale Text', 'required|min_length[3]|max_length[500]');
			$this->form_validation->set_rules('forsale_price', 'Forsale Price', 'required');
      $this->form_validation->set_rules('forsale_keyword', 'Forsale Keyword', 'required');
      $this->form_validation->set_rules('forsale_location', 'Forsale Location', 'required');
      $this->form_validation->set_rules('forsale_property_type', 'Forsale Property Type', 'required');
      $this->form_validation->set_rules('forsale_property_status', 'Forsale Property Status', 'required');
      $this->form_validation->set_rules('forsale_maximum_area', 'Forsale Maximum Area', 'required');

			if (!$this->form_validation->run() == FALSE){
				
				$content = array(
					'forsale_home_heading' => $this->input->post('forsale_home_heading',TRUE),
					'forsale_home_subheading' => $this->input->post('forsale_home_subheading',TRUE),
					'forsale_image' => $this->input->post('forsale_image',TRUE),
					'forsale_squarefeet' => $this->input->post('forsale_squarefeet',TRUE),
					'forsale_bedroom' => $this->input->post('forsale_bedroom',TRUE),
					'forsale_bathroom' => $this->input->post('forsale_bathroom',TRUE),
					'forsale_text' => $this->input->post('forsale_text',TRUE),
					'forsale_price' => $this->input->post('forsale_price',TRUE),
          'forsale_keyword' => $this->input->post('forsale_keyword',TRUE),
          'forsale_location' => $this->input->post('forsale_location',TRUE),
          'forsale_property_type' => $this->input->post('forsale_property_type',TRUE),
          'forsale_property_status' => $this->input->post('forsale_property_status',TRUE),
          'forsale_maximum_area' => $this->input->post('forsale_maximum_area',TRUE),
					'forsale_status' => 'enable',
					'forsale_created_by' => '1',

				); 
                if($_FILES['forsale_image']['size'] > 0){
                    $image = single_image_upload($_FILES['forsale_image'],'./uploads/settings');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['forsale_image'] = $image;
                    }
                } 
                
                $data['table'] = 'forsale';
               //$content['record'] = $this->general->get($data);
                $insert_id = $this->general->insert($data, $content);
                $this->session->set_flashdata('success', 'Added Successfully.');
                redirect('forsale');
            }
            else
            {   
                $content['main_content'] = 'forsale/add';           
                $this->load->view('front/inc/view',$content); 
            }
        }
        else
        {   
            $content['title'] = 'Forsale';
            $content['main_content'] = 'forsale/add';           
            $this->load->view('front/inc/view',$content);  
        }  
    }

    public function view($id)
    {   
        $data['where'] = array('forsale_id'=>$id);
        $data['table'] = 'forsale';
        $data['output_type'] = 'row';
        $content['record'] = $this->general->get($data);
    
        $content['title'] = 'Forsale';
        $content['main_content'] = 'forsale/view';          
        $this->load->view('front/inc/view',$content);
      
    }



    public function Seller(){
		// echo "<pre>";
		// print_r($this->input->post());die();

		if($this->input->post()){
			$this->form_validation->set_rules('seller_firstname', 'Seller Firstname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('seller_lastname', 'Seller Lastname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('seller_address', 'Seller Address', 'required');
			$this->form_validation->set_rules('seller_phone', 'Seller Phone', 'required|min_length[9]|max_length[15]');
			$this->form_validation->set_rules('seller_email', 'Seller Email', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('seller_message', 'Seller Message', 'required|min_length[2]|max_length[500]');

			if (!$this->form_validation->run() == FALSE){
				
				$content = array(
					'seller_firstname' => $this->input->post('seller_firstname',TRUE),
					'seller_lastname' => $this->input->post('seller_lastname',TRUE),
					'seller_address' => $this->input->post('seller_address',TRUE),
					'seller_phone' => $this->input->post('seller_phone',TRUE),
					'seller_email' => $this->input->post('seller_email',TRUE),
					'seller_message' => $this->input->post('seller_message',TRUE),
					'seller_status' => 'enable',
					'seller_created_by' => '1',

				); 

				$email = $this->input->post('seller_email');
				$data['table'] = 'seller';
				$content['main_content'] = "seller";
				$headers = 'From: '.$email;
				$to_email = 'fh@nadocrm.com';
				$subject = 'Seller Info';
				$message = '
				First Name : ' . $this->input->post("seller_firstname", TRUE) . '<br>
				Last Name : ' . $this->input->post("seller_lastname", TRUE) . '<br>
				Email : ' . $this->input->post("seller_email", TRUE) . '<br>
				Phone : ' . $this->input->post("seller_phone", TRUE) . '<br>
				Address : ' . $this->input->post("seller_address", TRUE) . '<br>
				Message : ' . $this->input->post("seller_message", TRUE) . '<br>
				';
				
				send_email($to_email,$subject,$message,$headers); 

				
				$this->general->insert($data,$content);

				$this->session->set_flashdata('success', 'Updated Successfully.');
				redirect('');
			}else{        
				redirect('');

			} 
		}
	}

	
	public function edit(){

		if($this->input->post()){
			
             // echo "<pre>";
             // print_r($this->input->post());die();

			$this->form_validation->set_rules('forsale_home_heading', 'Home Heading', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('forsale_home_subheading', 'Home Subheading', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('forsale_squarefeet', 'Forsale SquareFt.', 'required');
			$this->form_validation->set_rules('forsale_bedroom', 'Forsale Bedroom', 'required');
			$this->form_validation->set_rules('forsale_bathroom', 'Forsale Bathroom', 'required');
			$this->form_validation->set_rules('forsale_text', 'Forsale Text', 'required|min_length[3]|max_length[500]');
			$this->form_validation->set_rules('forsale_price', 'Forsale Price', 'required');

			if (!$this->form_validation->run() == FALSE){
				
				$content = array(
					'forsale_home_heading' => $this->input->post('forsale_home_heading',TRUE),
					'forsale_home_subheading' => $this->input->post('forsale_home_subheading',TRUE),
					'forsale_squarefeet' => $this->input->post('forsale_squarefeet',TRUE),
					'forsale_bedroom' => $this->input->post('forsale_bedroom',TRUE),
					'forsale_bathroom' => $this->input->post('forsale_bathroom',TRUE),
					'forsale_text' => $this->input->post('forsale_text',TRUE),
					'forsale_price' => $this->input->post('forsale_price',TRUE),
					'forsale_status' => 'enable',
					'forsale_created_by' => '1',

				); 

				$data['table'] = 'forsale';
				$content['main_content'] = "forsale/list";
								
				$this->general->insert($data,$content);      

				$this->session->set_flashdata('success', 'Updated Successfully.');
				redirect('');
			}else{        
				redirect('');

			} 
		}
	}
}


?>

