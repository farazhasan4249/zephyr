<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends Front_Controller {
	
	public function index()
	{   
        
        $data = array();
        $data['table'] = 'forsale';
        $data['output_type'] = 'result';
        $search = $this->general->get($data);

        /*$i = 0;
        foreach($search as $searches){
          $responseJSON['product_name'][$i] = $searches->product_name;
          $responseJSON['product_alternate_name'][$i] = $searches->product_alternate_name;
          $responseJSON['product_slug'][$i] = $searches->product_slug;
          $responseJSON['product_sku'][$i] = $searches->product_sku;
          $responseJSON['product_image'][$i] = $searches->product_image;
          $responseJSON['product_reg_price'][$i] = $searches->product_reg_price;
          $responseJSON['product_dis_price'][$i] = $searches->product_dis_price;
          $i++;

        }*/
        /*echo "<pre>";
        print_r($responseJSON);die();*/
        $decoded = json_encode($search, TRUE );
        echo $decoded; 		
  
	}

    public function search_button()
    {   
        $value = $this->input->get('value',TRUE);
        
        $data['table'] = "forsale";
        if(!empty($value)){
            $data['like'] = array('forsale_home_heading' => $value);
        }
        
        $data['output_type'] = "result";
        $content['records'] = $this->general->get($data);
        $content['search'] = 'Search';
        
        // $data = array();    
        // $data['table'] = "forsale";
        // $data['output_type'] = "result";
        // $content['categories'] = $this->general->get($data);
        
        // $data = array();    
        // $data['table'] = "product";
        // $data['select_max'] = "product_reg_price";
        // $data['output_type'] = "row";
        // $content['prize_max'] = $this->general->get($data);
        
        // $data = array();    
        // $data['table'] = "product";
        // $data['select_min'] = "product_reg_price";
        // $data['output_type'] = "row";
        // $content['prize_min'] = $this->general->get($data);
        
        $content['main_content'] = 'forsale';			
        $this->load->view('front/inc/view',$content);	     
  
    }

}
