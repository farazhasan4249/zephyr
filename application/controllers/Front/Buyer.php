<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Buyer extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index(){
		
		$content['main_content'] = "buyer";
		$this->load->view('front/inc/view',$content);

	}

	public function Lend(){
		    // echo "<pre>";
            // print_r($this->input->post());die();

		if($this->input->post()){
			$this->form_validation->set_rules('lend_firstname', 'Lend Firstname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('lend_lastname', 'Lend Lastname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('lend_address', 'Lend Address', 'required');
			$this->form_validation->set_rules('lend_phone', 'Lend Phone', 'required|min_length[9]|max_length[15]');
			$this->form_validation->set_rules('lend_email', 'Lend Email', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('lend_message', 'Lend Message', 'required|min_length[2]|max_length[500]');

			if (!$this->form_validation->run() == FALSE){
				
				$content = array(
					'lend_firstname' => $this->input->post('lend_firstname',TRUE),
					'lend_lastname' => $this->input->post('lend_lastname',TRUE),
					'lend_address' => $this->input->post('lend_address',TRUE),
					'lend_phone' => $this->input->post('lend_phone',TRUE),
					'lend_email' => $this->input->post('lend_email',TRUE),
					'lend_message' => $this->input->post('lend_message',TRUE),
					'lend_status' => 'enable',
					'lend_created_by' => '1',

				); 

				$email = $this->input->post('lend_email');
				$data['table'] = 'lend';
				$content['main_content'] = "lend";
				$headers = 'From: '.$email;
				$to_email = 'fh@nadocrm.com';
				$subject = 'Lend Info';
				$message = '
				First Name : ' . $this->input->post("lend_firstname", TRUE) . '<br>
				Last Name : ' . $this->input->post("lend_lastname", TRUE) . '<br>
				Email : ' . $this->input->post("lend_email", TRUE) . '<br>
				Phone : ' . $this->input->post("lend_phone", TRUE) . '<br>
				Address : ' . $this->input->post("lend_address", TRUE) . '<br>
				Message : ' . $this->input->post("lend_message", TRUE) . '<br>
				';
				
				send_email($to_email,$subject,$message,$headers); 

				
				$this->general->insert($data,$content);      

				$this->session->set_flashdata('success', 'Updated Successfully.');
				redirect('');
			}else{        
				redirect('');

			} 
		}
	}

	public function Property(){
		    // echo "<pre>";
            // print_r($this->input->post());die();

		if($this->input->post()){
			$this->form_validation->set_rules('property_firstname', 'Property Firstname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('property_lastname', 'Property Lastname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('property_ownerfirst', 'Property Owner Firstname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('property_ownerlast', 'Property Owner Lastname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('property_address', 'Property Address', 'required');
			$this->form_validation->set_rules('property_phone', 'Property Phone', 'required|min_length[9]|max_length[15]');
			$this->form_validation->set_rules('property_email', 'Property Email', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('property_message', 'Property Message', 'required|min_length[2]|max_length[500]');

			if (!$this->form_validation->run() == FALSE){
				
				$content = array(
					'property_firstname' => $this->input->post('property_firstname',TRUE),
					'property_lastname' => $this->input->post('property_lastname',TRUE),
					'property_ownerfirst' => $this->input->post('property_ownerfirst',TRUE),
					'property_ownerlast' => $this->input->post('property_ownerlast',TRUE),
					'property_address' => $this->input->post('property_address',TRUE),
					'property_phone' => $this->input->post('property_phone',TRUE),
					'property_email' => $this->input->post('property_email',TRUE),
					'property_message' => $this->input->post('property_message',TRUE),
					'property_status' => 'enable',
					'property_created_by' => '1',

				); 

				$email = $this->input->post('property_email');
				$data['table'] = 'property';
				$content['main_content'] = "property";
				$headers = 'From: '.$email;
				$to_email = 'fh@nadocrm.com';
				$subject = 'Property Info';
				$message = '
				First Name : ' . $this->input->post("property_firstname", TRUE) . '<br>
				Last Name : ' . $this->input->post("property_lastname", TRUE) . '<br>
				Email : ' . $this->input->post("property_email", TRUE) . '<br>
				Phone : ' . $this->input->post("property_phone", TRUE) . '<br>
				Address : ' . $this->input->post("property_address", TRUE) . '<br>
				Message : ' . $this->input->post("property_message", TRUE) . '<br>
				';
				
				send_email($to_email,$subject,$message,$headers); 

				
				$this->general->insert($data,$content);      

				$this->session->set_flashdata('success', 'Updated Successfully.');
				redirect('');
			}else{        
				redirect('');

			} 
		}
	}

	public function Buyer(){
		     //echo "<pre>";
             //print_r($this->input->post());die();

		if($this->input->post()){
			$this->form_validation->set_rules('buyer_firstname', 'Buyer Firstname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('buyer_lastname', 'Buyer Lastname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('buyer_address', 'Buyer Address', 'required');
			$this->form_validation->set_rules('buyer_phone', 'Buyer Phone', 'required|min_length[9]|max_length[15]');
			$this->form_validation->set_rules('buyer_email', 'Buyer Email', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('buyer_message', 'Buyer Message', 'required|min_length[2]|max_length[500]');

			if (!$this->form_validation->run() == FALSE){
				
				$content = array(
					'buyer_firstname' => $this->input->post('buyer_firstname',TRUE),
					'buyer_lastname' => $this->input->post('buyer_lastname',TRUE),
					'buyer_address' => $this->input->post('buyer_address',TRUE),
					'buyer_phone' => $this->input->post('buyer_phone',TRUE),
					'buyer_email' => $this->input->post('buyer_email',TRUE),
					'buyer_message' => $this->input->post('buyer_message',TRUE),
					'buyer_status' => 'enable',
					'buyer_created_by' => '1',

				); 

				$email = $this->input->post('buyer_email');
				$data['table'] = 'buyer';
				$content['main_content'] = "buyer";
				$headers = 'From: '.$email;
				$to_email = 'fh@nadocrm.com';
				$subject = 'Buyer Info';
				$message = '
				First Name : ' . $this->input->post("buyer_firstname", TRUE) . '<br>
				Last Name : ' . $this->input->post("buyer_lastname", TRUE) . '<br>
				Email : ' . $this->input->post("buyer_email", TRUE) . '<br>
				Phone : ' . $this->input->post("buyer_phone", TRUE) . '<br>
				Address : ' . $this->input->post("buyer_address", TRUE) . '<br>
				Message : ' . $this->input->post("buyer_message", TRUE) . '<br>
				';
				
				send_email($to_email,$subject,$message,$headers); 

				
				$this->general->insert($data,$content);      

				$this->session->set_flashdata('success', 'Updated Successfully.');
				redirect('');
			}else{        
				redirect('');

			} 
		}
	}
}
?>