<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends Front_Controller
{

	public function index()
	{
		$content['main_content'] = 'register';
		$this->load->view('front/inc/view', $content);
	}


     public function sendquery()
	 {
         
		$this->form_validation->set_rules("customer_name", "Name", 'trim|required|min_length[2]|max_length[300]');
		$this->form_validation->set_rules("customer_email", "Email Address", 'trim|required|valid_email|min_length[3]|max_length[300]');
		$this->form_validation->set_rules("customer_password", "Password", 'trim|required');
        //$this->form_validation->set_rules("customer_newsletter", "newsletter");
		$this->form_validation->set_rules("confirm_password", "Confirm Password", 'required|matches[customer_password]');

		
		if ($this->form_validation->run()) {

			
			if (!$this->session->userdata('customer_email')) {
				$email = $this->input->post('customer_email', TRUE);
				$result = $this->general->get_list('customer', array('customer_email' => $email));
                // print_r($result); exit;

				if (!$result) {
					$content['customer_name'] = $this->input->post('customer_name', TRUE);
					$content['customer_email'] = $this->input->post('customer_email', TRUE);
                   // $content['customer_newsletter'] = $this->input->post('customer_newsletter', TRUE);
					$content['customer_password'] = $this->encryption->encrypt($this->input->post('customer_password', TRUE));
                    //echo "<pre>"; print_r($content);exit;
					$data['table'] = 'customer';
					$result = $this->general->insert($data, $content);
					if ($result) {
						$section['subject'] = 'Sign Up Inquiry';
						$section['body'] = '
						Full Name = ' . $this->input->post("customer_name", TRUE) . '<br>
						Email = ' . $this->input->post("customer_email", TRUE) . '<br>
						';
						$body = $this->load->view('front/email/template', $section, TRUE);
						send_email($this->email_to, $this->site_title . 'Sign Up Info', $body);

						$this->session->set_flashdata('msg', '1');
						$this->session->set_flashdata('alert_data', 'Sign Up Successfull');
						redirect('login', 'refresh');
					} else {
						$this->session->set_flashdata('msg', '2');
						$this->session->set_flashdata('alert_data', 'Something Went Wrong, Please Try Again Later.');
						redirect($_SERVER['HTTP_REFERER']);
					}
				} else {
					$this->session->set_flashdata('msg', '2');
					$this->session->set_flashdata('alert_data', 'Email already Exist.');
					redirect($_SERVER['HTTP_REFERER']);
				}
			} else {
				redirect($_SERVER['HTTP_REFERER']);
			}
		} else {
			$this->session->set_flashdata('msg', '2');
			$this->session->set_flashdata('alert_data', 'Please Fill All Required Fields Correctly');
			$content['main_content'] = 'Register';
			$this->load->view('front/inc/view', $content);
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
}
