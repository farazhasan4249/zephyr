<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front_Controller extends CI_Controller {
	function __construct() {
		parent::__construct();	          	
		$this->header_logo = $this->general->get_single_field('settings','','settings_logo');	
		$this->site_title = $this->general->get_single_field('settings','','settings_title');	
		$this->phone = $this->general->get_single_field('settings','','settings_phone_num');
		$this->address = $this->general->get_single_field('settings','','settings_address');	
		$this->email_address = $this->general->get_single_field('settings','','settings_email');	
	 	// $this->email_from = $this->general->get_single_field('settings','','settings_email_from');	
	 	$this->currency = $this->general->get_single_field('settings', '','settings_currency');		
	  	// $this->email_to = $this->general->get_single_field('settings','','settings_email_to');	
		$this->favicon = $this->general->get_single_field('settings','','settings_favicon');
	 	// $this->footer_text = $this->general->get_single_field('settings','','footer_text');			
   //      $this->form_validation->set_error_delimiters('<span class="help-block">','</span>');
      /*  $data['table'] = 'social'; 
        $data['output_type'] = 'result'; 
        $this->links = $this->general->get($data);*/
    }


}

