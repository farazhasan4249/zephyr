<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Vision's Data</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/vision');?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">              
                <div class="form-group">
                  <label>Heading</label>
                  <input type="name" class="form-control" id="vision_heading" name="vision_heading" value="<?php echo !empty($record->vision_heading)?$record->vision_heading:''?>" required>
                  <?php echo form_error('vision_heading'); ?>
                </div>
                <div class="form-group">
                  <label>Text</label>
                  <textarea class="editor form-control" rows="3" id="vision_text" name="vision_text" required><?php echo !empty($record->vision_text)?$record->vision_text:''?></textarea>
                  <?php echo form_error('vision_text'); ?>
                </div>
              </div>
              <div class="form-group">
                <label>Banner Image</label>
                <div class="input-group-btn">
                  <div class="image-upload">                      
                    <img src="<?php echo !empty($record->vision_img)?base_url('uploads/cms/').$record->vision_img:base_url('assets/img/placeholder.png')?>">
                    <div class="file-btn">
                      <input type="file" id="vision_img" name="vision_img">
                      <input type="text" id="vision_img" name="vision_img" value="<?php echo !empty($record->vision_img)?$record->vision_img:''?>" hidden>
                      <label class="btn btn-info">Upload</label>
                    </div>
                  </div>
                </div>
                <?php echo form_error('vision_img'); ?>
               </div>    
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>