
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add About</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/About/edit');?>" method="post" enctype="multipart/form-data">       
               <div class="box-body">              
                <div class="form-group">
                  <label>About Heading</label>
                  <input type="name" class="form-control" id="about_heading" name="about_heading" value="<?php echo !empty($record->about_heading)?$record->about_heading:''?>" required>
                  <?php echo form_error('about_heading'); ?>
                </div> 

              <div class="form-group">
                  <label>About Content</label>
                   <textarea class="editor form-control" rows="3" id="about_content" name="about_content" required><?php echo !empty($record->about_content)?$record->about_content:''?></textarea>
                  <?php echo form_error('about_content'); ?>
                </div>

                <div class="box-body">              
                <div class="form-group">
                  <label>About Second Heading</label>
                  <input type="name" class="form-control" id="about_second_heading" name="about_second_heading" value="<?php echo !empty($record->about_second_heading)?$record->about_second_heading:''?>" required>
                  <?php echo form_error('about_second_heading'); ?>
                </div> 

                <div class="form-group">
                  <label>About Second Content</label>
                   <textarea class="editor form-control" rows="3" id="about_second_content" name="about_second_content" required><?php echo !empty($record->about_second_content)?$record->about_second_content:''?></textarea>
                  <?php echo form_error('about_second_content'); ?>
                </div>

                <div class="form-group">
                <label>About Banner</label>
                <div class="input-group-btn">
                  <div class="image-upload">                      
                    <img src="<?php echo !empty($record->about_banner)?base_url('uploads/images/').$record->about_banner:base_url('assets/admin/img/placeholder.png')?>">
                    <div class="file-btn">
                      <input type="file" id="about_banner" name="about_banner">
                      <input type="text" id="about_banner" name="about_banner" value="<?php echo !empty($record->about_banner)?$record->about_banner:''?>" hidden>
                      <label class="btn btn-info">Upload</label>
                    </div>
                  </div>
                </div>
                <?php echo form_error('about_banner'); ?>
              </div>  
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>  
              </div>  
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>