<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Data</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/terms-page');?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">              
                <div class="form-group">
                  <label>Banner Heading</label>
                  <input type="name" class="form-control" id="terms_page_banner_heading" name="terms_page_banner_heading" value="<?php echo !empty($record->terms_page_banner_heading)?$record->terms_page_banner_heading:''?>" required>
                  <?php echo form_error('terms_page_banner_heading'); ?>
                </div>
                <div class="form-group">
                  <label>Text</label>
                  <textarea class="editor form-control" rows="3" id="terms_page_txt" name="terms_page_txt" required><?php echo !empty($record->terms_page_txt)?$record->terms_page_txt:''?></textarea>
                  <?php echo form_error('terms_page_txt'); ?>
                </div>            
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>
