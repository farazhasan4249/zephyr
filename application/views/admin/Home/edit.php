<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Data</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/Home/edit');?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">              
                <div class="form-group">
                  <label>Home Heading</label>
                  <input type="name" class="form-control" id="home_heading" name="home_heading" value="<?php echo !empty($record->home_heading)?$record->home_heading:''?>" required>
                  <?php echo form_error('home_heading'); ?>
                </div>              
                <div class="form-group">
                  <label>Home Content</label>
                   <textarea class="editor form-control" rows="3" id="home_content" name="home_content" required><?php echo !empty($record->home_content)?$record->home_content:''?></textarea>
                  <?php echo form_error('home_content'); ?>
                </div>

                <div class="form-group">
                  <label>Home Second Heading</label>
                  <input type="name" class="form-control" id="home_second_heading" name="home_second_heading" value="<?php echo !empty($record->home_second_heading)?$record->home_second_heading:''?>" required>
                  <?php echo form_error('home_second_heading'); ?>
                </div> 
      
                <div class="form-group">
                  <label>Home Second Content</label>
                  <textarea class="editor form-control" rows="3" id="home_second_content" name="home_second_content" required><?php echo !empty($record->home_second_content)?$record->home_second_content:''?></textarea>
                  <?php echo form_error('home_second_content'); ?>
                </div>
              </div>
                <div class="form-group">
                <label>Home Banner</label>
                <div class="input-group-btn">
                  <div class="image-upload">                      
                    <img src="<?php echo !empty($record->home_banner)?base_url('uploads/images/').$record->home_banner:base_url('assets/admin/img/placeholder.png')?>">
                    <div class="file-btn">
                      <input type="file" id="home_banner" name="home_banner">
                      <input type="text" id="home_banner" name="home_banner" value="<?php echo !empty($record->home_banner)?$record->home_banner:''?>" hidden>
                      <label class="btn btn-info">Upload</label>
                    </div>
                  </div>
                </div>
                <?php echo form_error('home_banner'); ?>
              </div>  

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $(".video-upload").on("change","input[type=file]",function() {      
            $(this).parents('.pro_videos').children('.col-md-4').children('.video-up').html('<img class="preview" src="' + URL.createObjectURL(this.files[0]) + '" /><i class="fa fa-times" aria-hidden="true"></i>');
          });
      
         $(".video-upload").on("click",".video-up i.fa.fa-times",function() {  
            $(this).parents('.video-up').html('');
            $(this).parents('.pro_videos').children('input[type=file]').val('');
   
   });
  });
</script>