
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add Faqs</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/faq/edit');?>" method="post" enctype="multipart/form-data">       
               <div class="box-body">              
                <div class="form-group">
                  <label>faqs Heading</label>
                  <input type="name" class="form-control" id="faqs_heading" name="faqs_heading" value="<?php echo !empty($record->faqs_heading)?$record->faqs_heading:''?>" required>
                  <?php echo form_error('faqs_heading'); ?>
                </div> 

              <div class="form-group">
                  <label>faqs Content</label>
                   <textarea class="editor form-control" rows="3" id="faqs_content" name="faqs_content" required><?php echo !empty($record->faqs_content)?$record->faqs_content:''?></textarea>
                  <?php echo form_error('faqs_content'); ?>
                </div>

                <div class="form-group">
                  <label>faqs question</label>
                   <textarea class="editor form-control" rows="3" id="faqs_question" name="faqs_question" required><?php echo !empty($record->faqs_question)?$record->faqs_question:''?></textarea>
                  <?php echo form_error('faqs_question'); ?>
                </div>

                <div class="box-body">              
                <div class="form-group">
                  <label>faqs answer</label>
                   <textarea class="editor form-control" rows="3" id="faqs_answer" name="faqs_answer" required><?php echo !empty($record->faqs_answer)?$record->faqs_answer:''?></textarea>
                  <?php echo form_error('faqs_answer'); ?>
                </div> 

                <div class="form-group">
                  <label>faqs questiontwo</label>
                   <textarea class="editor form-control" rows="3" id="faqs_questiontwo" name="faqs_questiontwo" required><?php echo !empty($record->faqs_questiontwo)?$record->faqs_questiontwo:''?></textarea>
                  <?php echo form_error('faqs_questiontwo'); ?>
                </div>

                            
                <div class="form-group">
                  <label>faqs answertwo</label>
                   <textarea class="editor form-control" rows="3" id="faqs_answertwo" name="faqs_answertwo" required><?php echo !empty($record->faqs_answertwo)?$record->faqs_answertwo:''?></textarea>
                  <?php echo form_error('faqs_answertwo'); ?>
                </div> 

                 <div class="form-group">
                  <label>faqs questionthree</label>
                   <textarea class="editor form-control" rows="3" id="faqs_questionthree" name="faqs_questionthree" required><?php echo !empty($record->faqs_questionthree)?$record->faqs_questionthree:''?></textarea>
                  <?php echo form_error('faqs_questionthree'); ?>
                </div>

                <div class="form-group">
                  <label>faqs answerthree</label>
                   <textarea class="editor form-control" rows="3" id="faqs_answerthree" name="faqs_answerthree" required><?php echo !empty($record->faqs_answerthree)?$record->faqs_answerthree:''?></textarea>
                  <?php echo form_error('faqs_answerthree'); ?>
                </div> 

                <div class="form-group">
                  <label>faqs questionfour</label>
                   <textarea class="editor form-control" rows="3" id="faqs_questionfour" name="faqs_questionfour" required><?php echo !empty($record->faqs_questionfour)?$record->faqs_questionfour:''?></textarea>
                  <?php echo form_error('faqs_questionfour'); ?>
                </div>

                 <div class="form-group">
                  <label>faqs answerfour</label>
                   <textarea class="editor form-control" rows="3" id="faqs_answerfour" name="faqs_answerfour" required><?php echo !empty($record->faqs_answerfour)?$record->faqs_answerfour:''?></textarea>
                  <?php echo form_error('faqs_answerfour'); ?>
                </div> 

                <div class="form-group">
                  <label>faqs questionfive</label>
                   <textarea class="editor form-control" rows="3" id="faqs_questionfive" name="faqs_questionfive" required><?php echo !empty($record->faqs_questionfive)?$record->faqs_questionfive:''?></textarea>
                  <?php echo form_error('faqs_questionfive'); ?>
                </div>

                 <div class="form-group">
                  <label>faqs answerfive</label>
                   <textarea class="editor form-control" rows="3" id="faqs_answerfive" name="faqs_answerfive" required><?php echo !empty($record->faqs_answerfive)?$record->faqs_answerfive:''?></textarea>
                  <?php echo form_error('faqs_answerfive'); ?>
                </div> 

                <div class="form-group">
                  <label>faqs questionsix</label>
                   <textarea class="editor form-control" rows="3" id="faqs_questionsix" name="faqs_questionsix" required><?php echo !empty($record->faqs_questionsix)?$record->faqs_questionsix:''?></textarea>
                  <?php echo form_error('faqs_questionsix'); ?>
                </div>

                 <div class="form-group">
                  <label>faqs answersix</label>
                   <textarea class="editor form-control" rows="3" id="faqs_answersix" name="faqs_answersix" required><?php echo !empty($record->faqs_answersix)?$record->faqs_answersix:''?></textarea>
                  <?php echo form_error('faqs_answersix'); ?>
                </div> 


                <div class="form-group">
                <label>Faqs Banner</label>
                <div class="input-group-btn">
                  <div class="image-upload">                      
                    <img src="<?php echo !empty($record->faqs_banner)?base_url('uploads/images/').$record->faqs_banner:base_url('assets/admin/img/placeholder.png')?>">
                    <div class="file-btn">
                      <input type="file" id="faqs_banner" name="faqs_banner">
                      <input type="text" id="faqs_banner" name="faqs_banner" value="<?php echo !empty($record->faqs_banner)?$record->faqs_banner:''?>" hidden>
                      <label class="btn btn-info">Upload</label>
                    </div>
                  </div>
                </div>
                <?php echo form_error('faqs_banner'); ?>
              </div>  
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>  
              </div>  
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>