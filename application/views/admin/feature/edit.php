<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Data</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/feature/edit');?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">              
                <div class="form-group">
                  <label>Feature Sec Heading</label>
                  <input type="name" class="form-control" id="feature_section_heading" name="feature_section_heading" value="<?php echo !empty($record->feature_section_heading)?$record->feature_section_heading:''?>" required>
                  <?php echo form_error('feature_section_heading'); ?>
                </div>  
                  
                  <div class="form-group">
                <label>VideoOne</label>
                <div class="input-group-btn">
                  <div class="image-upload">                      
                    <img src="<?php echo !empty($record->feature_section_videoone)?base_url('uploads/videos/').$record->feature_section_videoone:base_url('assets/admin/img/placeholder.png')?>">
                    <div class="file-btn">
                      <input type="file" id="feature_section_videoone" name="feature_section_videoone">
                      <input type="text" id="feature_section_videoone" name="feature_section_videoone" value="<?php echo !empty($record->feature_section_videoone)?$record->feature_section_videoone:''?>" hidden>
                      <label class="btn btn-info">Upload</label>
                    </div>
                  </div>
                </div>
                <?php echo form_error('feature_section_videoone'); ?>
              </div>  


                <div class="form-group">
                  <label>Feature Section SubheadingOne</label>
                  <input type="name" class="form-control" id="feature_section_subheadingone" name="feature_section_subheadingone" value="<?php echo !empty($record->feature_section_subheadingone)?$record->feature_section_subheadingone:''?>" required>
                  <?php echo form_error('feature_section_subheadingone'); ?>
                </div>
      
                <div class="form-group">
                  <label>Feature Section ContentOne</label>
                  <textarea class="editor form-control" rows="3" id="feature_section_contentone" name="feature_section_contentone" required><?php echo !empty($record->feature_section_contentone)?$record->feature_section_contentone:''?></textarea>
                  <?php echo form_error('feature_section_contentone'); ?>
                </div>
              </div>

                <div class="form-group">
                <label>VideoTwo</label>
                <div class="input-group-btn">
                  <div class="image-upload">                      
                    <img src="<?php echo !empty($record->feature_section_videotwo)?base_url('uploads/videos/').$record->feature_section_videotwo:base_url('assets/admin/img/placeholder.png')?>">
                    <div class="file-btn">
                      <input type="file" id="feature_section_videotwo" name="feature_section_videotwo">
                      <input type="text" id="feature_section_videotwo" name="feature_section_videotwo" value="<?php echo !empty($record->feature_section_videotwo)?$record->feature_section_videotwo:''?>" hidden>
                      <label class="btn btn-info">Upload</label>
                    </div>
                  </div>
                </div>
                <?php echo form_error('feature_section_videotwo'); ?>
              </div>  

               <div class="form-group">
                  <label>Feature Section SubheadingTwo</label>
                  <input type="name" class="form-control" id="feature_section_subheadingtwo" name="feature_section_subheadingtwo" value="<?php echo !empty($record->feature_section_subheadingtwo)?$record->feature_section_subheadingtwo:''?>" required>
                  <?php echo form_error('feature_section_subheadingtwo'); ?>
                </div>
      
                <div class="form-group">
                  <label>Feature Section ContentTwo</label>
                  <textarea class="editor form-control" rows="3" id="feature_section_contenttwo" name="feature_section_contenttwo" required><?php echo !empty($record->feature_section_contenttwo)?$record->feature_section_contenttwo:''?></textarea>
                  <?php echo form_error('feature_section_contenttwo'); ?>
                </div>
              

              <div class="form-group">
                  <label>Feature Section ContentThree</label>
                  <textarea class="editor form-control" rows="3" id="feature_section_contentthree" name="feature_section_contentthree" required><?php echo !empty($record->feature_section_contentthree)?$record->feature_section_contentthree:''?></textarea>
                  <?php echo form_error('feature_section_contentthree'); ?>
                </div>
              
              <div class="form-group">
                  <label>Feature Section SubheadingThree</label>
                  <input type="name" class="form-control" id="feature_section_subheadingthree" name="feature_section_subheadingthree" value="<?php echo !empty($record->feature_section_subheadingthree)?$record->feature_section_subheadingthree:''?>" required>
                  <?php echo form_error('feature_section_subheadingthree'); ?>
                </div>
              
              <div class="form-group">
                <label>VideoThree</label>
                <div class="input-group-btn">
                  <div class="image-upload">                      
                    <img src="<?php echo !empty($record->feature_section_videothree)?base_url('uploads/videos/').$record->feature_section_videothree:base_url('assets/admin/img/placeholder.png')?>">
                    <div class="file-btn">
                      <input type="file" id="feature_section_videothree" name="feature_section_videothree">
                      <input type="text" id="feature_section_videothree" name="feature_section_videothree" value="<?php echo !empty($record->feature_section_videothree)?$record->feature_section_videothree:''?>" hidden>
                      <label class="btn btn-info">Upload</label>
                    </div>
                  </div>
                </div>
                <?php echo form_error('feature_section_videothree'); ?>
              </div> 
              <div class="form-group">
                  <label>Feature Section SubheadingFour</label>
                  <input type="name" class="form-control" id="feature_section_subheadingfour" name="feature_section_subheadingfour" value="<?php echo !empty($record->feature_section_subheadingfour)?$record->feature_section_subheadingfour:''?>" required>
                  <?php echo form_error('feature_section_subheadingfour'); ?>
                </div>
         
              <div class="form-group">
                <label>VideoFour</label>
                <div class="input-group-btn">
                  <div class="image-upload">                      
                    <img src="<?php echo !empty($record->feature_section_videofour)?base_url('uploads/videos/').$record->feature_section_videofour:base_url('assets/admin/img/placeholder.png')?>">
                    <div class="file-btn">
                      <input type="file" id="feature_section_videofour" name="feature_section_videofour">
                      <input type="text" id="feature_section_videofour" name="feature_section_videofour" value="<?php echo !empty($record->feature_section_videofour)?$record->feature_section_videofour:''?>" hidden>
                      <label class="btn btn-info">Upload</label>
                    </div>
                  </div>
                </div>
                <?php echo form_error('feature_section_videofour'); ?>
              </div>
              <div class="form-group">
                  <label>Feature Section ContentFour</label>
                  <textarea class="editor form-control" rows="3" id="feature_section_contentfour" name="feature_section_contentfour" required><?php echo !empty($record->feature_section_contentfour)?$record->feature_section_contentfour:''?></textarea>
                  <?php echo form_error('feature_section_contentfour'); ?>
                </div>
          



              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $(".video-upload").on("change","input[type=file]",function() {      
            $(this).parents('.pro_videos').children('.col-md-4').children('.video-up').html('<img class="preview" src="' + URL.createObjectURL(this.files[0]) + '" /><i class="fa fa-times" aria-hidden="true"></i>');
          });
      
         $(".video-upload").on("click",".video-up i.fa.fa-times",function() {  
            $(this).parents('.video-up').html('');
            $(this).parents('.pro_videos').children('input[type=file]').val('');
   
   });
  });
</script>