 <div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Data</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/settings/general');?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">
                <div class="form-group">
                  <label>Site Title</label>
                  <input type="name" class="form-control" id="settings_title" name="settings_title" value="<?php echo !empty($record->settings_title)?$record->settings_title:''?>" required>
                  <?php echo form_error('settings_title'); ?>
                </div>
                <div class="form-group">
                  <label>Phone Num</label>
                  <input type="text" class="form-control" id="settings_phone_num" name="settings_phone_num" value="<?php echo !empty($record->settings_phone_num)?$record->settings_phone_num:''?>" required>
                  <?php echo form_error('settings_phone_num'); ?>
                </div>
                <div class="form-group">
                  <label>Email address</label>
                  <input type="email" class="form-control" id="settings_email" name="settings_email" value="<?php echo !empty($record->settings_email)?$record->settings_email:''?>" required>
                  <?php echo form_error('settings_email'); ?>
                </div>
        
                <div class="form-group">
                  <label>Address</label>
                  <textarea class="form-control" rows="3" id="settings_address" name="settings_address" required><?php echo !empty($record->settings_address)?$record->settings_address:''?></textarea>
                  <?php echo form_error('settings_address'); ?>
                </div>   

                <div class="form-group">
                  <label>Footer Text</label>
                  <textarea class="form-control" rows="3" id="settings_footer_text" name="settings_footer_text" required><?php echo !empty($record->settings_footer_text)?$record->settings_footer_text:''?></textarea>
                  <?php echo form_error('settings_footer_text'); ?>
                </div> 

                <div class="form-group">
                  <label>Fav Icon</label>
                  <div class="input-group-btn">
                    <div class="image-upload">                      
                      <img src="<?php echo !empty($record->settings_favicon)?base_url('uploads/settings/').$record->settings_favicon:base_url('assets/admin/img/placeholder.png')?>">
                      <div class="file-btn">
                        <input type="file" id="settings_favicon" name="settings_favicon">
                        <input type="text" id="settings_favicon" name="settings_favicon" value="<?php echo !empty($record->settings_favicon)?$record->settings_favicon:''?>" hidden>
                        <label class="btn btn-info">Upload</label>
                      </div>
                    </div>
                  </div>
                </div>  
                <div class="form-group">
                  <label>Logo</label>
                  <div class="input-group-btn">
                    <div class="image-upload">                      
                      <img src="<?php echo !empty($record->settings_logo)?base_url('uploads/settings/').$record->settings_logo:base_url('assets/admin/img/placeholder.png')?>">
                      <div class="file-btn">
                        <input type="file" id="settings_logo" name="settings_logo">
                        <input type="text" id="settings_logo" name="settings_logo" value="<?php echo !empty($record->settings_logo)?$record->settings_logo:''?>" hidden>
                        <label class="btn btn-info">Upload</label>
                      </div>
                    </div>
                  </div>
                   <?php echo form_error('settings_logo'); ?>                
                </div> 
                        
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>


