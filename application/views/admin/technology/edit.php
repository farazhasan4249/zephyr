<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Data</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/Technology/edit');?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">              
                <div class="form-group">
                  <label>Technology Sec Heading</label>
                  <input type="name" class="form-control" id="technology_section_heading" name="technology_section_heading" value="<?php echo !empty($record->technology_section_heading)?$record->technology_section_heading:''?>" required>
                  <?php echo form_error('technology_section_heading'); ?>
                </div>              
                <div class="form-group">
                  <label>Home Sec SubHeading One</label>
                  <input type="name" class="form-control" id="technology_section_subheadingone" name="technology_section_subheadingone" value="<?php echo !empty($record->technology_section_subheadingone)?$record->technology_section_subheadingone:''?>" required>
                  <?php echo form_error('technology_section_subheadingone'); ?>
                </div>
      
                <div class="form-group">
                  <label>Technology Sec ContentOne</label>
                  <textarea class="editor form-control" rows="3" id="technology_section_contentone" name="technology_section_contentone" required><?php echo !empty($record->technology_section_contentone)?$record->technology_section_contentone:''?></textarea>
                  <?php echo form_error('technology_section_contentone'); ?>
                </div>
             

              <div class="form-group">
                  <label>Technology Sec Subheading two</label>
                  <input type="name" class="form-control" id="technology_section_subheadingtwo" name="technology_section_subheadingtwo" value="<?php echo !empty($record->technology_section_subheadingtwo)?$record->technology_section_subheadingtwo:''?>" required>
                  <?php echo form_error('technology_section_subheadingtwo'); ?>
                </div>
            

              <div class="form-group">
                  <label>Technology Sec Content two</label>
                  <textarea class="editor form-control" rows="3" id="technology_section_contenttwo" name="technology_section_contenttwo" required><?php echo !empty($record->technology_section_contenttwo)?$record->technology_section_contenttwo:''?></textarea>
                  <?php echo form_error('technology_section_contenttwo'); ?>
                </div>
             
              <div class="form-group">
                  <label>Technology Sec Subheading Three</label>
                  <input type="name" class="form-control" id="technology_section_subheadingthree" name="technology_section_subheadingthree" value="<?php echo !empty($record->technology_section_subheadingthree)?$record->technology_section_subheadingthree:''?>" required>
                  <?php echo form_error('technology_section_subheadingthree'); ?>
                </div>
          

              <div class="form-group">
                  <label>Technology Sec Content three</label>
                  <textarea class="editor form-control" rows="3" id="technology_section_contentthree" name="technology_section_contentthree" required><?php echo !empty($record->technology_section_contentthree)?$record->technology_section_contentthree:''?></textarea>
                  <?php echo form_error('technology_section_contentthree'); ?>
                </div>
            

              <div class="form-group">
                  <label>Technology Sec Subheading four</label>
                  <input type="name" class="form-control" id="technology_section_subheadingfour" name="technology_section_subheadingfour" value="<?php echo !empty($record->technology_section_subheadingfour)?$record->technology_section_subheadingfour:''?>" required>
                  <?php echo form_error('technology_section_subheadingfour'); ?>
                </div>
        

              <div class="form-group">
                  <label>Technology Sec Content four</label>
                  <textarea class="editor form-control" rows="3" id="technology_section_contentfour" name="technology_section_contentfour" required><?php echo !empty($record->technology_section_contentfour)?$record->technology_section_contentfour:''?></textarea>
                  <?php echo form_error('technology_section_contentfour'); ?>
           </div>

               <div class="form-group">
                  <label>Technology Sec Subheading Five</label>
                  <input type="name" class="form-control" id="technology_section_subheadingfive" name="technology_section_subheadingfive" value="<?php echo !empty($record->technology_section_subheadingfive)?$record->technology_section_subheadingfive:''?>" required>
                  <?php echo form_error('technology_section_subheadingfive'); ?>
                </div>
       

               <div class="form-group">
                  <label>Technology Sec Content five</label>
                  <textarea class="editor form-control" rows="3" id="technology_section_contentfive" name="technology_section_contentfive" required><?php echo !empty($record->technology_section_contentfive)?$record->technology_section_contentfive:''?></textarea>
                  <?php echo form_error('technology_section_contentfive'); ?>
                </div>
      

                <div class="form-group">
                  <label>Technology Sec Subheading Six</label>
                  <input type="name" class="form-control" id="technology_section_subheadingsix" name="technology_section_subheadingsix" value="<?php echo !empty($record->technology_section_subheadingsix)?$record->technology_section_subheadingsix:''?>" required>
                  <?php echo form_error('technology_section_subheadingsix'); ?>
                </div>
       
                
                  <div class="form-group">
                  <label>Technology Sec Content six</label>
                  <textarea class="editor form-control" rows="3" id="technology_section_contentsix" name="technology_section_contentsix" required><?php echo !empty($record->technology_section_contentsix)?$record->technology_section_contentsix:''?></textarea>
                  <?php echo form_error('technology_section_contentsix'); ?>
                </div>
       

  
        

              <div class="form-group">
                  <label>Technology Sec Subheading Seven</label>
                  <input type="name" class="form-control" id="technology_section_subheadingseven" name="technology_section_subheadingseven" value="<?php echo !empty($record->technology_section_subheadingseven)?$record->technology_section_subheadingseven:''?>" required>
                  <?php echo form_error('technology_section_subheadingseven'); ?>
                </div>
      

              <div class="form-group">
                  <label>Technology Sec Content Seven</label>
                  <textarea class="editor form-control" rows="3" id="technology_section_contentseven" name="technology_section_contentseven" required><?php echo !empty($record->technology_section_subheadingseven)?$record->technology_section_contentseven:''?></textarea>
                  <?php echo form_error('technology_section_contentseven'); ?>
                </div>
         

              <div class="form-group">
                  <label>Technology Sec Subheading Eight</label>
                  <input type="name" class="form-control" id="technology_section_subheadingeight" name="technology_section_subheadingeight" value="<?php echo !empty($record->technology_section_subheadingeight)?$record->technology_section_subheadingeight:''?>" required>
                  <?php echo form_error('technology_section_subheadingeight'); ?>
                </div>
        

              <div class="form-group">
                  <label>Technology Sec Content Eight</label>
                  <textarea class="editor form-control" rows="3" id="technology_section_contenteight" name="technology_section_contenteight" required><?php echo !empty($record->technology_section_contenteight)?$record->technology_section_contenteight:''?></textarea>
                  <?php echo form_error('technology_section_contenteight'); ?>
                </div>
       

              <div class="form-group">
                  <label>Technology Sec Subheading Nine</label>
                  <input type="name" class="form-control" id="technology_section_subheadingnine" name="technology_section_subheadingnine" value="<?php echo !empty($record->technology_section_subheadingnine)?$record->technology_section_subheadingnine:''?>" required>
                  <?php echo form_error('technology_section_subheadingnine'); ?>
                </div>
           

              <div class="form-group">
                  <label>Technology Sec Content Nine</label>
                  <textarea class="editor form-control" rows="3" id="technology_section_contentnine" name="technology_section_contentnine" required><?php echo !empty($record->technology_section_contentnine)?$record->technology_section_subheadingnine:''?></textarea>
                  <?php echo form_error('technology_section_contentnine'); ?>
                </div>
           



              

                
                

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>