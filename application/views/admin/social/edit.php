<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Data</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/social/edit/').$record->social_id;?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">   
                <div class="form-group">
                  <label>Name</label>
                  <input type="name" class="form-control" id="social_name" name="social_name" value="<?php echo !empty($record->social_name)?$record->social_name:''?>" required>
                  <?php echo form_error('social_name'); ?>
                </div>                      
                <div class="form-group">
                  <label>Link</label>
                  <input type="name" class="form-control" id="social_link" name="social_link" value="<?php echo !empty($record->social_link)?$record->social_link:''?>">
                  <?php echo form_error('social_link'); ?>
                </div>              
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>
