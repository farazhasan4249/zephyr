<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Data</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/service/edit/').$record->service_id;?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">   
                <div class="form-group">
                  <label>Service Heading</label>
                  <input type="name" class="form-control" id="service_heading" name="service_heading" value="<?php echo !empty($record->service_heading)?$record->service_heading:''?>" required>
                  <?php echo form_error('service_heading'); ?>
                </div> 
                <div class="form-group">
                  <label>Service text</label>
                  <textarea class="editor form-control" rows="3" id="service_text" name="service_text" required><?php echo !empty($record->service_text)?$record->service_text:''?></textarea>
                  <?php echo form_error('service_text'); ?>
                </div>  
                <div class="form-group">
                    <label>Service Icon Image</label>
                    <div class="input-group-btn">
                      <div class="image-upload">                      
                        <img src="<?php echo !empty($record->service_icon)?base_url('uploads/cms/').$record->service_icon:base_url('assets/img/placeholder.png')?>">
                        <div class="file-btn">
                          <input type="file" id="service_icon" name="service_icon">
                          <input type="text" id="service_icon" name="service_icon" value="<?php echo !empty($record->service_icon)?$record->service_icon:''?>" hidden>
                          <label class="btn btn-info">Upload</label>
                        </div>
                      </div>
                    </div>
                    <?php echo form_error('service_icon'); ?>
                </div>   
                <div class="form-group">
                    <label>Service Image</label>
                    <div class="input-group-btn">
                      <div class="image-upload">                      
                        <img src="<?php echo !empty($record->service_img)?base_url('uploads/cms/').$record->service_img:base_url('assets/img/placeholder.png')?>">
                        <div class="file-btn">
                          <input type="file" id="service_img" name="service_img">
                          <input type="text" id="service_img" name="service_img" value="<?php echo !empty($record->service_img)?$record->service_img:''?>" hidden>
                          <label class="btn btn-info">Upload</label>
                        </div>
                      </div>
                    </div>
                    <?php echo form_error('service_img'); ?>
                </div>          
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>
