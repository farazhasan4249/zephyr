<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">View Service</h3>
          </div>     
          <div class="col-md-6">
            <div class="box-body">
              <div class="form-group">
                  <span class="col-md-2 view_label">Heading</span>
                  <span class="col-md-10 view_details"><?php echo $record->service_heading;?></span>
              </div>
              <div class="form-group">
                  <span class="col-md-2 view_label">Text</span>
                  <span class="col-md-10 view_details"><?php echo $record->service_text;?></span>
              </div>
              <div class="form-group">
                  <span class="col-md-2 view_label">Icon Image</span>
                  <span class="col-md-10 view_details"><img src="<?php echo base_url('uploads/cms/').$record->service_icon;?>"></span>
              </div>
              <div class="form-group">
                  <span class="col-md-2 view_label">Page Image</span>
                  <span class="col-md-10 view_details"><img src="<?php echo base_url('uploads/cms/').$record->service_img;?>"></span>
              </div>
            </div>      
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>
