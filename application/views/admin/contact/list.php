<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
           <!--  <div class="box-header with-border">   
              <div class="tddts">
                <a href="<?php //echo site_url('admin/testimonials/add');?>" class="add-btn">Add New</a>
              </div>            
            </div>   -->
            <div class="box-body">
              <table id="DataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.NO.</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>City</th>
                  <th>Type</th>
                  <th>Message</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>            
                <?php $i=1; if(!empty($records)): foreach($records as $record):?>    
                <tr>
                  <td><?php echo $i;?></td>
                   <td><?php echo !empty($record->contact_page_fname)?limit_text('50',$record->contact_page_fname):'';?></td>
                   <td><?php echo !empty($record->contact_page_lname)?limit_text('50',$record->contact_page_lname):'';?></td>
                  <td><?php echo !empty($record->contact_page_email)?limit_text('50',$record->contact_page_email):'';?></td>
                  <td><?php echo !empty($record->contact_page_phonenum)?limit_text('50',$record->contact_page_phonenum):'';?></td>
                  <td><?php echo !empty($record->contact_page_city)?limit_text('50',$record->contact_page_city):'';?></td>
                  <td><?php echo !empty($record->contact_page_type)?limit_text('50',$record->contact_page_type):'';?></td>
                  <td><?php echo !empty($record->contact_page_message)?limit_text('50',$record->contact_page_message):'';?></td>
                  <!-- <td>
                    <a href="<?php// echo !empty($record->contact_page_id)?base_url('admin/contact_page/edit/').$record->testimonials_id:'';?>"><span class="edit_icon"><i class="fa fa-pencil" aria-hidden="true"></i></span></a>
                    <a href="<?php //echo !empty($record->testimonials_id)?base_url('admin/testimonials/view/').$record->testimonials_id:'';?>"><span class="view_icon"><i class="fa fa-eye" aria-hidden="true"></i></span></a>  -->

                  <td>  <a href="<?php echo !empty($record->contact_page_id)?base_url('admin/contact/view/').$record->contact_page_id:'';?>"><span class="view_icon"><i class="fa fa-eye" aria-hidden="true"></i></span></a>


                   <a href="<?php echo !empty($record->contact_page_id)?base_url('admin/contact/delete/').$record->contact_page_id:'';?>"><span class="delete_icon"><i class="fa fa-trash" aria-hidden="true"></i></span></a>
                  </td>
                </tr>
                <?php $i++; endforeach;?>  
                <?php else:?>
                <tr>
                  <td>No Record Found</td>
                </tr>
                <?php endif;?>
                </tbody>
              </table>
            </div>
         </div>   
      </div>
    </div>
  </section>

</div>


<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script>

$(function () {
        $('#DataTable').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        })
    })

  

</script>