<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">View Contact Info</h3>
          </div>     
          <div class="col-md-6">
            <div class="box-body">
              <div class="form-group">
                  <span class="col-md-2 view_label"><strong>First Name:</strong></span>
                  <span class="col-md-10 view_details"><?php echo $record->contact_page_fname;?></span>
              </div> <br>

               <div class="form-group">
                  <span class="col-md-2 view_label"><strong>Last Name:</strong></span>
                  <span class="col-md-10 view_details"><?php echo $record->contact_page_lname;?></span>
              </div> <br>

              <div class="form-group">
                  <span class="col-md-2 view_label"><strong>Email:</strong></span>
                  <span class="col-md-10 view_details"><?php echo $record->contact_page_email;?></span>
              </div> <br>

              <div class="form-group">
                  <span class="col-md-2 view_label"><strong>Phone:</strong></span>
                  <span class="col-md-10 view_details"><?php echo $record->contact_page_phonenum;?></span>
              </div>  <br>
              
              <div class="form-group">
                  <span class="col-md-2 view_label"><strong>City:</strong></span>
                  <span class="col-md-10 view_details"><?php echo $record->contact_page_city;?></span>
              </div>  <br>

              <div class="form-group">
                  <span class="col-md-2 view_label"><strong>Type:</strong></span>
                  <span class="col-md-10 view_details"><?php echo $record->contact_page_type;?></span>
              </div>  <br>

               <div class="form-group">
                  <span class="col-md-2 view_label"><strong>Message:</strong></span>
                  <span class="col-md-10 view_details"><?php echo $record->contact_page_message;?></span>
              </div>
            
        
            </div>      
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>
