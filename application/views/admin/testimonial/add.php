<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add Testimonial</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/testimonial/add')?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control" id="testimonial_name" name="testimonial_name" required>
                  <?php echo form_error('testimonial_name'); ?>
                </div>
                
                <div class="form-group">
                  <label>Testimonial Text</label>
                  <textarea class="editor form-control" rows="3" id="testimonial_text" name="testimonial_text" required><?php echo !empty($record->testimonial_text)?$record->testimonial_text:''?></textarea>
                  <?php echo form_error('testimonial_text'); ?>
                </div>

                 <div class="form-group">
                  <label>Testimonial Heading</label>
                  <input type="text" class="form-control" id="testimonial_heading" name="testimonial_heading" required>
                  <?php echo form_error('testimonial_heading'); ?>
                </div>
                
                <div class="form-group">
                    <label>Testimonial Image</label>
                    <div class="input-group-btn">
                      <div class="image-upload">                      
                        <img src="<?php echo base_url('assets/admin/img/placeholder.png')?>">
                        <div class="file-btn">
                          <input type="file" id="testimonial_image" name="testimonial_image">
                          <label class="btn btn-info">Upload</label>
                        </div>
                      </div>
                    </div>
                    <?php echo form_error('testimonial_image'); ?>
                </div>
              </div>
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>