<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">View Category</h3>
          </div>     
          <div class="col-md-6">
            <div class="box-body">
              <div class="form-group">
                  <span class="col-md-2 view_label"><strong>Name</strong></span>
                  <span class="col-md-10 view_details"><?php echo $category->category_text;?></span>
              </div>
              <div class="form-group">
                  <span class="col-md-2 view_label"><strong>Banner Image</strong></span>
                  <span class="col-md-10 view_details"><img style="width:500px;height:250px;" src="<?php echo base_url('uploads/cms/').$category->category_banner?>"></span>
              </div>
              <div class="form-group">
                  <span class="col-md-2 view_label"><strong>Cover Image</strong></span>
                  <span class="col-md-10 view_details"><img style="width:500px;height:250px;" src="<?php echo base_url('uploads/cms/').$category->category_img?>"></span>
              </div>
              <?php $i=1;if(!empty($album_images)): foreach($album_images as $img):?>
              <div class="form-group">
                  <span class="col-md-2 view_label"><strong>Image <?php echo $i;?></strong></span>
                  <span class="col-md-10 view_details"><img style="width:500px;height:250px;" src="<?php echo base_url('uploads/cms/').$img->category_page_img?>"></span>
              </div>
              <?php $i++;?>
              <?php endforeach; endif;?>
            </div>      
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>
