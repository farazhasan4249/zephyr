<script>
$(document).ready(function (){
    $(".image-upload").on("change","input[type=file]",function() {      
      $(this).parents('.pro_images').children('.col-md-4').children('.image-up').html('<img style="max-width:600px;" class="preview" src="' + URL.createObjectURL(this.files[0]) + '" /><i class="fa fa-times" aria-hidden="true"></i>');
    });

    $(".image-upload").on("click",".image-up i.fa.fa-times",function() {  
      $(this).parents('.image-up').html('');
      $(this).parents('.pro_images').children('input[type=file]').val('');

    });   

    $(".multiimage-upload").on("change","input[type=file]",function(e) {
        
        var min = count;
        
    for(var i=0; i < this.files.length; i++)
    {
        var file = this.files[i];
         console.log(file);
         var fileType = file["type"];
        var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
        if ($.inArray(fileType, ValidImageTypes) < 0) 
        {
            alert_danger("Invalid File Format");
        }
        else
        {
            if( i == min-count )
            {
                $('#multi-image-upload_'+count).children('img').attr('src',URL.createObjectURL(file));
                $('#multi-image-upload_'+count).append('<i class="fa fa-times" aria-hidden="true"></i>');
                $('#multi-image-upload_'+count).children('.file-btn').hide();
                $('#multi-image-upload_'+count).children('.file-btn').children(':file').removeAttr('required');
                
            }
            else
            {
                $('#multi-image-upload_'+count).children('img').attr('src',URL.createObjectURL(file));
            }
            
            var multimg = $('#multi-image-upload_'+count);
            count++;
            if( i == this.files.length-1 )
            {
                multimg.parents('.up').parents('.image-up').append('<div class="up"><div class="multi-image-upload col-md-3" id="multi-image-upload_'+count+'"><img src="<?php echo base_url('assets/admin/img/placeholder.png')?>"><div class="file-btn"><input type="file" id="category_page_img" name="category_page_img[]" multiple><label class="btn btn-info">Upload</label></div></div></div>');
               
            }
            else
            {
                
                multimg.parents('.up').append('<div class="multi-image-upload col-md-3" id="multi-image-upload_'+count+'"><img src="<?php echo base_url('assets/admin/img/placeholder.png')?>"></div>');    
                
            }
        }  
        
    }
    });

    $(".multiimage-upload").on("click",".image-up i.fa.fa-times",function() {
        
        var str = $(this).parents('.col-md-3').parents('.up');
        // console.log(str);
        str.remove();
        
    }); 
  $("form").on("change","#category_text",function() {   
    var str = $(this).val();
    str = str.trim().replace(/\s+/g, '-').toLowerCase();
    $('#category_slug').val(str);
  });
});

</script>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add Category</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/category/add')?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">
                <div class="form-group">
                  <label>Name:</label>
                  <input type="text" class="form-control" id="category_text" name="category_text" required>
                  <input type="hidden" class="form-control" id="category_slug" name="category_slug">
                  <?php echo form_error('category_text'); ?>
                </div>
                <div class="form-group">
                    <label>Banner Image</label>
                    <div class="input-group-btn">
                      <div class="image-upload">                      
                        <img src="<?php echo base_url('assets/admin/img/placeholder.png')?>">
                        <div class="file-btn">
                          <input type="file" id="category_banner" name="category_banner">
                          <label class="btn btn-info">Upload</label>
                        </div>
                      </div>
                    </div>
                    <?php echo form_error('category_banner'); ?>
                </div>
                <div class="form-group">
                    <label>Cover Image</label>
                    <div class="input-group-btn">
                      <div class="image-upload">                      
                        <img src="<?php echo base_url('assets/admin/img/placeholder.png')?>">
                        <div class="file-btn">
                          <input type="file" id="category_img" name="category_img">
                          <label class="btn btn-info">Upload</label>
                        </div>
                      </div>
                    </div>
                    <?php echo form_error('category_img'); ?>
                </div>
                <div class="form-group multiimage-upload">
                  <label>Image</label>
                  <div class="input-group-btn row image-up" id="multiimageview" >
                      <script>
                        var count = 0;
                    </script>
                  <?php if(!empty($images)): foreach($images as $img):?>
                  <div class="multi-image-upload col-md-3">   
                     <i class="fa fa-times" aria-hidden="true"></i>                      
                     <img src="<?php echo !empty($img->category_page_img)?base_url('uploads/cms/').$img->category_page_img:base_url('assets/admin/img/placeholder.png')?>">
                    <div class="file-btn">
                      <input type="text" id="category_page_img" name="category_page_img[]" value="<?php echo !empty($img->category_page_img)?$img->category_page_img:''?>" hidden>
                    </div>
                  </div>
                  <?php endforeach; endif;?>
                  <div class="up 1">
                    <div class="multi-image-upload col-md-3" id="multi-image-upload_0">
                        <img src="<?php echo base_url('assets/admin/img/placeholder.png')?>">
                        <div class="file-btn">
                            <input type="file" id="category_page_img" name="category_page_img[]" multiple >
                            <label class="btn btn-info">Upload</label>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
               <?php echo form_error('category_page_img'); ?>
              <!--  <div class="form-group multiimage-upload">-->
              <!--    <label>Image</label>-->
              <!--    <div class="input-group-btn row image-up" >-->
              <!--    <?php if(!empty($images)): foreach($images as $img):?>-->
              <!--    <div class="multi-image-upload col-md-3">   -->
              <!--       <i class="fa fa-times" aria-hidden="true"></i>                        -->
              <!--       <img src="<?php echo !empty($img->category_page_img)?base_url('uploads/cms/').$img->category_page_img:base_url('assets/admin/img/placeholder.png')?>">-->
              <!--      <div class="file-btn">-->
              <!--        <input type="text" id="category_page_img" name="category_page_img[]" value="<?php echo !empty($img->category_page_img)?$img->category_page_img:''?>" hidden>-->
              <!--      </div>-->
              <!--    </div>-->
              <!--    <?php endforeach; endif;?>                  -->
              <!--    <div class="multi-image-upload col-md-3">                      -->
              <!--      <img src="<?php echo base_url('assets/admin/img/placeholder.png')?>">-->
              <!--      <div class="file-btn">-->
              <!--        <input type="file" id="category_page_img" name="category_page_img[]" multiple>-->
              <!--       <label class="btn btn-info">Upload</label>-->
              <!--      </div>-->
              <!--    </div>-->
              <!--  </div>-->
              <!--</div>-->

              </div>
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>