<?php if !empty($forsale): ?>
<div class="clearfix"></div>
<section id="listing1" class="listing1">
  <div class="custom-top">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-12 col-xs-12">
          <div class="row">
            <?php if(isset($records) && !empty($records)) { ?>
              <?php foreach($records as $record) { ?>

                <div class="col-sm-6">

                  <div class="property_item heading_space">
                    <div class="property_head text-center">
                      <h3 class="captlize"><?php echo base_url($record->forsale_home_heading)?$record->forsale_home_heading:''?></h3>
                      <p><?php echo base_url($record->forsale_home_subheading)?$record->forsale_home_subheading:''?></p>
                    </div>
                    <div class="image"> <a href="#"> <img src="<?php echo base_url('assets/front/images/')?>listing7.jpg" alt="latest property" class="img-responsive"></a>
                      <div class="price clearfix"> <span class="tag">For Sale</span> </div>
                    </div>
                    <div class="proerty_content">
                      <div class="property_meta"> <span><i class="icon-select-an-objecto-tool"></i><?php echo base_url($record->forsale_squarefeet)?$record->forsale_squarefeet:''?> sq ft</span> <span><i class="icon-bed"></i><?php echo base_url($record->forsale_bedroom)?$record->forsale_bedroom:''?> Bedrooms</span> <span><i class="icon-safety-shower"></i><?php echo base_url($record->forsale_bathroom)?$record->forsale_bathroom:''?> Bathrooms</span> </div>
                      <div class="proerty_text">
                        <p><?php echo base_url($record->forsale_text)?$record->forsale_text:''?> </p>
                      </div>
                      <div class="favroute clearfix">
                        <p class="pull-md-left"><?php echo base_url($record->forsale_price)?$record->forsale_price:''?></p>
                        <ul class="pull-right">
                          <li><a href="#"><i class="icon-like"></i></a></li>
                          <li><a href="#" class="share_expender" ><i class="icon-share3"></i></a></li>
                        </ul>
                      </div>

                    </div>

                  </div>

                </div>
              <?php }}?>

            </div>
          </div>
          <?php endif; ?>