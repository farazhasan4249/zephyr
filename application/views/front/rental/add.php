<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12  text-center ">
        <div class="box-header with-border">
            <h3 class="box-title"><strong>Add Property For Rent-Out</strong></h3><br>
          </div>
      </div>
      <div class="col-md-12">
        <div class="box box-primary">
          <!-- <div class="box-header with-border">
            <h3 class="box-title"><strong>Add Property For Rent-Out</strong></h3><br>
          </div>  -->    
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('rental/add');?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">              
                <div class="form-group">
                  <label>Rental Home Heading</label>
                  <input type="name" class="form-control" id="rental_home_heading" name="rental_home_heading" value="<?php echo !empty($record->rental_home_heading)?$record->rental_home_heading:''?>" required>
                  <?php echo form_error('rental_home_heading'); ?>
                </div>  

                <div class="form-group">
                  <label>Rental Home Sub-Heading</label>
                  <input type="name" class="form-control" id="rental_home_subheading" name="rental_home_subheading" value="<?php echo !empty($record->rental_home_subheading)?$record->rental_home_subheading:''?>" required>
                  <?php echo form_error('rental_home_subheading'); ?>
                </div>  

                  
                  <div class="form-group">
                <label>Rental Image</label>
                <div class="input-group-btn">
                  <div class="image-upload">                      
                    <img src="<?php echo !empty($record->rental_image)?base_url('uploads/images/').$record->rental_image:base_url('assets/admin/img/placeholder.png')?>" class="img-responsive">
                    <div class="file2-btn">
                      <button>
                      <input type="file" id="rental_image" name="rental_image" placeholder="upload">
                       <h5>upload</h5>

                       </button>
                     </div>
                  </div>
                </div>
                <?php echo form_error('rental_image'); ?>
              </div>  

              <div class="form-group">
                  <label>Rental Sq. Ft.</label>
                  <input type="name" class="form-control" id="rental_squarefeet" name="rental_squarefeet" value="<?php echo !empty($record->rental_squarefeet)?$record->rental_squarefeet:''?>" required>
                  <?php echo form_error('rental_squarefeet'); ?>
                </div>

                <div class="form-group">
                  <label>Rental Bedroom</label>
                  <input type="name" class="form-control" id="rental_bedroom" name="rental_bedroom" value="<?php echo !empty($record->rental_bedroom)?$record->rental_bedroom:''?>" required>
                  <?php echo form_error('rental_bedroom'); ?>
                </div>

                 <div class="form-group">
                  <label>Rental Bathroom</label>
                  <input type="name" class="form-control" id="rental_bathroom" name="rental_bathroom" value="<?php echo !empty($record->rental_bathroom)?$record->rental_bathroom:''?>" required>
                  <?php echo form_error('rental_bathroom'); ?>
                </div>

                 

              <div class="form-group">
                  <label>Rental Price</label>
                  <input type="name" class="form-control" id="rental_price" name="rental_price" value="<?php echo !empty($record->rental_price)?$record->rental_price:''?>" required>
                  <?php echo form_error('rental_price'); ?>
                </div>

                <div class="form-group">
                  <label>Rental Text</label>
                   <textarea class="editor form-control" rows="3" id="rental_text" name="rental_text" required><?php echo !empty($record->rental_text)?$record->rental_text:''?></textarea>
                  <?php echo form_error('rental_text'); ?>
                </div>

                <div class="form-group">
                  <label>Rental Keyword</label>
                  <input type="name" class="form-control" id="rental_keyword" name="rental_keyword" value="<?php echo !empty($record->rental_keyword)?$record->rental_keyword:''?>" required>
                  <?php echo form_error('rental_keyword'); ?>
                </div>

                <div class="form-group">
                  <label>Rental location</label>
                  <input type="name" class="form-control" id="rental_location" name="rental_location" value="<?php echo !empty($record->rental_location)?$record->rental_location:''?>" required>
                  <?php echo form_error('rental_location'); ?>
                </div>

                 <div class="form-group">
                  <label>Rental Property Type</label>
                  <input type="name" class="form-control" id="rental_property_type" name="rental_property_type" value="<?php echo !empty($record->rental_property_type)?$record->rental_property_type:''?>" required>
                  <?php echo form_error('rental_property_type'); ?>
                </div>

                <div class="form-group">
                  <label>Rental Property Status</label>
                  <input type="name" class="form-control" id="rental_property_status" name="rental_property_status" value="<?php echo !empty($record->rental_property_status)?$record->rental_property_status:''?>" required>
                  <?php echo form_error('rental_property_status'); ?>
                </div>

                <div class="form-group">
                  <label>Rental Maximum Area</label>
                  <input type="name" class="form-control" id="rental_maximum_area" name="rental_maximum_area" value="<?php echo !empty($record->rental_maximum_area)?$record->rental_maximum_area:''?>" required>
                  <?php echo form_error('rental_maximum_area'); ?>
                </div>



              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>