

	<div class="about-banner">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h4><?php echo base_url($about->about_heading)?$about->about_heading:''?></h4>
					<p><?php echo base_url($about->about_content)?$about->about_content:''?></p>
				</div>
			</div>
		</div>
	</div>

	<div class="clearfix"></div>

	<section>
		<div class="abot-bg">
			<div class="container">
				<div class="row">

					<div class="col-md-6 col-sm-6 col-xs-12 no_span">
						<h3><?php echo base_url($about->about_second_heading)?$about->about_second_heading:''?></h3>
						<div class="borline"></div>
						<p><?php echo base_url($about->about_second_content)?$about->about_second_content:''?></p>

					</div>

					<div class="col-md-6 col-sm-6 col-xs-12"><img src="<?php echo base_url('assets/front/images/')?>about01.jpg" class="img-responsive" />

					</div>

				</div>
			</div>
		</div>
	</section>


	<section>
		<div class="second-sec">
			<div class="container">
				<div class="second-text">


					<h2 class="ml10"> <span class="text-wrapper"> <span class="letters">Our Services</span> </span> </h2>

					<p>Psum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
				</div>
				<div class="row">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
						<div class="min-icon"> <img src="<?php echo base_url('assets/front/images/')?>icon01.png" class="img-responsive center-block">
							<h3>Sale Service</h3>
							<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
							<h5><a href="#">READ MORE</a></h5>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
						<div class="min-icon"> <img src="<?php echo base_url('assets/front/images/')?>icon02.png" class="img-responsive center-block">
							<h3>Renting Service</h3>
							<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
							<h5><a href="#">READ MORE</a></h5>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
						<div class="min-icon"> <img src="<?php echo base_url('assets/front/images/')?>icon03.png" class="img-responsive center-block">
							<h3>Property Management</h3>
							<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
							<h5><a href="#">READ MORE</a></h5>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
						<div class="min-icon"> <img src="<?php echo base_url('assets/front/images/')?>icon04.png" class="img-responsive center-block">
							<h3>Free Market Analysis</h3>
							<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
							<h5><a href="#">READ MORE</a></h5>
						</div>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 center">
						<button>View More</button>
					</div>
				</div>
			</div>
		</div>
	</section>



<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery-2.1.4.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/bootstrap.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.parallax-1.1.3.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.appear.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/bootsnav.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/masonry.pkgd.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.cubeportfolio.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/range-Slider.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/owl.carousel.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/selectbox-0.2.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/zelect.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.fancybox.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.tools.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.revolution.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.layeranimation.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.navigation.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.parallax.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.slideanims.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.video.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/custom.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/functions.js"></script>
</body>
</html>
