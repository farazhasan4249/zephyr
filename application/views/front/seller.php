<section>

<div class="container">
<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
		<div class="buyer-form">
			<h3 class="buyer-title">Seller Form</h3>
			<form action="<?php echo base_url('seller/seller')?>" method="post">
				<div class="form-group">
					<div class="row">
					  	<div class="col-sm-6 col-xs-12">
					  		<input type="text" class="form-control input-lg" id="seller_firstname" name="seller_firstname" placeholder="First Name">
					  	</div>
					  	<div class="col-sm-6 col-xs-12">
					  		<input type="text" class="form-control input-lg" id="seller_lastname" name="seller_lastname" placeholder="Last Name">
					  	</div>
					</div>
				</div>

				<div class="form-group">
					<input type="text" class="form-control input-lg" id="seller_address" name="seller_address" placeholder="Property Address">
				</div>

				<div class="form-group">
				    <input type="number" class="form-control input-lg" id="seller_phone" name="seller_phone" placeholder="Phone Number">
				</div>

				<div class="form-group">
					<input type="email" class="form-control input-lg" id="seller_email" name="seller_email" placeholder="Email Address">
				</div>

				<div class="form-group">
				  <textarea class="form-control input-lg" rows="5" id="seller_message" name="seller_message" placeholder="Type Your message"></textarea>
				</div>

				<div class="form-group">
				  <button type="submit" class="form-control btn-buyer">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>

</section>
