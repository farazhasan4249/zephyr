

<div class="contact-banner">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h4>contact us</h4>
        <p>LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT. INTEGER FERMENTUM AUGUE AC MI IMPERDIET, AT SODALES NIBH VULPUTATE. NULLA QUIS FELIS LOREM. NULLAM AT FELIS AT ERAT VULPUTATE RHONCUS UT AC LIBERO.</p>
      </div>
    </div>
  </div>
</div>

<div class="clearfix"></div>


<section>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 col-sm-12 col-xs-12 no-spas">
        <div class="box5">
          <h1>Address</h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
            eiusmod tempor incididunt ut labore et dolore magna aliqua. 
          Ut enim ad minim veniam, quis nostrud exercitation </p>
          <div class="border2">
            <h5>BeBuilder Tempor dictum</h5>
            <h4>Level 13, 2 Lorem St,<br>
              Ipsum, Text, 4000<br>
            USA </h4>
            <h6>E-mail: <a href="mailto:info@southernliving.com">info@zepher living.com</a></h6>
            <h6> Call Us :<a href="#">+64 (0) 38 376 6284</a> </h6>
          </div>
        </div>
      </div>


      <div class="col-md-6 col-sm-12 col-xs-12 no_span">
       
        <div class="box2">
          <h1>Send A Message</h1>
          <div class="clearfix"></div>
          <div class="col-md-10 col-sm-12 col-xs-12 padding">
            <div class="top"></div>
            <div class="row">
             <form action="<?php echo base_url('contact/contact')?>" method="post">
              <div class="col-md-6 col-sm-12 col-xs-12 padding">
                <input type="text" id="contact_page_fname" name="contact_page_fname" placeholder="First Name">
              </div>
              <div class="col-md-6 col-sm-12 col-xs-12 padding-right ">
                <input type="text" id="contact_page_lname" name="contact_page_lname" placeholder="Last Name">
              </div>
              <div class="col-md-6 col-sm-12 col-xs-12 padding-right ">
                <input type="email" id="contact_page_email" name="contact_page_email" placeholder="Email">
              </div>
              <div class="col-md-6 col-sm-12 col-xs-12 padding-right ">
                <input type="text" id="contact_page_phonenum" name="contact_page_phonenum" placeholder="Phone Number">
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 padding">
                <select id="contact_page_city" name="contact_page_city">
                  <option>Search By City</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                </select>
              </div>
              
              <div class="col-md-12 col-sm-12 col-xs-12 padding">
                <select id="contact_page_type" name="contact_page_type">
                  <option>--Select--</option>
                  <option>Buyer</option>
                  <option>Seller</option>
                </select>
              </div>  

              <div class="col-md-12 col-sm-12 col-xs-12 padding">
                <textarea title="text" id="contact_page_message" name="contact_page_message" placeholder="Message"></textarea>
              </div>

              <div class="col-md-5 col-sm-12 col-xs-12">
               <div class="booton">
                <button>Send a message</button>
              </div>

            </div>
          </form>
        </div>
        
        
        
        
        

      </div>

    </div>
  </div>
</div>
</div>
</section>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no_span">
  <iframe src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=<?php echo str_replace(",", "", str_replace(" ", "+", $this->address)) ?>&z=14&output=embed" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>





<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery-2.1.4.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/bootstrap.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.parallax-1.1.3.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.appear.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/bootsnav.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/masonry.pkgd.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.cubeportfolio.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/range-Slider.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/owl.carousel.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/selectbox-0.2.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/zelect.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.fancybox.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.tools.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.revolution.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.layeranimation.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.navigation.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.parallax.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.slideanims.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.video.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/custom.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/functions.js"></script>
</body>
</html>
