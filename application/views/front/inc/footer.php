

<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
          <div class="footer-logo"> <img src="<?php echo base_url('assets/front/images/')?>footer-logo.png"> </div>
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
          <div class="footer-link">
            <h5>Quick Links</h5>
            <ul>
              <li><a href="<?php echo base_url('home')?>">home</a></li>
              <li><a href="<?php echo base_url('faq')?>">terms of use</a></li>
              <li><a href="<?php echo base_url('about')?>">about us</a></li>
              <li><a href="privacy.html">privacy policy</a></li>
              
            </ul>
          </div>
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
          <div class="footer-link02">
            <ul>
              <li><a href="<?php echo base_url('faq')?>">FAQs</a></li>
              <li><a href="#">Blog</a></li>
              
              <li><a href="<?php echo base_url('contact')?>">Contact</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
          <div class="footer-link03">
            <h5>Contact Us</h5>
            <ul>
              <li><i class="fa fa-location-arrow"></i> <?php echo base_url($this->address)?$this->address:''?></li>
              <li><i class="fa fa-phone"></i> <?php echo base_url($this->phone)?$this->phone:''?></li>
              <li><i class="fa fa-envelope"></i> <?php echo base_url($this->email_address)?$this->email_address:''?></li>
            </ul>
          </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
          <div class="footer-text">
            <h5>Subscribe to our newsletter</h5>
            <p>Subscribe to our newsletter to get an update our latest properties.</p>
            <form action="newsletter" method="post">
            <input type="email" id="newsletter_email" name="newsletter_email" placeholder="enter your email">
            <button>Subscribe</button>
          </form>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no_span">
      <div class="copyright02">
        <p>Copyright © 2019 <a href="#">ZEPHYR REALTY INVESTMENT </a>. All rights reserved.</p>
      </div>
    </div>
  </div>

</footer>
