<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title><?php echo $this->site_title; ?></title>
<link rel="icon" href="<?php echo base_url('assets/front/images/')?>icon.png">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/reality-icon.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/bootsnav.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/custom-style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/cubeportfolio.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/jquery.fancybox.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/owl.transitions.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/settings.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/range-Slider.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/search.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
<?php if($this->session->flashdata('success')):?>
<script>alert_success("<?php echo $this->session->flashdata('success')?>");</script>
<?php endif;?>
<?php if($this->session->flashdata('error')):?>
<script>alert_danger("<?php echo $this->session->flashdata('error')?>");</script>
<?php endif;?> 




<div class="modal" id="myModal">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 center">
  <div class="buyer-form">
      <h3 class="buyer-title">Seller Form</h3>
      <form action="<?php echo base_url('forsale/seller')?>" method="post">
        <div class="form-group">
          <div class="row">
              <div class="col-sm-6 col-xs-12">
                <input type="text" class="form-control input-lg" id="seller_firstname" name="seller_firstname" placeholder="First Name">
              </div>
              <div class="col-sm-6 col-xs-12">
                <input type="text" class="form-control input-lg" id="seller_lastname" name="seller_lastname" placeholder="Last Name">
              </div>
          </div>
        </div>

        <div class="form-group">
          <input type="text" class="form-control input-lg" id="seller_address" name="seller_address" placeholder="Property Address">
        </div>

        <div class="form-group">
            <input type="number" class="form-control input-lg" id="seller_phone" name="seller_phone" placeholder="Phone Number">
        </div>

        <div class="form-group">
          <input type="email" class="form-control input-lg" id="seller_email" name="seller_email" placeholder="Email Address">
        </div>

        <div class="form-group">
          <textarea class="form-control input-lg" rows="5" id="seller_message" name="seller_message" placeholder="Type Your message"></textarea>
        </div>

        <div class="form-group">
          <button type="submit" class="form-control btn-buyer">Submit</button>
        </div>
      </form>
    </div>
    <div class="modal-footer">
        <button data-dismiss="modal">X</button>
    </div>
  </div>
  </div>
</div>
</div>

<!-- top header  -->
<div class="top-header">
  <div class="container">
  <marquee><label>For Property Maintenance, Bill Payments or Signing contracts, Visit Us:  <a href="https://zephyrinvest.tenantcloud.com/" target="_blank"> https://zephyrinvest.tenantcloud.com/</a></label></marquee>
  </div>
</div>


<header class="navigation-box">
  <div class="min-div">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-9">

          <a href="<?php echo base_url('home')?>"><img src="<?php echo base_url('assets/front/images/')?>logo.png" class="img-responsive"></a></div>
        <div class=" hidden visible-xs toggle col-xs-2"><img src="<?php echo base_url('assets/front/images/')?>menu.png" class="img-responsive"></div>
        <div class="col-md-9 col-sm-9 col-xs-12">
          <div class="custom-nav">
            <ul>
              <?php if (!$this->session->userdata('customer_email')): ?>
              <li ><a class="active"  href="<?php echo base_url('home')?>">Home </a></li>
              <li><a href="<?php echo base_url('about')?>"> About Us</a> </li>
              <li><a href="<?php echo base_url('foreclosure')?>"> Foreclosure Home</a></li>
              <li><a href="<?php echo base_url('forsale')?>"> Selling Home </a></li>
              <li><a href="<?php echo base_url('rental')?>"> Rentals Rent</a></li>
              <li><a href="<?php echo base_url('faq')?>"> FAQs </a> </li>
              <li><a href="#"> Blog </a> </li>
              <li><a href="<?php echo base_url('contact')?>"> Contact Us </a></li>
              <li><a href="<?php echo base_url('register')?>"> Register </a></li>
             <li><a href="<?php echo base_url('login')?>"><i class="fa fa-user"></i></a> </li>
             <?php else: ?>
              <li ><a class="">
              <li><a href="<?php echo base_url('home')?>"> Home</a></li> 
              <li><a href="<?php echo base_url('buyer')?>"> Our App </a></li> 
              <li><a href="<?php echo base_url('forsale/add')?>"> For Sale</a></li> 
              <li><a href="<?php echo base_url('rental/add')?>"> Rentals Rent</a></li>
              <!-- <li><a href="<?php //echo base_url('seller')?>"> Sell Home</a></li> -->
              <!-- <li><a href="<?php //echo base_url('lend')?>"> Lend/Leasing </a></li>
              <li><a href="<?php //echo base_url('property')?>"> Property Management</a></li> -->
              <li><a href="<?php echo base_url('foreclosure/add')?>"> Foreclosure Home</a></li>
              <!-- <li><a href="<?php //echo base_url('selling')?>"> Selling Home </a></li> -->
              <li><a href="<?php echo base_url('register/logout')?>"> <i class="fa fa-sign-out" title="logout"></i> </a></li>
              <?php endif; ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>