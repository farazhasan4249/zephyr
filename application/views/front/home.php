
<div class="min-banner">
  <div class="container">
    <div class="banner-text">
      <h1>WELCOME TO</h1>
      <h2 class="ml7"> <span class="text-wrapper"> <span class="letters"><?php echo base_url($home->home_heading)?$home->home_heading:''?></span> </span> </h2>
      <p><?php echo base_url($home->home_content)?$home->home_content:''?></p>
    </div>
    <div class="min-filde">
      <h2>Search Property</h2>
    <form action="<?php echo base_url('home')?>" method="post">
      <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-12">
          <label>Select Your</label>
          <input type="text" id="home_location" name="home_location" placeholder="Location">
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
          <label>Select Your</label>
          <input type="text" id="home_area" name="home_area" placeholder="Area">
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
          <label>Select Your</label>
          <input type="text" id="home_budget" name="home_budget" placeholder="Buget">
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
          <button>SEARCH</button>
        </div>

      </div>
    </form>
    </div>
    </div>
  </div>
</div>
<section>
  <div class="frist-sec">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9">
          <div class="frist-sec-text">
      <h4 class="ml10"> <span class="text-wrapper"><span class="letters">
        <?php echo base_url($home->home_second_heading)?$home->home_second_heading:''?></span> </span> </h4>
            <p><?php echo base_url($home->home_second_content)?$home->home_second_content:''?></p>
            <button>read more</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="second-sec">
    <div class="container">
      <div class="second-text">
        <h2 class="ml10"> <span class="text-wrapper"><span class="letters">Our Services</span> </span> </h2>
        <p>Psum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
      </div>
      <div class="row">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <div class="min-icon"> <img src="<?php echo base_url('assets/front/images/')?>icon01.png" class="img-responsive center-block">
            <h3>Sale Service</h3>
            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
            <h5><a href="#">READ MORE</a></h5>
          </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <div class="min-icon"> <img src="<?php echo base_url('assets/front/images/')?>icon02.png" class="img-responsive center-block">
            <h3>Renting Service</h3>
            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
            <h5><a href="#">READ MORE</a></h5>
          </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <div class="min-icon"> <img src="<?php echo base_url('assets/front/images/')?>icon03.png" class="img-responsive center-block">
            <h3>Property Management</h3>
            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
            <h5><a href="#">READ MORE</a></h5>
          </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <div class="min-icon"> <img src="<?php echo base_url('assets/front/images/')?>icon04.png" class="img-responsive center-block">
            <h3>Free Market Analysis</h3>
            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
            <h5><a href="#">READ MORE</a></h5>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 center">
          <button>View More</button>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="min-three">
    <div class="container">
      <div class="three-text">
        <h2 class="ml10"> <span class="text-wrapper"> <span class="letters">Blog and Testimonial</span> </span> </h2>
        <p>We take a broader, more holistic all</p>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="test">
            <div class="owl-carousel owl-theme">
        <?php if(isset($testimonial) && !empty($testimonial)) { ?>
        <?php foreach($testimonial as $test) { ?> 
              <div class="item">
                <div class="test-text">
                  <h2><?= $test->testimonial_heading?></h2>
                  <p><?= $test->testimonial_text?></p>
                  <div class="col-md-4 col-sm-4 col-xs-12 center"> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> </div>
                  <img src="<?=  base_url('uploads/settings/').$test->testimonial_image ?>" class="img-responsive center">
                  <h3><?= $test->testimonial_name?></h3>
                </div>
              
              </div>
           <?php }}?>
            </div>      
          </div>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="last-sec">
            <div class="col-md-8 col-sm-8 col-xs-12 pull-right"> <img src="<?php echo base_url('assets/front/images/')?>last01.jpg" class="img-responsive "> </div>
            <div class="col-md-4 col-sm-4 col-xs-12 ">
              <div class="box-new">
                <h2>Posted by Lorem Ipsum, 20/12/17</h2>
                <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer.</p>
                <h4><a href="#">READ MORE</a></h4>
              </div>
            </div>
          </div>
          <div class="last-sec">
            <div class="col-md-8 col-sm-8 col-xs-12 pull-right"> <img src="<?php echo base_url('assets/front/images/')?>last02.jpg" class="img-responsive "> </div>
            <div class="col-md-4 col-sm-4 col-xs-12 ">
              <div class="box-new">
                <h2>Posted by Lorem Ipsum, 20/12/17</h2>
                <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer.</p>
                <h4><a href="#">READ MORE</a></h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery-2.1.4.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/bootstrap.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.parallax-1.1.3.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.appear.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/bootsnav.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/masonry.pkgd.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.cubeportfolio.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/range-Slider.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/owl.carousel.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/selectbox-0.2.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/zelect.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.fancybox.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.tools.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.revolution.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.layeranimation.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.navigation.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.parallax.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.slideanims.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.video.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/custom.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/functions.js"></script>
</body>
</html>
