<div class="selling-banner">
  <div class="container">
    <h1 class="ml7"> <span class="text-wrapper"> <span class="letters">Selling Home </span> </span> </h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque rutrum libero sed finibus volutpat. Quisque finibus interdum purus, quis mollis odio consectetur vitae. Ut feugiat risus erat, in tincidunt est rutrum vitae. Donec in fermentum ligula, sed bibendum ante. </p>
</div>
</div>
<div class="clearfix">
</div>
<section id="listing1" class="listing1">
    <div class="custom-top">
        <div class="container">
            <div class="row">  
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="row">
                      <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
                        <div class="custom-btn-modal">
                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Sell Your Home</button>
                        </div>
                    </div>
                    <?php if(isset($records) && !empty($records)) { ?>
                        <?php foreach($records as $record) { ?>

                            <div class="col-sm-6">

                                <div class="property_item heading_space">
                                    <div class="property_head text-center">
                                        <h3 class="captlize"><?php echo base_url($record->forsale_home_heading)?$record->forsale_home_heading:''?></h3>
                                        <p>
                                            <?php echo base_url($record->forsale_home_subheading)?$record->forsale_home_subheading:''?>
                                        </p>
                                    </div>
                                    <div class="image widh"> <a href="#"> <img src="<?php echo base_url('uploads/settings/').$record->forsale_image?>" alt="latest property" class="img-responsive"></a>
                                        <div class="price clearfix"> <span class="tag">For Sale</span> </div>
                                    </div>
                                    <div class="proerty_content">
                                        <div class="property_meta"> <span><i class="icon-select-an-objecto-tool"></i><?php echo base_url($record->forsale_squarefeet)?$record->forsale_squarefeet:''?> sq ft</span> <span><i class="icon-bed"></i><?php echo base_url($record->forsale_bedroom)?$record->forsale_bedroom:''?> Bedrooms</span> <span><i class="icon-safety-shower"></i><?php echo base_url($record->forsale_bathroom)?$record->forsale_bathroom:''?> Bathrooms</span> </div>
                                        <div class="proerty_text">
                                            <p>
                                                <?php echo base_url($record->forsale_text)?$record->forsale_text:''?>
                                            </p>
                                        </div>
                                        <div class="favroute clearfix">
                                            <p class="pull-md-left">
                                                <?php echo base_url($record->forsale_price)?$record->forsale_price:''?>
                                            </p>
                                            <ul class="pull-right">
                                                <li><a href="#"><i class="icon-like"></i></a></li>
                                                <!-- <li><a href="#" class="share_expender"><i class="icon-share3"></i></a></li> -->
                                            </ul>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        <?php }}?>

                   



                                   <!--  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <ul class="pagination pagination-cus">
                                            <li class="active"><a href="#">1</a></li>
                                            <li><a href="?per_page=2" data-ci-pagination-page="2">2</a></li>
                                            <li><a href="?per_page=2" data-ci-pagination-page="2" rel="next">&gt;</a></li>
                                        </ul>
                                    </div> -->
                                </div>
                            </div>





                            <ul class="ProductView filter_data">

                            </ul>

                <!-- <div align="center" id="pagination_link">

                </div> -->

                <aside class="col-md-4 col-xs-12">
                    <div class="property-query-area clearfix">
                        <div class="col-md-12">
                            <h3 class="text-uppercase bottom20 top15">Advanced Search</h3>
                        </div>
                        <form class="callus" action="<?php echo base_url('forsale/search')?>" method="get">
                            <div class="single-query form-group col-sm-12">
                                <input type="text" id="forsale_keyword" name="forsale_keyword" class="keyword-input" placeholder="Keyword ">
                            </div>
                            <div class="single-query form-group col-sm-12">
                                <div class="intro">
                                    <input type="text" id="forsale_location" name="forsale_location" class="keyword-input" placeholder="location ">
                                    <!--  <select onchange="get_filter()" id="forsale_location" name="forsale_location">
                      <option selected="" value="any">Location</option>
                      <option>location</option>
                      <option>UK </option>
                  </select> -->
              </div>
          </div>
          <div class="single-query form-group col-sm-12">
            <div class="intro">
                <input type="text" id="forsale_property_type" name="forsale_property_type" class="keyword-input" placeholder="Property Type ">

                                    <!--  <select onchange="get_filter()" id="forsale_property_type" name="forsale_property_type">

                      <option class="active">Property Type</option>
                      <option>All areas</option>
                  </select> -->

              </div>
          </div>
          <div class="single-query form-group col-sm-12">
            <div class="intro">
                <input type="text" id="forsale_property_status" name="forsale_property_status" class="keyword-input" placeholder="Property Status ">
                                    <!--   <select onchange="get_filter()" id="forsale_property_status" name="forsale_property_status">
                      <option class="active">Property Status</option>
                      <option>All areas</option>
                  </select> -->
              </div>
          </div>
          <div class="search-2 col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="single-query form-group">
                        <div class="intro">
                            <input type="text" id="forsale_bedroom" name="forsale_bedroom" class="keyword-input" placeholder="Bedroom ">

                                                <!-- <select onchange="get_filter()" id="forsale_bedroom" name="forsale_bedroom">
                            <option class="active">Min Beds</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                        </select> -->
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="single-query form-group">
                    <div class="intro">
                        <input type="text" id="forsale_bathroom" name="forsale_bathroom" class="keyword-input" placeholder="Bathroom ">

                                                <!--  <select onchange="get_filter()" id="forsale_bathroom" name="forsale_bathroom">
                            <option class="active">Min Baths</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                        </select> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-6">
                <div class="single-query form-group">
                    <input type="text" id="forsale_squarefeet" name="forsale_squarefeet" class="keyword-input" placeholder="Min Areasq ft)">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="single-query form-group">
                    <input type="text" id="forsale_maximum_area" name="forsale_maximum_area" class="keyword-input" placeholder="Max Area ">
                </div>
            </div>
        </div>
    </div>
                           <!--  <div class="col-sm-12 bottom10">
                                 <div class="single-query-slider">
                    <label><strong>Price Range:</strong></label>
                    <div class="price text-right"> <span>$</span>
                      <div class="leftLabel"></div>
                      <span>to $</span>
                      <div class="rightLabel"></div>
                    </div>
                                 <div data-range_min="0" data-range_max="1500000" data-cur_min="0" data-cur_max="1500000" class="nstSlider">
                      <div class="bar"></div>
                      <div class="leftGrip"></div>
                      <div class="rightGrip"></div>
                    </div>
                  </div>
              </div> -->
              <div class="col-sm-12 form-group">
                <button type="submit" class="btn-blue border_radius">Search</button>
            </div>
        </form>

    </div>

</aside>
<div class="filter_data"></div>

</div>

</div>

</div>
</section>


<script>
  function like_property(like) {
     var like_id = like.id;


     $(document).on('click','#like'+like_id,function(){


      var classs = $(this).attr('class');
      var forsale_id = $(this).data('row-id');
  //var like = classs.replace(' liked','');
  
  if (classs == 'like-btn') {
    $('#like'+like_id).removeClass('like-btn');
    $('#like'+like_id).addClass('unlike-btn');
    likes = 'unlike-btn';
} else if(classs == 'unlike-btn') {
    $('#like'+like_id).removeClass('unlike-btn');
    $('#like'+like_id).addClass('like-btn');
    likes = 'like-btn';
}
$.ajax({
    url:"<?=base_url('forsale/like')?>",
    method: "post",
    data:{like:likes, forsale_id:forsale_id},
    success: function(data){

    }
})
})
 }
</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/jquery-2.1.4.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/jquery.parallax-1.1.3.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/jquery.appear.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/bootsnav.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/masonry.pkgd.min.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/jquery.cubeportfolio.min.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/range-Slider.min.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/owl.carousel.min.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/selectbox-0.2.min.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/zelect.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/jquery.fancybox.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.layeranimation.min.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.navigation.min.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.parallax.min.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.slideanims.min.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.video.min.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/custom.js"></script>
<script src="<?php echo base_url('assets/front/js/')?>/functions.js"></script>
</body>

</html>