<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Data</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('front/forsale/edit');?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">              
                <div class="form-group">
                  <label>Forsale Home Heading</label>
                  <input type="name" class="form-control" id="forsale_home_heading" name="forsale_home_heading" value="<?php echo !empty($record->forsale_home_heading)?$record->forsale_home_heading:''?>" required>
                  <?php echo form_error('forsale_home_heading'); ?>
                </div>  

                 <div class="form-group">
                  <label>Forsale Home SubHeading</label>
                  <input type="name" class="form-control" id="forsale_home_subheading" name="forsale_home_subheading" value="<?php echo !empty($record->forsale_home_subheading)?$record->forsale_home_subheading:''?>" required>
                  <?php echo form_error('forsale_home_subheading'); ?>
                </div>
                  
                  <div class="form-group">
                <label>Forsale Image</label>
                <div class="input-group-btn">
                  <div class="image-upload">                      
                    <img src="<?php echo !empty($record->forsale_image)?base_url('uploads/images/').$record->forsale_image:base_url('assets/admin/img/placeholder.png')?>">
                    <div class="file-btn">
                      <input type="file" id="forsale_image" name="forsale_image">
                      <input type="text" id="forsale_image" name="forsale_image" value="<?php echo !empty($record->forsale_image)?$record->forsale_image:''?>" hidden>
                      <label class="btn btn-info">Upload</label>
                    </div>
                  </div>
                </div>
                <?php echo form_error('forsale_image'); ?>
              </div>  

              <div class="form-group">
                  <label>Forsale Sq. Ft.</label>
                  <input type="name" class="form-control" id="forsale_squarefeet" name="forsale_squarefeet" value="<?php echo !empty($record->forsale_squarefeet)?$record->forsale_squarefeet:''?>" required>
                  <?php echo form_error('forsale_squarefeet'); ?>
                </div>

                <div class="form-group">
                  <label>Forsale Bedroom</label>
                  <input type="name" class="form-control" id="forsale_bedroom" name="forsale_bedroom" value="<?php echo !empty($record->forsale_bedroom)?$record->forsale_bedroom:''?>" required>
                  <?php echo form_error('forsale_bedroom'); ?>
                </div>

                 <div class="form-group">
                  <label>Forsale Bathroom</label>
                  <input type="name" class="form-control" id="forsale_bathroom" name="forsale_bathroom" value="<?php echo !empty($record->forsale_bathroom)?$record->forsale_bathroom:''?>" required>
                  <?php echo form_error('forsale_bathroom'); ?>
                </div>

                 <div class="form-group">
                  <label>Forsale Text</label>
                  <textarea class="editor form-control" rows="3" id="forsale_text" name="forsale_text" required><?php echo !empty($record->forsale_text)?$record->forsale_text:''?></textarea>
                  <?php echo form_error('forsale_text'); ?>
                </div>
              </div>

              <div class="form-group">
                  <label>Forsale Price</label>
                  <input type="name" class="form-control" id="forsale_price" name="forsale_price" value="<?php echo !empty($record->forsale_price)?$record->forsale_price:''?>" required>
                  <?php echo form_error('forsale_price'); ?>
                </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $(".video-upload").on("change","input[type=file]",function() {      
            $(this).parents('.pro_videos').children('.col-md-4').children('.video-up').html('<img class="preview" src="' + URL.createObjectURL(this.files[0]) + '" /><i class="fa fa-times" aria-hidden="true"></i>');
          });
      
         $(".video-upload").on("click",".video-up i.fa.fa-times",function() {  
            $(this).parents('.video-up').html('');
            $(this).parents('.pro_videos').children('input[type=file]').val('');
   
   });
  });
</script>