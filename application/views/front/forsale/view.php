<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">View Properties</h3>
          </div>     
          <div class="col-md-6">
            <div class="box-body">
              <div class="form-group">
                  <span class="col-md-2 view_label">Property Heading</span>
                  <span class="col-md-10 view_details"><?php echo !empty($record->forsale_home_heading)?$record->forsale_home_heading:''?></span>
              </div>

               <div class="form-group">
                  <span class="col-md-2 view_label">Property SubHeading</span>
                  <span class="col-md-10 view_details"><?php echo !empty($record->forsale_home_subheading)?$record->forsale_home_subheading:''?></span>
              </div>
              
              <div class="form-group">
                  <span class="col-md-2 view_label">Property Square Ft.</span>
                  <span class="col-md-10 view_details"><?php echo !empty($record->forsale_squarefeet)?$record->forsale_squarefeet:''?></span>
              </div>
            
              
              <div class="form-group">
                  <span class="col-md-2 view_label">Property Image</span>
                  <span class="col-md-10 view_details"><img style="height:150px" src="<?php echo base_url('uploads/settings/').$record->forsale_image;?>"></span>
              </div>

              <div class="form-group">
                  <span class="col-md-2 view_label">Property Bedroom</span>
                  <span class="col-md-10 view_details"><?php echo !empty($record->forsale_bedroom)?$record->forsale_bedroom:''?></span>
              </div>

              <div class="form-group">
                  <span class="col-md-2 view_label">Property Bathroom</span>
                  <span class="col-md-10 view_details"><?php echo !empty($record->forsale_bathroom)?$record->forsale_bathroom:''?></span>
              </div>

              <div class="form-group">
                  <span class="col-md-2 view_label">Property Text</span>
                  <span class="col-md-10 view_details"><?php echo !empty($record->forsale_text)?$record->forsale_text:''?></span>
              </div>

              <div class="form-group">
                  <span class="col-md-2 view_label">Property Price</span>
                  <span class="col-md-10 view_details"><?php echo !empty($record->forsale_price)?$record->forsale_price:''?></span>
              </div>
              
            </div>      
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>
