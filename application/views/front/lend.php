<section>

<div class="container">
<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
		<div class="buyer-form">
			<h3 class="buyer-title">Lend A Property</h3>
			<form action="<?php echo base_url('lend/lend')?>" method="post">
				<div class="form-group">
					<div class="row">
					  	<div class="col-sm-6 col-xs-12">
					  		<input type="text" class="form-control input-lg" id="lend_firstname" name="lend_firstname" placeholder="First Name">
					  	</div>
					  	<div class="col-sm-6 col-xs-12">
					  		<input type="text" class="form-control input-lg" id="lend_lastname" name="lend_lastname" placeholder="Last Name">
					  	</div>
					</div>
				</div>

				<div class="form-group">
					<input type="text" class="form-control input-lg" id="lend_address" name="lend_address" placeholder="Interested Property Address">
				</div>

				<div class="form-group">
				    <input type="number" class="form-control input-lg" id="lend_phone" name="lend_phone" placeholder="Phone Number">
				</div>

				<div class="form-group">
					<input type="email" class="form-control input-lg" id="lend_email" name="lend_email" placeholder="Email Address">
				</div>

				<div class="form-group">
				  <textarea class="form-control input-lg" rows="5" id="lend_message" name="lend_message" placeholder="Type Your message"></textarea>
				</div>

				<div class="form-group">
				  <button type="submit" class="form-control btn-buyer">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>

</section>
