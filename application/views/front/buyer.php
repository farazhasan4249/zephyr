
<div class="buyers-banner">
  <div class="container">
    <h1 class="ml7"> <span class="text-wrapper"> <span class="letters"> Reach Us Out </span> </span> </h1>
    <p>Want to Buy a new property or lend/lease and want us to maintain your property, Reach us out. </p>
  </div>
</div>

<section class="buyer-bg">

<div class="container">
<div class="row">
	<div class="col-sm-4">
		<p>Donec tempor erat et est faucibus, faucibus pretium tellus.</p>
		<div class="buyer-form">
			<h3 class="buyer-title">Buyer Form</h3>
			<form action="<?php echo base_url('buyer/buyer')?>" method="post">
				<div class="form-group">
					<div class="row">
					  	<div class="col-sm-6 col-xs-12">
					  		<input type="text" class="form-control input-lg" id="buyer_firstname" name="buyer_firstname" placeholder="First Name">
					  	</div>
					  	<div class="col-sm-6 col-xs-12">
					  		<input type="text" class="form-control input-lg" id="buyer_lastname" name="buyer_lastname" placeholder="Last Name">
					  	</div>
					</div>
				</div>

				<div class="form-group">
					<input type="text" class="form-control input-lg" id="buyer_address" name="buyer_address" placeholder="Interested Property Address">
				</div>

				<div class="form-group">
				    <input type="number" class="form-control input-lg" id="buyer_phone" name="buyer_phone" placeholder="Phone Number">
				</div>

				<div class="form-group">
					<input type="email" class="form-control input-lg" id="buyer_email" name="buyer_email" placeholder="Email Address">
				</div>

				<div class="form-group">
				  <textarea class="form-control input-lg" rows="5" id="buyer_message" name="buyer_message" placeholder="Type Your message"></textarea>
				</div>

				<div class="form-group">
				  <button type="submit" class="form-control btn-buyer">Submit</button>
				</div>
			</form>
		</div>
	</div>
	<div class="col-sm-4">
		<p>Donec tempor erat et est faucibus, faucibus pretium tellus.</p>
		<div class="buyer-form">
			<h3 class="buyer-title">Lend A Property</h3>
			<form action="<?php echo base_url('buyer/lend')?>" method="post">
				<div class="form-group">
					<div class="row">
					  	<div class="col-sm-6 col-xs-12">
					  		<input type="text" class="form-control input-lg" id="lend_firstname" name="lend_firstname" placeholder="First Name">
					  	</div>
					  	<div class="col-sm-6 col-xs-12">
					  		<input type="text" class="form-control input-lg" id="lend_lastname" name="lend_lastname" placeholder="Last Name">
					  	</div>
					</div>
				</div>

				<div class="form-group">
					<input type="text" class="form-control input-lg" id="lend_address" name="lend_address" placeholder="Interested Property Address">
				</div>

				<div class="form-group">
				    <input type="number" class="form-control input-lg" id="lend_phone" name="lend_phone" placeholder="Phone Number">
				</div>

				<div class="form-group">
					<input type="email" class="form-control input-lg" id="lend_email" name="lend_email" placeholder="Email Address">
				</div>

				<div class="form-group">
				  <textarea class="form-control input-lg" rows="5" id="lend_message" name="lend_message" placeholder="Type Your message"></textarea>
				</div>

				<div class="form-group">
				  <button type="submit" class="form-control btn-buyer">Submit</button>
				</div>
			</form>
		</div>
	</div>
	<div class="col-sm-4">
		<p>Donec tempor erat et est faucibus, faucibus pretium tellus.</p>
		<div class="buyer-form">
			<h3 class="buyer-title">Property Management</h3>
			<form action="<?php echo base_url('buyer/property')?>" method="post">
				<div class="form-group">
					<div class="row">
					  	<div class="col-sm-6 col-xs-12">
					  		<input type="text" class="form-control input-lg" id="property_firstname" name="property_firstname" placeholder="First Name">
					  	</div>
					  	<div class="col-sm-6 col-xs-12">
					  		<input type="text" class="form-control input-lg" id="property_lastname" name="property_lastname" placeholder="Last Name">
					  	</div>
					</div>
				</div>

				<div class="form-group">
					<input type="text" class="form-control input-lg" id="property_ownerfirst" name="property_ownerfirst" placeholder="Owner FirstName">
				</div>

				<div class="form-group">
					<input type="text" class="form-control input-lg" id="property_ownerlast" name="property_ownerlast" placeholder="Owner LastName">
				</div>

				<div class="form-group">
					<input type="text" class="form-control input-lg" id="property_address" name="property_address" placeholder="Interested Property Address">
				</div>

				<div class="form-group">
				    <input type="number" class="form-control input-lg" id="property_phone" name="property_phone" placeholder="Phone Number">
				</div>

				<div class="form-group">
					<input type="email" class="form-control input-lg" id="property_email" name="property_email" placeholder="Email Address">
				</div>

				<div class="form-group">
				  <textarea class="form-control input-lg" rows="5" id="property_message" name="property_message" placeholder="Type Your message"></textarea>
				</div>

				<div class="form-group">
				  <button type="submit" class="form-control btn-buyer">Submit</button>
				</div>
			</form>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="buyer-redirect">
			<button><a href="https://zephyrinvest.tenantcloud.com" target="_blank">click here to redirect</a></button><br>
			<p><strong>Click here to request for Property maintenance, Bill Payments or Sign contracts.</strong></p>
		</div>
	</div>
</div>

</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery-2.1.4.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/bootstrap.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.parallax-1.1.3.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.appear.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/bootsnav.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/masonry.pkgd.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.cubeportfolio.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/range-Slider.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/owl.carousel.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/selectbox-0.2.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/zelect.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.fancybox.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.tools.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.revolution.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.layeranimation.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.navigation.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.parallax.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.slideanims.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.video.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/custom.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/functions.js"></script>
</body>
</html>
