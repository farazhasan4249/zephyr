
<?php if($this->session->flashdata('success')):?>
<script>alert_success("<?php echo $this->session->flashdata('success')?>");</script>
<?php endif;?>
<?php if($this->session->flashdata('error')):?>
<script>alert_danger("<?php echo $this->session->flashdata('error')?>");</script>
<?php endif;?> 
<div class="sign">
<section id="login" class="padding">
  <div class="container">
    <h3 class="hidden">hidden</h3>
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="profile-login">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Login</a></li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Register</a></li>
          </ul>
          <!-- Tab panes -->

          <div class="tab-content padding_half">
            <div role="tabpanel" class="tab-pane fade in active" id="home">
              <div class="agent-p-form">
                <form action="<?php echo base_url('login/loginquery')?>" class="callus clearfix">
                  <div class="single-query form-group col-sm-12">
                    <input type="email" id="customer_email_login" name="customer_email_login" class="keyword-input" placeholder="Email">
                  </div>
                  <div class="single-query form-group  col-sm-12">
                    <input type="password" id="customer_password_login" name="customer_password_login" class="keyword-input" placeholder="Password">
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="col-sm-6">
                        <div class="search-form-group white form-group text-left">
                          <div class="check-box-2"><i><input type="checkbox" name="check-box"></i></div>
                          <span>Remember Me</span>
                        </div>
                      </div>
                      <div class="col-sm-6 text-right">
                        <a href="login/forgot_password" class="lost-pass">Lost your password?</a>
                      </div>
                    </div>
                  </div>
                  <div class=" col-sm-12">
                    <input type="submit" value="submit now" class="btn-slide border_radius"> 
                  </div>
                </form>
              </div>
            </div>
          
            <div role="tabpanel" class="tab-pane fade" id="profile">
              <div class="agent-p-form">
                <form action="<?php echo base_url('login/register')?>" method="post" class="callus clearfix">
                  <div class="single-query col-sm-12 form-group">
                    <input type="text" class="keyword-input" id="customer_name" name="customer_name" placeholder="username" required>
                  </div>
                  <div class="single-query col-sm-12 form-group">
                    <input type="email" class="keyword-input" id="customer_email" name="customer_email" placeholder="Email Address">
                  </div>
                  <div class="single-query col-sm-12 form-group">
                    <input type="password" class="keyword-input" id="customer_password" name="customer_password" placeholder="Password">
                  </div>
                  <div class="single-query col-sm-12 form-group">
                    <input type="password" class="keyword-input" id="confirmpassword" name="confirm_password" placeholder="Confirm Password">
                  </div> <span id="message"></span>
                  <div class="search-form-group white col-sm-12 form-group text-left">
                    <div class="check-box-2"><i><input type="checkbox" id="customer_newsletter" name="check-box"></i></div>
                    <span>Receive Newsletter</span>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <div class="query-submit-button">
                      <input type="submit" value="Create an Account" class="btn-slide">
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
         <span id="message"></span>
      </div>
    </div>
  </div>
</div>
</section>

<!-- <script>
    $(document).ready(function (){
        $('#confirmpassword').on('keyup', function () {
            if ($('#password').val() == $('#confirmpassword').val()) {
                $('#message').html('Password Matched').css('color', 'green');
            }
            else
                $('#message').html('Invalid Confirm Password').css('color', 'red');
        });
    });
</script> -->

<!-- Login end -->


 

<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery-2.1.4.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/bootstrap.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.parallax-1.1.3.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.appear.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/bootsnav.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/masonry.pkgd.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.cubeportfolio.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/range-Slider.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/owl.carousel.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/selectbox-0.2.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/zelect.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.fancybox.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.tools.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.revolution.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.layeranimation.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.navigation.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.parallax.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.slideanims.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.video.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/custom.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/functions.js"></script>
</body>
</html>
