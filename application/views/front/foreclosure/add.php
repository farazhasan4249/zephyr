<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12  text-center ">
        <div class="box-header with-border">
            <h3 class="box-title"><strong>Add Foreclosure Property</strong></h3><br>
          </div>
      </div>
      <div class="col-md-12">
        <div class="box box-primary">
         <!--  <div class="box-header with-border">
            <h3 class="box-title"><strong>Add Foreclosure Property</strong></h3><br>
          </div>  -->    
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('foreclosure/add');?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">              
                <div class="form-group">
                  <label>foreclosure Home Heading</label>
                  <input type="name" class="form-control" id="foreclosure_home_heading" name="foreclosure_home_heading" value="<?php echo !empty($record->foreclosure_home_heading)?$record->foreclosure_home_heading:''?>" required>
                  <?php echo form_error('foreclosure_home_heading'); ?>
                </div>  

                 <div class="form-group">
                  <label>Foreclosure Location</label>
                  <input type="name" class="form-control" id="foreclosure_location" name="foreclosure_location" value="<?php echo !empty($record->foreclosure_location)?$record->foreclosure_location:''?>" required>
                  <?php echo form_error('foreclosure_location'); ?>
                </div>
                  
                  <div class="form-group">
                <label>Foreclosure Image</label>
                <div class="input-group-btn">
                  <div class="image-upload">                      
                    <img src="<?php echo !empty($record->foreclosure_image)?base_url('uploads/images/').$record->foreclosure_image:base_url('assets/admin/img/placeholder.png')?>" class="img-responsive">
                    <div class="file2-btn">
                      <button>
                      <input type="file" id="foreclosure_image" name="foreclosure_image" placeholder="upload">
                       <h5>upload</h5>

                       </button>
                     </div>
                  </div>
                </div>
                <?php echo form_error('foreclosure_image'); ?>
              </div>  

              <div class="form-group">
                  <label>Foreclosure Sq. Ft.</label>
                  <input type="name" class="form-control" id="foreclosure_squarefeet" name="foreclosure_squarefeet" value="<?php echo !empty($record->foreclosure_squarefeet)?$record->foreclosure_squarefeet:''?>" required>
                  <?php echo form_error('foreclosure_squarefeet'); ?>
                </div>

                <div class="form-group">
                  <label>Foreclosure Bedroom</label>
                  <input type="name" class="form-control" id="foreclosure_bedroom" name="foreclosure_bedroom" value="<?php echo !empty($record->foreclosure_bedroom)?$record->foreclosure_bedroom:''?>" required>
                  <?php echo form_error('Foreclosure_bedroom'); ?>
                </div>

                 <div class="form-group">
                  <label>Foreclosure Bathroom</label>
                  <input type="name" class="form-control" id="foreclosure_bathroom" name="foreclosure_bathroom" value="<?php echo !empty($record->foreclosure_bathroom)?$record->foreclosure_bathroom:''?>" required>
                  <?php echo form_error('foreclosure_bathroom'); ?>
                </div>

                 

              <div class="form-group">
                  <label>Foreclosure Price</label>
                  <input type="name" class="form-control" id="foreclosure_price" name="foreclosure_price" value="<?php echo !empty($record->foreclosure_price)?$record->foreclosure_price:''?>" required>
                  <?php echo form_error('foreclosure_price'); ?>
                </div>

                  <div class="form-group">
                  <label>Foreclosure Keyword</label>
                  <input type="name" class="form-control" id="foreclosure_keyword" name="foreclosure_keyword" value="<?php echo !empty($record->foreclosure_keyword)?$record->foreclosure_keyword:''?>" required>
                  <?php echo form_error('foreclosure_keyword'); ?>
                </div>

                 <div class="form-group">
                  <label>Foreclosure Location</label>
                  <input type="name" class="form-control" id="foreclosure_location" name="foreclosure_location" value="<?php echo !empty($record->foreclosure_location)?$record->foreclosure_location:''?>" required>
                  <?php echo form_error('foreclosure_location'); ?>
                </div>

                <div class="form-group">
                  <label>Foreclosure Property Type</label>
                  <input type="name" class="form-control" id="foreclosure_property_type" name="foreclosure_property_type" value="<?php echo !empty($record->foreclosure_property_type)?$record->foreclosure_property_type:''?>" required>
                  <?php echo form_error('foreclosure_property_type'); ?>
                </div>

                <div class="form-group">
                  <label>Foreclosure Property Status</label>
                  <input type="name" class="form-control" id="foreclosure_property_status" name="foreclosure_property_status" value="<?php echo !empty($record->foreclosure_property_status)?$record->foreclosure_property_status:''?>" required>
                  <?php echo form_error('foreclosure_property_status'); ?>
                </div>

                <div class="form-group">
                  <label>Foreclosure Maximum Area</label>
                  <input type="name" class="form-control" id="foreclosure_maximum_area" name="foreclosure_maximum_area" value="<?php echo !empty($record->foreclosure_maximum_area)?$record->foreclosure_maximum_area:''?>" required>
                  <?php echo form_error('foreclosure_maximum_area'); ?>
                </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>