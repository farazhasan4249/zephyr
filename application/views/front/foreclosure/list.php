
<div class="fornec-banner">
  <div class="container">
    <h1 class="ml7"> <span class="text-wrapper"> <span class="letters"> Foreclosure Home </span> </span> </h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque rutrum libero sed finibus volutpat. Quisque finibus interdum purus, quis mollis odio consectetur vitae. Ut feugiat risus erat, in tincidunt est rutrum vitae. Donec in fermentum ligula, sed bibendum ante. </p>
  </div>
</div>
<section id="listing1" class="listing1 padding_top">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-12 col-xs-12">
        <div class="row">
           
          <div class="col-md-9">
            <h2 class="uppercase"> Foreclosure Home</h2>
            <p class="heading_space"></p>
          </div>
          <div class="col-md-3">
            <form class="callus">
             <!--  <div class="single-query">
                <div class="intro">
                  <select>
                    <option class="active">Default Order</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                  </select>
                </div>
              </div> -->
            </form>
          </div>
        </div>
        <div class="row">
          <?php if(isset($records) && !empty($records)) { ?>
              <?php foreach($records as $record) { ?>
          <div class="col-sm-12">
            <div class="listing_full">
              <div class="image"> <img alt="image" src="<?php echo base_url('uploads/settings/').$record->foreclosure_image?>"> <span class="tag_t">For Sale</span> <span class="tag_l">Featured</span> </div>
              <div class="listing_full_bg">
                <div class="listing_inner_full"> <span><a href="#"><i class="icon-like"></i></a></span> <a href="#.">
                  <h3><?php echo base_url($record->foreclosure_home_heading)?$record->foreclosure_home_heading:''?></h3>
                  <p><?php echo base_url($record->foreclosure_location)?$record->foreclosure_location:''?> </p>
                  </a>
                  <div class="favroute clearfix">
                    <div class="property_meta"><span><i class="icon-select-an-objecto-tool"></i><?php echo base_url($record->foreclosure_squarefeet)?$record->foreclosure_squarefeet:''?> sq ft</span><span><i class=" icon-bed"></i><?php echo base_url($record->foreclosure_bedroom)?$record->foreclosure_bedroom:''?> Bedrooms</span><span><i class="icon-safety-shower"></i><?php echo base_url($record->foreclosure_bathroom)?$record->foreclosure_bathroom:''?> Bathrooms</span><span class="border-l">$<?php echo base_url($record->foreclosure_price)?$record->foreclosure_price:''?> / pm</span></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php }}?>
        </div>
      </div>

      <!-- <div align="center" id="pagination_link">
            
                </div> -->

      
      <aside class="col-md-4 col-xs-12">
        <div class="property-query-area clearfix">
          <div class="col-md-12">
            <h3 class="text-uppercase bottom20 top15">Advanced Search</h3>
          </div>
          <form class="callus" action="<?php echo base_url('foreclosure/search')?>" method="get">
            <div class="single-query form-group col-sm-12">
              <input type="text" id="foreclosure_keyword" name="foreclosure_keyword" class="keyword-input" placeholder="Keyword (e.g. 'office')">
            </div>
            <div class="single-query form-group col-sm-12">
              <div class="intro">
                <input type="text" id="foreclosure_location" name="foreclosure_location" class="keyword-input" placeholder="Location">
            </div>
                <!-- <select id="foreclosure_location" name="foreclosure_location">
                  <option selected="" value="any">Location</option>
                  <option>All areas</option>
                  <option>Bayonne </option>
                  <option>Greenville</option>
                  <option>Manhattan</option>
                  <option>Queens</option>
                  <option>The Heights</option>
                </select> -->
              </div>
         
            <div class="single-query form-group col-sm-12">
              <div class="intro">
                 <input type="text" id="foreclosure_property_type" name="foreclosure_property_type" class="keyword-input" placeholder="Property Type">
              <!--   <select id="foreclosure_property_type" name="foreclosure_property_type">
                  <option class="active">Property Type</option>
                  <option>All areas</option>
                  <option>Bayonne </option>
                  <option>Greenville</option>
                  <option>Manhattan</option>
                  <option>Queens</option>
                  <option>The Heights</option>
                </select> -->
              </div>
            </div>
         
            <div class="single-query form-group col-sm-12">
              <div class="intro">
                <input type="text" id="foreclosure_property_status" name="foreclosure_property_status" class="keyword-input" placeholder="Property Status">
               <!--  <select id="foreclosure_property_status" name="foreclosure_property_status">
                  <option class="active">Property Status</option>
                  <option>All areas</option>
                  <option>Bayonne </option>
                  <option>Greenville</option>
                  <option>Manhattan</option>
                  <option>Queens</option>
                  <option>The Heights</option>
                </select> -->
              </div>
            </div>
            <div class="search-2 col-sm-12">
              <div class="row">
                <div class="col-sm-6">
                  <div class="single-query form-group">
                    <div class="intro">
                       <input type="text" id="foreclosure_bedroom" name="foreclosure_bedroom" class="keyword-input" placeholder="Bedroom">
                    <!--   <select id="foreclosure_bedroom" name="foreclosure_bedroom">
                        <option class="active">Min Beds</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                      </select> -->
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="single-query form-group">
                    <div class="intro">
                       <input type="text" id="foreclosure_bathroom" name="foreclosure_bathroom" class="keyword-input" placeholder="Bathroom">
                    <!--   <select id="foreclosure_bathroom" name="foreclosure_bathroom">
                        <option class="active">Min Baths</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                      </select> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row">
                <div class="col-sm-6">
                  <div class="single-query form-group">
                    <input type="text" id="foreclosure_squarefeet" name="foreclosure_squarefeet" class="keyword-input" placeholder="Min Area (sq ft)">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="single-query form-group">
                    <input type="text" id="foreclosure_maximum_area" name="foreclosure_maximum_area" class="keyword-input" placeholder="Max Area ">
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="col-sm-12 bottom10">
              <div class="single-query-slider">
                <label><strong>Price Range:</strong></label>
                <div class="price text-right"> <span>$</span>
                  <div class="leftLabel"></div>
                  <span>to $</span>
                  <div class="rightLabel"></div>
                </div>
                <div data-range_min="0" data-range_max="1500000" data-cur_min="0" data-cur_max="1500000" class="nstSlider">
                  <div class="bar"></div>
                  <div class="leftGrip"></div>
                  <div class="rightGrip"></div>
                </div>
              </div>
            </div> -->
            <div class="col-sm-12 form-group">
              <button type="submit" class="btn-blue border_radius">Search</button>
            </div>
               </div>
          </form>
          <!-- <div class="search-propertie-filters collapse">
            <div class="container-2">
              <div class="row">
                <div class="col-sm-6 col-xs-6">
                  <div class="search-form-group white">
                    <input type="checkbox" name="check-box" />
                    <span>Rap music</span> </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                  <div class="search-form-group white">
                    <input type="checkbox" name="check-box" />
                    <span>Rap music</span> </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                  <div class="search-form-group white">
                    <input type="checkbox" name="check-box" />
                    <span>Rap music</span> </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                  <div class="search-form-group white">
                    <input type="checkbox" name="check-box" />
                    <span>Rap music</span> </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 col-xs-6">
                  <div class="search-form-group white">
                    <input type="checkbox" name="check-box" />
                    <span>Rap music</span> </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                  <div class="search-form-group white">
                    <input type="checkbox" name="check-box" />
                    <span>Rap music</span> </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                  <div class="search-form-group white">
                    <input type="checkbox" name="check-box" />
                    <span>Rap music</span> </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                  <div class="search-form-group white">
                    <input type="checkbox" name="check-box" />
                    <span>Rap music</span> </div>
                </div>
              </div>
            </div>
          </div> -->
        </div>
      </aside>
    </div>
  </div>
</section>


<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery-2.1.4.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/bootstrap.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.parallax-1.1.3.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.appear.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/bootsnav.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/masonry.pkgd.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.cubeportfolio.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/range-Slider.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/owl.carousel.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/selectbox-0.2.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/zelect.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.fancybox.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.tools.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.revolution.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.layeranimation.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.navigation.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.parallax.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.slideanims.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.video.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/custom.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/functions.js"></script>
</body>
</html>
