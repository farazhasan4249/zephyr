<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

<div class="rentals-banner">
  <div class="container">
    <h1 class="ml7"> <span class="text-wrapper"> <span class="letters">Rentals Rent</span> </span> </h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque rutrum libero sed finibus volutpat. Quisque finibus interdum purus, quis mollis odio consectetur vitae. Ut feugiat risus erat, in tincidunt est rutrum vitae. Donec in fermentum ligula, sed bibendum ante. </p>
  </div>
</div>
<!-- Page Banner End --> 

<!-- Listing Start -->
<section id="listing1" class="listing1 padding_top">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-md-9">
            <h2 class="uppercase"> Rentals Rent</h2>
            <p class="heading_space"> </p>
          </div>
          <div class="col-md-3">
            <form class="callus">
              <div class="single-query">
               <!--  <div class="intro">
                  <select>
                    <option class="active">Default Order</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                  </select>
                </div> -->
              </div>
            </form>
          </div>
        </div>
        <div class="row">
          <?php if(isset($records) && !empty($records)) { ?>
              <?php foreach($records as $record) { ?>
          <div class="col-sm-6">
            <div class="property_item heading_space">
              <div class="property_head text-center">
                <h3 class="captlize"><?php echo base_url($record->rental_home_heading)?$record->rental_home_heading:''?></h3>
                <p><?php echo base_url($record->rental_home_subheading)?$record->rental_home_subheading:''?></p>
              </div>
              <div class="image"> <a href="#."> <img src="<?php echo base_url('uploads/settings/').$record->rental_image?>" alt="latest property" class="img-responsive"></a>
                <div class="price clearfix"> <span class="tag">For Sale</span> </div>
              </div>
              <div class="proerty_content">
                <div class="property_meta"> <span><i class="icon-select-an-objecto-tool"></i><?php echo base_url($record->rental_squarefeet)?$record->rental_squarefeet:''?> sq ft</span> <span><i class="icon-bed"></i><?php echo base_url($record->rental_bedroom)?$record->rental_bedroom:''?> Bedrooms</span> <span><i class="icon-safety-shower"></i><?php echo base_url($record->rental_bathroom)?$record->rental_bathroom:''?> Bathrooms</span> </div>
                <div class="proerty_text">
                  <p><?php echo base_url($record->rental_text)?$record->rental_text:''?> </p>
                </div>
                <div class="favroute clearfix">
                  <p class="pull-md-left"><?php echo base_url($record->rental_price)?$record->rental_price:''?></p>
                  <ul class="pull-right">
                    <li><a href="#"><i class="icon-like"></i></a></li>
                    <!-- <li><a href="#" class="share_expender"><i class="icon-share3"></i></a></li> -->
                  </ul>
                </div>
                <div class="toggle_share collapse" id="four">
                  
                </div>
              </div>
            </div>
          </div>
            <?php }}?>
          </div>
         <!--  <div class="col-sm-6">
            <div class="property_item heading_space">
              <div class="property_head default_clr text-center"> <img src="<?php echo base_url('assets/front/images/')?>favruite.png" alt="property" class="start_tag">
                <h3 class="captlize">Historic Town House</h3>
                <p>45 Regent Street, , UK</p>
              </div>
              <div class="image"> <a href="#"> <img src="<?php echo base_url('assets/front/images/')?>listing2.jpg" alt="latest property" class="img-responsive"></a>
                <div class="price clearfix"> <span class="tag">For Sale</span> </div>
              </div>
              <div class="proerty_content">
                <div class="property_meta"> <span><i class="icon-select-an-objecto-tool"></i>4800 sq ft</span> <span><i class="icon-bed"></i>3 Bedrooms</span> <span><i class="icon-safety-shower"></i>2 Bedrooms</span> </div>
                <div class="proerty_text">
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam power nonummy nibh tempor 
                    cum soluta nobis… </p>
                </div>
                <div class="favroute clearfix">
                  <p class="pull-md-left">$8,600</p>
                  <ul class="pull-right">
                    <li><a href="#"><i class="icon-like"></i></a></li>
                    <li><a href="#" class="share_expender"><i class="icon-share3"></i></a></li>
                  </ul>
                </div>
                
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="property_item heading_space">
              <div class="property_head text-center">
                <h3 class="captlize">Historic Town House</h3>
                <p>45 Regent Street, , UK</p>
              </div>
              <div class="image"> <a href="#"> <img src="<?php echo base_url('assets/front/images/')?>listing7.jpg" alt="latest property" class="img-responsive"></a>
                <div class="price clearfix"> <span class="tag">For Sale</span> </div>
              </div>
              <div class="proerty_content">
                <div class="property_meta"> <span><i class="icon-select-an-objecto-tool"></i>4800 sq ft</span> <span><i class="icon-bed"></i>3 Bedrooms</span> <span><i class="icon-safety-shower"></i>2 Bedrooms</span> </div>
                <div class="proerty_text">
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam power nonummy nibh tempor 
                    cum soluta nobis… </p>
                </div>
                <div class="favroute clearfix">
                  <p class="pull-md-left">$8,600</p>
                  <ul class="pull-right">
                    <li><a href="#"><i class="icon-like"></i></a></li>
                    <li><a href="#" class="share_expender" ><i class="icon-share3"></i></a></li>
                  </ul>
                </div>
                
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="property_item heading_space">
              <div class="property_head text-center">
                <h3 class="captlize">Historic Town House</h3>
                <p>45 Regent Street, , UK</p>
              </div>
              <div class="image"> <a href="#"> <img src="<?php echo base_url('assets/front/images/')?>listing4.jpg" alt="latest property" class="img-responsive"></a>
                <div class="price clearfix"> <span class="tag">For Sale</span> </div>
              </div>
              <div class="proerty_content">
                <div class="property_meta"> <span><i class="icon-select-an-objecto-tool"></i>4800 sq ft</span> <span><i class="icon-bed"></i>3 Bedrooms</span> <span><i class="icon-safety-shower"></i>2 Bedrooms</span> </div>
                <div class="proerty_text">
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam power nonummy nibh tempor 
                    cum soluta nobis… </p>
                </div>
                <div class="favroute clearfix">
                  <p class="pull-md-left">$8,600</p>
                  <ul class="pull-right">
                    <li><a href="#"><i class="icon-like"></i></a></li>
                    <li><a href="#" class="share_expender"><i class="icon-share3"></i></a></li>
                  </ul>
                </div>
                
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="property_item heading_space">
              <div class="property_head text-center">
                <h3 class="captlize">Historic Town House</h3>
                <p>45 Regent Street, , UK</p>
              </div>
              <div class="image"> <a href="#"> <img src="<?php echo base_url('assets/front/images/')?>listing5.jpg" alt="latest property" class="img-responsive"></a>
                <div class="price clearfix"> <span class="tag">For Sale</span> </div>
              </div>
              <div class="proerty_content">
                <div class="property_meta"> <span><i class="icon-select-an-objecto-tool"></i>4800 sq ft</span> <span><i class="icon-bed"></i>3 Bedrooms</span> <span><i class="icon-safety-shower"></i>2 Bedrooms</span> </div>
                <div class="proerty_text">
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam power nonummy nibh tempor 
                    cum soluta nobis… </p>
                </div>
                <div class="favroute clearfix">
                  <p class="pull-md-left">$8,600</p>
                  <ul class="pull-right">
                    <li><a href="#"><i class="icon-like"></i></a></li>
                    <li><a href="#" class="share_expender"><i class="icon-share3"></i></a></li>
                  </ul>
                </div>
                
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="property_item heading_space">
              <div class="property_head text-center">
                <h3 class="captlize">Historic Town House</h3>
                <p>45 Regent Street, , UK</p>
              </div>
              <div class="image"> <a href="#"> <img src="<?php echo base_url('assets/front/images/')?>listing6.jpg" alt="latest property" class="img-responsive"></a>
                <div class="price clearfix"> <span class="tag">For Sale</span> </div>
              </div>
              <div class="proerty_content">
                <div class="property_meta"> <span><i class="icon-select-an-objecto-tool"></i>4800 sq ft</span> <span><i class="icon-bed"></i>3 Bedrooms</span> <span><i class="icon-safety-shower"></i>2 Bedrooms</span> </div>
                <div class="proerty_text">
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam power nonummy nibh tempor 
                    cum soluta nobis… </p>
                </div>
                <div class="favroute clearfix">
                  <p class="pull-md-left">$8,600</p>
                  <ul class="pull-right">
                    <li><a href="#"><i class="icon-like"></i></a></li>
                    <li><a href="#" class="share_expender"><i class="icon-share3"></i></a></li>
                  </ul>
                </div>
                
              </div> -->
            </div>
       
     
      <aside class="col-md-4 col-xs-12">
        <div class="property-query-area clearfix">
          <div class="col-md-12">
            <h3 class="text-uppercase bottom20 top15">Advanced Search</h3>
          </div>
          <form class="callus"  action="<?php echo base_url('rental/search')?>" method="get">
            <div class="single-query form-group col-sm-12">
              <input type="text" class="keyword-input" placeholder="Keyword ">
            </div>
            <div class="single-query form-group col-sm-12">
              <div class="intro">
                 <input type="text" id="rental_location" name="rental_location" class="keyword-input" placeholder="location ">
               <!--  <select>
                  <option selected="" value="any">Location</option>
                  <option>All</option>
                  <option>UK </option>
                </select> -->
              </div>
            </div>
            <div class="single-query form-group col-sm-12">
              <div class="intro">
                <input type="text" id="rental_property_type" name="rental_property_type" class="keyword-input" placeholder="Property Type ">
               <!--  <select>
                  <option class="active">Property Type</option>
                  <option>All areas</option>
                </select> -->
              </div>
            </div>
            <div class="single-query form-group col-sm-12">
              <div class="intro">
                <input type="text" id="rental_property_status" name="rental_property_status" class="keyword-input" placeholder="Property Status ">
                <!-- <select>
                  <option class="active">Property Status</option>
                  <option>All areas</option>
                </select> -->
              </div>
            </div>
            <div class="search-2 col-sm-12">
              <div class="row">
                <div class="col-sm-6">
                  <div class="single-query form-group">
                    <div class="intro">
                      <input type="text" id="rental_bedroom" name="rental_bedroom" class="keyword-input" placeholder="Min Bedroom ">
                     <!--  <select>
                        <option class="active">Min Beds</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                      </select> -->
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="single-query form-group">
                    <div class="intro">
                      <input type="text" id="rental_bathroom" name="rental_bathroom" class="keyword-input" placeholder="Min Bathroom ">
                     <!--  <select>
                        <option class="active">Min Baths</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                      </select> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row">
                <div class="col-sm-6">
                  <div class="single-query form-group">
                    <input type="text" id="rental_squarefeet" name="rental_squarefeet" class="keyword-input" placeholder="Min Areasq ft)">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="single-query form-group">
                    <input type="text" id="rental_maximum_area" name="rental_maximum_area" class="keyword-input" placeholder="Max Area ">
                  </div>
                </div>
              </div>
            </div>
           <!--  <div class="col-sm-12 bottom10">
              <div class="single-query-slider">
                <label><strong>Price Range:</strong></label>
                <div class="price text-right"> <span>$</span>
                  <div class="leftLabel"></div>
                  <span>to $</span>
                  <div class="rightLabel"></div>
                </div>
                <div data-range_min="0" data-range_max="1500000" data-cur_min="0" data-cur_max="1500000" class="nstSlider">
                  <div class="bar"></div>
                  <div class="leftGrip"></div>
                  <div class="rightGrip"></div>
                </div>
              </div>
            </div> -->
            <div class="col-sm-12 form-group">
              <button type="submit" class="btn-blue border_radius">Search</button>
            </div>
          </form>
          <div class="search-propertie-filters collapse">
            <div class="container-2">
              <div class="row">
                <div class="col-sm-6 col-xs-6">
                  <div class="search-form-group white">
                    <input type="checkbox" name="check-box" />
                    <span>Rap music</span> </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                  <div class="search-form-group white">
                    <input type="checkbox" name="check-box" />
                    <span>Rap music</span> </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                  <div class="search-form-group white">
                    <input type="checkbox" name="check-box" />
                    <span>Rap music</span> </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                  <div class="search-form-group white">
                    <input type="checkbox" name="check-box" />
                    <span>Rap music</span> </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 col-xs-6">
                  <div class="search-form-group white">
                    <input type="checkbox" name="check-box" />
                    <span>Rap music</span> </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                  <div class="search-form-group white">
                    <input type="checkbox" name="check-box" />
                    <span>Rap music</span> </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                  <div class="search-form-group white">
                    <input type="checkbox" name="check-box" />
                    <span>Rap music</span> </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                  <div class="search-form-group white">
                    <input type="checkbox" name="check-box" />
                    <span>Rap music</span> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
       <!--  <div class="row">
          <div class="col-md-12">
            <h3 class="bottom40 margin40">Featured Properties</h3>
          </div>
        </div>
        <div class="row bottom20">
          <div class="col-md-4 col-sm-4 col-xs-6 p-image"> <img src="<?php echo base_url('assets/front/images/')?>f-p-1.png" alt="image"/> </div>
          <div class="col-md-8 col-sm-8 col-xs-6">
            <div class="feature-p-text">
              <h4>Historic Town House</h4>
              <p class="bottom15">45 Regent Street, , UK</p>
              <a href="#">$128,600</a> </div>
          </div>
        </div>
        <div class="row bottom20">
          <div class="col-md-4 col-sm-4 col-xs-6 p-image"> <img src="<?php echo base_url('assets/front/images/')?>f-p-1.png" alt="image"/> </div>
          <div class="col-md-8 col-sm-8 col-xs-6">
            <div class="feature-p-text">
              <h4>Historic Town House</h4>
              <p class="bottom15">45 Regent Street, , UK</p>
              <a href="#">$128,600</a> </div>
          </div>
        </div>
        <div class="row bottom20">
          <div class="col-md-4 col-sm-4 col-xs-6 p-image"> <img src="<?php echo base_url('assets/front/images/')?>f-p-1.png" alt="image"/> </div>
          <div class="col-md-8 col-sm-8 col-xs-6">
            <div class="feature-p-text">
              <h4>Historic Town House</h4>
              <p class="bottom15">45 Regent Street, , UK</p>
              <a href="#">$128,600</a> </div>
          </div>
        </div> -->
      </aside>
    </div>
  </div>
</section>



<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery-2.1.4.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/bootstrap.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.parallax-1.1.3.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.appear.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/bootsnav.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/masonry.pkgd.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.cubeportfolio.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/range-Slider.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/owl.carousel.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/selectbox-0.2.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/zelect.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.fancybox.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.tools.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.revolution.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.layeranimation.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.navigation.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.parallax.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.slideanims.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.video.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/custom.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/functions.js"></script>
</body>
</html>
