
  
  <div class="faqs-banner">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <h4><?php echo base_url($faqs->faqs_heading)?$faqs->faqs_heading:''?></h4>
          <p><?php echo base_url($faqs->faqs_content)?$faqs->faqs_content:''?></p>
        </div>
      </div>
    </div>
  </div>
  <div class="marg">
    <div class="container">
      <div class="panel-group" id="accordion">
        <h5>General Questions</h5>
        <div class="col-md-6 col-sm-6 col-xs-12 ">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><?php echo base_url($faqs->faqs_question)?$faqs->faqs_question:''?></a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
              <div class="panel-body">
                <p><?php echo base_url($faqs->faqs_answer)?$faqs->faqs_answer:''?></p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen"><?php echo base_url($faqs->faqs_questiontwo)?$faqs->faqs_questiontwo:''?></a>
              </h4>
            </div>
            <div id="collapseTen" class="panel-collapse collapse">
              <div class="panel-body">
                <p><?php echo base_url($faqs->faqs_answertwo)?$faqs->faqs_answertwo:''?></p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven"><?php echo base_url($faqs->faqs_questionthree)?$faqs->faqs_questionthree:''?></a>
              </h4>
            </div>
            <div id="collapseEleven" class="panel-collapse collapse">
              <div class="panel-body">
                <p><?php echo base_url($faqs->faqs_answerthree)?$faqs->faqs_answerthree:''?></p>
              </div>
            </div>
          </div>



        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 ">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsefour"><?php echo base_url($faqs->faqs_questionfour)?$faqs->faqs_questionfour:''?></a>
              </h4>
            </div>
            <div id="collapsefour" class="panel-collapse collapse in">
              <div class="panel-body">
                <p><?php echo base_url($faqs->faqs_answerfour)?$faqs->faqs_answerfour:''?></p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsefive"><?php echo base_url($faqs->faqs_questionfive)?$faqs->faqs_questionfive:''?>
                </a>
              </h4>
            </div>
            <div id="collapsefive" class="panel-collapse collapse">
              <div class="panel-body">
                <p><?php echo base_url($faqs->faqs_answerfive)?$faqs->faqs_answerfive:''?></p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEsix"><?php echo base_url($faqs->faqs_questionsix)?$faqs->faqs_questionsix:''?>
                </a>
              </h4>
            </div>
            <div id="collapseEsix" class="panel-collapse collapse">
              <div class="panel-body">
                <p><?php echo base_url($faqs->faqs_answersix)?$faqs->faqs_answersix:''?></p>
              </div>
            </div>
          </div>



        </div>

      </div>
    </div>

  </div>





<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery-2.1.4.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/bootstrap.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.parallax-1.1.3.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.appear.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/bootsnav.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/masonry.pkgd.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.cubeportfolio.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/range-Slider.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/owl.carousel.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/selectbox-0.2.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/zelect.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.fancybox.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.tools.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/jquery.themepunch.revolution.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.layeranimation.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.navigation.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.parallax.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.slideanims.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/revolution.extension.video.min.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/custom.js"></script> 
<script src="<?php echo base_url('assets/front/js/')?>/functions.js"></script>
</body>
</html>
