<section>

<div class="container">
<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
		<div class="buyer-form">
			<h3 class="buyer-title">Property Management</h3>
			<form action="<?php echo base_url('property/property')?>" method="post">
				<div class="form-group">
					<div class="row">
					  	<div class="col-sm-6 col-xs-12">
					  		<input type="text" class="form-control input-lg" id="property_firstname" name="property_firstname" placeholder="First Name">
					  	</div>
					  	<div class="col-sm-6 col-xs-12">
					  		<input type="text" class="form-control input-lg" id="property_lastname" name="property_lastname" placeholder="Last Name">
					  	</div>
					</div>
				</div>

				<div class="form-group">
					<input type="text" class="form-control input-lg" id="property_ownerfirst" name="property_ownerfirst" placeholder="Owner FirstName">
				</div>

				<div class="form-group">
					<input type="text" class="form-control input-lg" id="property_ownerlast" name="property_ownerlast" placeholder="Owner LastName">
				</div>

				<div class="form-group">
					<input type="text" class="form-control input-lg" id="property_address" name="property_address" placeholder="Interested Property Address">
				</div>

				<div class="form-group">
				    <input type="number" class="form-control input-lg" id="property_phone" name="property_phone" placeholder="Phone Number">
				</div>

				<div class="form-group">
					<input type="email" class="form-control input-lg" id="property_email" name="property_email" placeholder="Email Address">
				</div>

				<div class="form-group">
				  <textarea class="form-control input-lg" rows="5" id="property_message" name="property_message" placeholder="Type Your message"></textarea>
				</div>

				<div class="form-group">
				  <button type="submit" class="form-control btn-buyer">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>

</section>
