

<div class="sign">
<section id="login" class="padding">
  <div class="container">
    <h3 class="hidden">hidden</h3>
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="profile-login">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Register</a></li>
          </ul>
          <!-- Tab panes -->

          <div class="tab-content padding_half">
            <div role="tabpanel" class="tab-pane fade in active" id="home">
              <div class="agent-p-form">
                <form action="<?php echo base_url('register/sendquery')?>" method="post" class="callus clearfix">
                  <div class="single-query col-sm-12 form-group">
                      <input type="text" id="customer_name" name="customer_name" class="keyword-input" placeholder="username" required="">
                  </div>
                  <div class="single-query col-sm-12 form-group">
                      <input type="text" id="customer_email" name="customer_email" class="keyword-input" placeholder="Email Address">
                  </div>
                  <div class="single-query form-group  col-sm-12">
                    <input type="password" id="customer_password" name="customer_password" class="keyword-input" placeholder="Password">
                  </div>
                   <div class="single-query form-group  col-sm-12">
                   <input type="password" class="keyword-input" id="confirmpassword" name="confirm_password" placeholder="Confirm Password">
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="col-sm-6">
                        <!-- <div class="search-form-group white form-group text-left">
                          <div class="check-box-2"><i><div class="check-box"><i><input type="checkbox" id="customer_newsletter" name="check-box"></i></div></i></div>
                          <span>Recieve Newsletter</span>
                        </div> -->
                      </div>
                    </div>
                  </div>
                  <div class=" col-sm-12">
                    <input type="submit" value="Create an Account" class="btn-slide border_radius"> 
                  </div>
                </form>
              </div>
            </div>
            
          </div>
        </div>
         <span id="message"></span>
      </div>
    </div>
  </div>
</div>
</section>


<script>
    $(document).ready(function () {
        $('#confirmpassword').on('keyup', function () {
            if ($('#password').val() == $('#confirmpassword').val()) {
                $('#message').html('Password Matched').css('color', 'green');
            }
            else
                $('#message').html('Invalid Confirm Password').css('color', 'red');
        });
    });
</script>
