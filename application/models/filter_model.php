<?php
 
class filter_model extends CI_Model
{
 function fetch_filter_type($type)
 {
  $this->db->distinct();
  $this->db->select($type);
  $this->db->from('forsale');
  $this->db->where('forsale_status' , 'enable');
  return $this->db->get();
 }

 public function get($data)
  {   
    if(isset($data['select']) && !empty($data['select'])){
      $this->db->select($data['select']);
    }
    if(isset($data['select_max']) && !empty($data['select_max'])){
      $this->db->select_max($data['select_max']);
    }
    if(isset($data['select_min']) && !empty($data['select_min'])){
      $this->db->select_min($data['select_min']);
    }
    
    if(isset($data['select_avg']) && !empty($data['select_avg'])){
      $this->db->select_avg($data['select_avg']);
    }
    if(isset($data['select_sum']) && !empty($data['select_sum'])){
      $this->db->select_sum($data['select_sum']);
    }
    if(!empty($data['join_array']) && is_array($data['join_array'])){
      foreach($data['join_array'] as $record){
        $this->db->join($record['join_table'],$record['join'],$record['join_type']);
        $this->db->where($record['join_table'].'_status','enable');
      }
    }
    if(!empty($data['join'])){        
      $this->db->join($data['join_table'],$data['join'],$data['join_type']);      
    }
    if(isset($data['where']) && !empty($data['where'])){
      $this->db->where($data['where']);
    }
    if(isset($data['or_where']) && !empty($data['or_where'])){
      $this->db->or_where($data['or_where']);
    }
    if(isset($data['or_where_in']) && !empty($data['or_where_in'])){
      $this->db->or_where_in($data['or_where_in']);
    }   
    if(isset($data['where_not_in']) && !empty($data['where_not_in'])){
      $this->db->where_not_in($data['where_not_in']);
    }   
    if(isset($data['like']) && !empty($data['like'])){
      $this->db->like($data['like']);
    }   
    if(isset($data['like_col']) && !empty($data['like_col'])){
      $this->db->like_col($data['like_col']);
    }   
    if(isset($data['or_like']) && !empty($data['or_like'])){
      $this->db->or_like($data['or_like']);
    }   
    if(isset($data['not_like']) && !empty($data['not_like'])){
      $this->db->not_like($data['not_like']);
    }   
    if(isset($data['or_not_like']) && !empty($data['or_not_like'])){
      $this->db->or_not_like($data['or_not_like']);
    }
    if(isset($data['group_by']) && !empty($data['group_by'])){
      $this->db->group_by($data['group_by']);
    }   
    if(isset($data['distinct']) && !empty($data['distinct'])){
      $this->db->distinct($data['distinct']);
    }   
    if(isset($data['having']) && !empty($data['having'])){
      $this->db->having($data['having']);
    }
    if(isset($data['order_by']) && !empty($data['order_by'])){
      $this->db->order_by($data['order_by']);
    }
    if(isset($data['order_by_col']) && !empty($data['order_by_col'])){
      $this->db->order_by_col($data['order_by_col']);
    }
    if(isset($data['having']) && !empty($data['having'])){
      $this->db->having($data['having']);
    }
    if(isset($data['limit']) && !empty($data['limit'])){
      $this->db->limit($data['limit']);
    }
    if(isset($data['count_all']) && !empty($data['count_all'])){
      $this->db->count_all($data['count_all']);
    }
    
    $this->db->where($data['table'].'_status','enable');
    $query = $this->db->get($data['table']);  
    
    if(isset($data['output_type']) && $data['output_type'] == 'row'){
      $result = $query->row();
    }
    elseif(isset($data['output_type']) && $data['output_type'] == 'result'){
      $result = $query->result();
    }
    else{
      $result = $query->result_array();
    }   
    
    return $result; 
  } 



 function make_query($forsale_keyword, $forsale_location , $forsale_property_type, $forsale_property_status, $forsale_bedroom, $forsale_bathroom, $forsale_squarefeet,$forsale_maximum_area)
 {
  $query = "
       SELECT * FROM forsale
        WHERE forsale_status = 'enable'
  ";

  if(isset($forsale_home_heading) && !empty($forsale_home_heading))
  {
   $query .= "
    AND forsale_home_heading IN('".$forsale_home_heading."')
   ";
  }

  if(isset($forsale_location) && !empty($forsale_location))
  {
   $query .= "
    AND forsale_location IN('".$forsale_location."')
   ";
  }

  if(isset($forsale_property_type) && !empty($forsale_property_type))
  {
   $query .= "
    AND forsale_property_type IN('".$forsale_property_type."')
   ";
  }

  if (isset($forsale_property_status) && !empty($forsale_property_status)) {
    $query .= "
    AND forsale_property_status IN('".$forsale_property_status."')
    ";
  }

  if (isset($forsale_bedroom) && !empty($forsale_bedroom)) {
    $query .= "
    AND forsale_bedroom IN('".$forsale_bedroom."')
    ";
  }
  
  if (isset($forsale_bathroom) && !empty($forsale_bathroom)) {
    $query .= "
    AND forsale_bathroom IN('".$forsale_bathroom."')
    ";
  }

  if (isset($forsale_squarefeet) && !empty($forsale_squarefeet)) {
    $query .= "
    AND forsale_squarefeet IN ('".$forsale_squarefeet."')
    ";
  }

  if (isset($forsale_minimum_price) && !empty($forsale_minimum_price)) {
    $query .= "
    AND forsale_minimum_price IN ('".$forsale_minimum_price."'
    ";
  }

  if (isset($forsale_keyword) && !empty($forsale_keyword)) {
    $query .= "
    AND forsale_keyword IN ('".$forsale_keyword."'
    ";
  }

    return $query;
 }

 function search_property($search_property){

                 $this->db->like('forsale_keyword', $search_property);
                 $this->db->or_like('forsale_location', $search_property);
                 $this->db->or_like('forsale_property_type', $search_property);
                 $this->db->or_like('forsale_property_status', $search_property);
                 $this->db->or_like('forsale_bedroom', $search_property);
                 $this->db->or_like('forsale_bathroom', $search_property);
                 $this->db->or_like('forsale_squarefeet', $search_property);
                 $this->db->or_like('forsale_maximum_area', $search_property);
                 $query = $this->db->get('forsale');
                  if($query->num_rows()>0){
                 return $query->result();
               }
               else{
                return false;
               }
  }

//  public function search_property($forsale_keyword, $forsale_location , $forsale_property_type, $forsale_property_status, $forsale_bedroom, $forsale_bathroom, $forsale_squarefeet,$forsale_maximum_area){
//     $this->db->select('*');
//     $this->db->from('forsale');
//     if(!empty($forsale_keyword)) {
//         $this->db->group_start();
//         $this->db->like('forsale_keyword', $forsale_keyword);
//         // $this->db->or_like('description', $keyword);
//         $this->db->group_end();
//     }
//     elseif(!empty($forsale_location)) {
//       $this->db->group_start();
//       $this->db->like('forsale_location', $forsale_location);
//       $this->db->group_end();
//     }

//     elseif(!empty($forsale_property_type)){
//      $this->db->group_start();
//       $this->db->like('forsale_property_type', $forsale_property_type);
//       $this->db->group_end();

//     }

//     elseif(!empty($forsale_property_status)){
//      $this->db->group_start();
//       $this->db->like('forsale_property_status', $forsale_property_status);
//       $this->db->group_end();

//     }

//     elseif(!empty($forsale_bedroom)){
//      $this->db->group_start();
//       $this->db->like('forsale_bedroom', $forsale_bedroom);
//       $this->db->group_end();

//     }

//     elseif(!empty($forsale_bathroom)){
//      $this->db->group_start();
//       $this->db->like('forsale_bathroom', $forsale_bathroom);
//       $this->db->group_end();

//     }

//     elseif(!empty($forsale_squarefeet)){
//      $this->db->group_start();
//       $this->db->like('forsale_squarefeet', $forsale_squarefeet);
//       $this->db->group_end();

//     }

//     elseif(!empty($forsale_maximum_area)){
//       $this->db->group_start();
//       $this->db->like('forsale_maximum_area', $forsale_maximum_area);
//       $this->db->group_end();
//     }
//     // $this->db->like('location', $location);
//     $query = $this->db->get();
//     return $query->result_array();
// }

 // public function insert($data,$content)
 //  {            
 //    $result = $this->db->insert($data['table'],$content);     
 //    return $this->db->insert_id();
 //  } 


 function count_all($forsale_keyword, $forsale_location , $forsale_property_type, $forsale_property_status, $forsale_bedroom, $forsale_bathroom, $forsale_squarefeet,$forsale_maximum_area)
 {
  $query = $this->make_query($forsale_keyword, $forsale_location , $forsale_property_type, $forsale_property_status, $forsale_bedroom, $forsale_bathroom, $forsale_squarefeet,$forsale_maximum_area);
  $data = $this->db->query($query);
  return $data->num_rows();
 }

 function fetch_data($limit, $start,$forsale_keyword, $forsale_location , $forsale_property_type, $forsale_property_status, $forsale_bedroom, $forsale_bathroom, $forsale_squarefeet,$forsale_maximum_area)
 {
  $query = $this->make_query($forsale_keyword, $forsale_location , $forsale_property_type, $forsale_property_status, $forsale_bedroom, $forsale_bathroom, $forsale_squarefeet,
    $forsale_maximum_area);

     // $query .= 'LIMIT '.$start.', ' . $limit;
  // $query .= ' LIMIT '.$start.', ' . $limit;
  // $query = $this->db->limit($limit, $start);
  // $data = $this->db->get('forsale');

  $data = $this->db->query($query);
   //print_r($query);exit;
  
  $output = '';
  if($data->num_rows() > 0)
  {
   foreach($data->result_array() as $row)
   {
    if (!empty($row['forsale_image'])) {
      $image = 'assets/front/images/listing7.jpg'.$row['forsale_image'];
     } else {
       $image = 'assets/front/images/listing7.jpg';
     }
   $price = !empty($this->currency && $row['forsale_price'])?$this->currency.$row['forsale_price'].'/':'';
   

   $heading = !empty($row['forsale_home_heading'])?$row['forsale_home_heading']:"";
   $created_at = !empty($row['forsale_created_at'])?$row['forsale_created_at']:"";
   $updated_at = !empty($row['forsale_updated_at'])?$row['forsale_updated_at']:"";
   $subheading = !empty($row['forsale_home_subheading'])?$row['forsale_home_subheading']:"";
   $squarefeet = !empty($row['forsale_squarefeet'])?$row['forsale_home_subheading']:"";
   $bedroom =!empty($row['forsale_bedroom'])?$row['forsale_bedroom']:"";
   $bathroom =!empty($row['forsale_bathroom'])?$row['forsale_bathroom']:"";
   $text =!empty($row['forsale_text'])?$row['forsale_text']:"";   
   
    $output .= '<div class="col-sm-6">

                  <div class="property_item heading_space">
                    <div class="property_head text-center">
                      <h3 class="captlize">'.$heading.'</h3>
                      <p>'.$subheading.'</p>
                    </div>
                    <div class="image"> <a href="#"> <img src="'.base_url().'assets/front/images/listing7.jpg'.$row['forsale_image'].'" alt="latest property" class="img-responsive"></a>
                      <div class="price clearfix"> <span class="tag">For Sale</span> </div>
                    </div>
                    <div class="proerty_content">
                      <div class="property_meta"> <span><i class="icon-select-an-objecto-tool"></i>'.$squarefeet.'sq ft</span> <span><i class="icon-bed"></i>'.$bedroom.' Bedrooms</span> <span><i class="icon-safety-shower"></i>'.$bathroom.' Bathrooms</span> </div>
                      <div class="proerty_text">
                        <p>'.$text.' </p>
                      </div>
                      <div class="favroute clearfix">
                        <p class="pull-md-left">'.$price.'</p>
                        <ul class="pull-right">
                          <li><a href="#"><i class="icon-like"></i></a></li>
                          <li><a href="#" class="share_expender" ><i class="icon-share3"></i></a></li>
                        </ul>
                      </div>

                    </div>

                  </div>

                </div>
     
    ';
   }
  }

  else
  {
   $output = '<h3>No Data Found ';
  }
  return $output;
 }
}

?>