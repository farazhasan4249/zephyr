<?php
 
class foreclosure_model extends CI_Model
{
 function fetch_filter_type($type)
 {
  $this->db->distinct();
  $this->db->select($type);
  $this->db->from('foreclosure');
  $this->db->where('foreclosure_status' , 'enable');
  return $this->db->get();
 }

 public function get($data)
  {   
    if(isset($data['select']) && !empty($data['select'])){
      $this->db->select($data['select']);
    }
    if(isset($data['select_max']) && !empty($data['select_max'])){
      $this->db->select_max($data['select_max']);
    }
    if(isset($data['select_min']) && !empty($data['select_min'])){
      $this->db->select_min($data['select_min']);
    }
    
    if(isset($data['select_avg']) && !empty($data['select_avg'])){
      $this->db->select_avg($data['select_avg']);
    }
    if(isset($data['select_sum']) && !empty($data['select_sum'])){
      $this->db->select_sum($data['select_sum']);
    }
    if(!empty($data['join_array']) && is_array($data['join_array'])){
      foreach($data['join_array'] as $record){
        $this->db->join($record['join_table'],$record['join'],$record['join_type']);
        $this->db->where($record['join_table'].'_status','enable');
      }
    }
    if(!empty($data['join'])){        
      $this->db->join($data['join_table'],$data['join'],$data['join_type']);      
    }
    if(isset($data['where']) && !empty($data['where'])){
      $this->db->where($data['where']);
    }
    if(isset($data['or_where']) && !empty($data['or_where'])){
      $this->db->or_where($data['or_where']);
    }
    if(isset($data['or_where_in']) && !empty($data['or_where_in'])){
      $this->db->or_where_in($data['or_where_in']);
    }   
    if(isset($data['where_not_in']) && !empty($data['where_not_in'])){
      $this->db->where_not_in($data['where_not_in']);
    }   
    if(isset($data['like']) && !empty($data['like'])){
      $this->db->like($data['like']);
    }   
    if(isset($data['like_col']) && !empty($data['like_col'])){
      $this->db->like_col($data['like_col']);
    }   
    if(isset($data['or_like']) && !empty($data['or_like'])){
      $this->db->or_like($data['or_like']);
    }   
    if(isset($data['not_like']) && !empty($data['not_like'])){
      $this->db->not_like($data['not_like']);
    }   
    if(isset($data['or_not_like']) && !empty($data['or_not_like'])){
      $this->db->or_not_like($data['or_not_like']);
    }
    if(isset($data['group_by']) && !empty($data['group_by'])){
      $this->db->group_by($data['group_by']);
    }   
    if(isset($data['distinct']) && !empty($data['distinct'])){
      $this->db->distinct($data['distinct']);
    }   
    if(isset($data['having']) && !empty($data['having'])){
      $this->db->having($data['having']);
    }
    if(isset($data['order_by']) && !empty($data['order_by'])){
      $this->db->order_by($data['order_by']);
    }
    if(isset($data['order_by_col']) && !empty($data['order_by_col'])){
      $this->db->order_by_col($data['order_by_col']);
    }
    if(isset($data['having']) && !empty($data['having'])){
      $this->db->having($data['having']);
    }
    if(isset($data['limit']) && !empty($data['limit'])){
      $this->db->limit($data['limit']);
    }
    if(isset($data['count_all']) && !empty($data['count_all'])){
      $this->db->count_all($data['count_all']);
    }
    
    $this->db->where($data['table'].'_status','enable');
    $query = $this->db->get($data['table']);  
    
    if(isset($data['output_type']) && $data['output_type'] == 'row'){
      $result = $query->row();
    }
    elseif(isset($data['output_type']) && $data['output_type'] == 'result'){
      $result = $query->result();
    }
    else{
      $result = $query->result_array();
    }   
    
    return $result; 
  } 

  function search_property($search_property){

                 $this->db->like('foreclosure_keyword', $search_property);
                 $this->db->or_like('foreclosure_location', $search_property);
                 $this->db->or_like('foreclosure_property_type', $search_property);
                 $this->db->or_like('foreclosure_property_status', $search_property);
                 $this->db->or_like('foreclosure_bedroom', $search_property);
                 $this->db->or_like('foreclosure_bathroom', $search_property);
                 $this->db->or_like('foreclosure_squarefeet', $search_property);
                 $this->db->or_like('foreclosure_maximum_area', $search_property);
                 $query = $this->db->get('foreclosure');
                 return $query->result();
  }




 function make_query($foreclosure_keyword, $foreclosure_location , $foreclosure_property_type, $foreclosure_property_status, $foreclosure_bedroom, $foreclosure_bathroom, $foreclosure_squarefeet,$foreclosure_maximum_area)
 {
  $query = "
       SELECT * FROM foreclosure
        WHERE foreclosure_status = 'enable'
  ";

  if(isset($foreclosure_home_heading) && !empty($foreclosure_home_heading))
  {
   $query .= "
    AND foreclosure_home_heading IN('".$foreclosure_home_heading."')
   ";
  }

  if(isset($foreclosure_location) && !empty($foreclosure_location))
  {
   $query .= "
    AND foreclosure_location IN('".$foreclosure_location."')
   ";
  }

  if(isset($foreclosure_property_type) && !empty($foreclosure_property_type))
  {
   $query .= "
    AND foreclosure_property_type IN('".$foreclosure_property_type."')
   ";
  }

  if (isset($foreclosure_property_status) && !empty($foreclosure_property_status)) {
    $query .= "
    AND foreclosure_property_status IN('".$foreclosure_property_status."')
    ";
  }

  if (isset($foreclosure_bedroom) && !empty($foreclosure_bedroom)) {
    $query .= "
    AND foreclosure_bedroom IN('".$foreclosure_bedroom."')
    ";
  }
  
  if (isset($foreclosure_bathroom) && !empty($foreclosure_bathroom)) {
    $query .= "
    AND foreclosure_bathroom IN('".$foreclosure_bathroom."')
    ";
  }

  if (isset($foreclosure_squarefeet) && !empty($foreclosure_squarefeet)) {
    $query .= "
    AND foreclosure_squarefeet IN ('".$foreclosure_squarefeet."')
    ";
  }

  // if (isset($foreclosure_maximum_area) && !empty($foreclosure_maximum_area)) {
  //   $query .= "
  //   AND foreclosure_maximum_area IN ('".$foreclosure_maximum_area."')
  //   ";
  // }

  // if (isset($foreclosure_maximum_price) && !empty($foreclosure_maximum_price)) {
  //   $query .= "
  //   AND foreclosure_maximum_price IN ('".$foreclosure_maximum_price."')
  //   ";
  // }

  // if (isset($foreclosure_minimum_price) && !empty($foreclosure_minimum_price)) {
  //   $query .= "
  //   AND foreclosure_minimum_price IN ('".$foreclosure_minimum_price."')
  //   ";
  // }

  if (isset($foreclosure_keyword) && !empty($foreclosure_keyword)) {
    $query .= "
    AND foreclosure_keyword IN ('".$foreclosure_keyword."')
    ";
  }

  // if (isset($foreclosure_home_subheading) && !empty($foreclosure_home_subheading)) {
  //   $query .= "
  //   AND foreclosure_home_subheading IN ('".$foreclosure_home_subheading."')
  //   ";
  // }
  // if (!empty($search))
  // {
  //  $query .= "
  //   AND `foreclosure_keyword` LIKE'%".$search."%' 
  //  ";
  // }

  return $query;
 }

 function count_all($foreclosure_keyword, $foreclosure_location , $foreclosure_property_type, $foreclosure_property_status, $foreclosure_bedroom, $foreclosure_bathroom, $foreclosure_squarefeet,$foreclosure_maximum_area)
 {
  $query = $this->make_query($foreclosure_keyword, $foreclosure_location , $foreclosure_property_type, $foreclosure_property_status, $foreclosure_bedroom, $foreclosure_bathroom, $foreclosure_squarefeet,$foreclosure_maximum_area);
  $data = $this->db->query($query);
  return $data->num_rows();
 }

 function fetch_data($limit, $start,$foreclosure_keyword, $foreclosure_location , $foreclosure_property_type, $foreclosure_property_status, $foreclosure_bedroom, $foreclosure_bathroom, $foreclosure_squarefeet,$foreclosure_maximum_area)
 {
  $query = $this->make_query($foreclosure_keyword, $foreclosure_location , $foreclosure_property_type, $foreclosure_property_status, $foreclosure_bedroom, $foreclosure_bathroom, $foreclosure_squarefeet,
    $foreclosure_maximum_area);

  // $query .= ' LIMIT '.$start.', ' . $limit;
  // $query = $this->db->limit($limit, $start);
  // $data = $this->db->get('forsale');

  $data = $this->db->query($query);
   //print_r($query);exit;
  
  $output = '';
  if($data->num_rows() > 0)
  {
   foreach($data->result_array() as $row)
   {
    if (!empty($row['foreclosure_image'])) {
      $image = 'assets/front/images/listing7.jpg'.$row['foreclosure_image'];
     } else {
       $image = 'assets/front/images/listing7.jpg';
     }
   $price = !empty($this->currency && $row['foreclosure_price'])?$this->currency.$row['foreclosure_price'].'/':'';
   

   $heading = !empty($row['foreclosure_home_heading'])?$row['foreclosure_home_heading']:"";
   $created_at = !empty($row['foreclosure_created_at'])?$row['foreclosure_created_at']:"";
   $updated_at = !empty($row['foreclosure_updated_at'])?$row['foreclosure_updated_at']:"";
   $subheading = !empty($row['foreclosure_home_subheading'])?$row['foreclosure_home_subheading']:"";
   $squarefeet = !empty($row['foreclosure_squarefeet'])?$row['foreclosure_squarefeet']:"";
   $bedroom =!empty($row['foreclosure_bedroom'])?$row['foreclosure_bedroom']:"";
   $bathroom =!empty($row['foreclosure_bathroom'])?$row['foreclosure_bathroom']:"";
   $text =!empty($row['foreclosure_text'])?$row['foreclosure_text']:"";
   
   // $like = ($row['like'] == "unlike-btn" && $row['consum_id'] == $this->session->userdata('reg_id'))?"unlike-btn":"like-btn";
    $output .= '<div class="col-sm-6">

                  <div class="property_item heading_space">
                    <div class="property_head text-center">
                      <h3 class="captlize">'.$heading.'</h3>
                      <p>'.$subheading.'</p>
                    </div>
                    <div class="image"> <a href="#"> <img src="'.base_url().'assets/front/images/listing7.jpg'.$row['foreclosure_image'].'" alt="latest property" class="img-responsive"></a>
                      <div class="price clearfix"> <span class="tag">For Sale</span> </div>
                    </div>
                    <div class="proerty_content">
                      <div class="property_meta"> <span><i class="icon-select-an-objecto-tool"></i>'.$squarefeet.'sq ft</span> <span><i class="icon-bed"></i>'.$bedroom.' Bedrooms</span> <span><i class="icon-safety-shower"></i>'.$bathroom.' Bathrooms</span> </div>
                      <div class="proerty_text">
                        <p>'.$text.' </p>
                      </div>
                      <div class="favroute clearfix">
                        <p class="pull-md-left">'.$price.'</p>
                        <ul class="pull-right">
                          <li><a href="#"><i class="icon-like"></i></a></li>
                          <li><a href="#" class="share_expender" ><i class="icon-share3"></i></a></li>
                        </ul>
                      </div>

                    </div>

                  </div>

                </div>
     
    ';
   }
  }

  else
  {
   $output = '<h3>No Data Found ';
  }
  return $output;
 }
}

?>